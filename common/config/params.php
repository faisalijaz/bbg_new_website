<?php

//use kartik\export\ExportMenu;

return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'user.passwordResetTokenExpire' => 3600,
    'months' => [
        '01' => 'January',
        '02' => 'Febraury',
        '03' => 'March',
        '04' => 'April',
        '05' => 'May',
        '06' => 'June',
        '07' => 'July',
        '08' => 'August',
        '09' => 'September',
        '10' => 'October',
        '11' => 'November',
        '12' => 'December',
    ],
    /*
    'exportConfig' => [
        ExportMenu::FORMAT_TEXT => false,
        ExportMenu::FORMAT_PDF => false,
        ExportMenu::FORMAT_HTML => false,
        ExportMenu::FORMAT_EXCEL => false,
        ExportMenu::FORMAT_EXCEL_X => false
    ],
    */
    'paymentTypes' => [
        '' => 'Cash on Deliver',
        'COD' => 'Cash',
        'CCO' => 'Credit Card Online',
        'CP' => 'Contract Payment',
        'RP' => 'Reward Points'
    ],
    'accountTypes' => [
        'nominee' => 'Nominee',
        'alternate' => 'Alternate',
        'additional' => 'Additional Member',
        'named_associate' => 'Named Associate'
    ],
    'statusTitle' => ['in-active','active','invoiced','deleted','renewal required'],
    'userTitle' => [
        'Mr' => 'Mr',
        'Ms' => 'Ms',
        'Miss' => 'Miss',
        'Dr' => 'Dr',
        'Mrs' => 'Mrs',
        'HMCG' => 'HMCG',
        'H.E' => 'H.E',
        'Sir' => 'Sir'
    ],
    'UserTypesforEvent' => [
        'member' => 'Member',
        'guest' => 'Non-Member',
        'committee' => 'Committee',
        'focus-chair' => 'Focus Chair',
        'platinum-sponsor' => ' Platinum Sponsor',
        'speaker' => 'Speaker',
        'speakers-guest' => 'Speaker’s Guest',
        'VIP' => 'VIP',
        'business-associate' => 'Business associate'
    ],

    'text_delete' => 'File Deleted',
    'text_directory' => 'Directory Created',
    'text_uploaded' => 'File Uploaded',
    'no_image' => "https://d1l2trc10ppjt4.cloudfront.net/default-placeholder5a66caaf3a8a3.png",
    'no_company' => "https://d1l2trc10ppjt4.cloudfront.net/company5a707269073bc.png",
    'invoice_header_image' => 'https://d1l2trc10ppjt4.cloudfront.net/logo/bbg_new_logo5a685a0071f2d.png',
    'email_header_image' => 'https://d1l2trc10ppjt4.cloudfront.net/Site/email_header_25a9a566782907.jpg',
    'email_footer_image' => 'https://d1l2trc10ppjt4.cloudfront.net/Site/email_template_footer5a671d6b8e5db.gif',
    'invoice_title' => 'TAX INVOICE',
    'event_badge' => 'https://d1l2trc10ppjt4.cloudfront.net/Site/email_header_15a9a565a9056c.jpg',
    'receipt_title' => 'Payment Receipt',
    'testPaytabsSecureHash' => 'Kfgedycmp6g8hmm0FNsX3Pe0ein9IH5tWmHaVTwkZMtg9PhOCJLFzZGOi4Y1L3uIqbSl5oG5b9mkYD8AnUgWUWLd0JeMNIudUmzZ',
    'testPaytabsMerchant' => '10026202',
    'livePaytabsSecureHash' => 'kSBNQh08Gv0em0ZLgf7qQQUuQZjJYnHjfi66z6fgCfuZftLvJTWAJR4zQoiPL081jf7Tlm9YCEXS8JoRClrlcwU3VdSHd1IwgQpC',
    'livePaytabsMerchant' => '10025833',
    /* 'payment_verify_url' => "http://bbg.new/site/verify-payment/",
    'payment_verify_url_event' => "http://bbg.new/events/thank-you/", */
    'payment_verify_url' => "http://bbg.manal.ae/site/verify-payment/",
    'payment_verify_url_event' => "http://bbg.manal.ae/events/thank-you/"

];
