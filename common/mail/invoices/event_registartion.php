<?php

use common\models\Invoices;

?>
<style>
    table td {
        font-family: "Century Gothic", Gadget, sans-serif;
        font-size: 11px;
        line-height: 1.5;
        padding: 2px;
    }

    table th {
        color: #ffffff;
        width: 50%;
        font-size: 14px;
        padding:5px;
    }

    h2 {
        font-size: 22px;
        font-weight: normal;
        color: #D11349;
        margin: 15px 0px 5px;
    }

    h3 {
        font-size: 16px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 0px;
    }

    h4 {
        font-size: 12px;
        font-weight: bold;
        color: #002D5D;
        margin: 15px 15px;
    }

    p {
        margin: 20px 0px;
        font-size: 11px;
    }

    .eventLabel, .eventValue {
        color: #002D5D;
        margin: 5px 0px;
        line-height: 1.5;
        font-size: 13px;
        font-weight: bold;
    }

    .eventValue {
        color: #D11349;
        font-weight: normal;
    }

    @media print {
        body {
            margin: 0mm auto;
            background: #ccc;
        }
    }
</style>

<table width="650px" align="center" cellpadding="0" cellspacing="10" style="background-color: #fff;border: solid 1px #cccccc;">
    <tr>
        <td>
            <table width="98%" align="center" cellpadding="0" cellspacing="0">
                <?php
                if ($subscriber == null) {
                    ?>
                    <tr>
                        <td colspan="2">There is not any Invoice.</td>
                    </tr>
                    <?php
                } else {

                    $member = null;
                    $invoice = null;

                    if ($subscriber->member <> null) {
                        $member = $subscriber->member;
                    }

                    if ($subscriber->invoice <> null) {
                        $invoice = $subscriber->invoice;
                    } else {
                        $invoice = Invoices::findOne(['invoice_id' => $subscriber->gen_invoice]);
                    }

                    ?>
                    <tr>
                        <td width="27%" align="left" valign="top"><br><img
                                    src="http://bbgdubai.org/_assets/_images/logo.png"/>
                        </td>
                        <td align="right" colspan="2">
                            <strong>Billed To:</strong>
                            <?= $subscriber->firstname . ' ' . $subscriber->lastname; ?>
                            <?= ($subscriber->user_type == 'guest') ? '(Guest)' : ''; ?>
                            <br/>
                            <b>Invoice #: <?= $subscriber->gen_invoice; ?></b>
                            <br/><b>Date:</b> <?php echo date("d-M-Y", $subscriber->created_at); ?>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">
                            <h2 style="font-size:22px; font-weight: normal; color: #D11349; margin: 15px 0px 5px;">
                                <?= trim('INVOICE'); ?>
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left">
                            <table width="100%" height="100%" align="left" cellpadding="3" cellspacing="1">
                                <tr>
                                    <td colspan="2"></td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                    </td>
                                </tr>
                                <tr>
                                    <th align="left" bgcolor="#002D5D"
                                        style="font-family:'Century Gothic', Gadget, sans-serif ; font-size: 11px;  line-height: 2; color: #FFFFFF;">
                                        Item
                                    </th>
                                    <th align="left" bgcolor="#002D5D"
                                        style="font-family:  'Century Gothic', Gadget, sans-serif ; font-size: 11px;  line-height: 2; color: #FFFFFF;">
                                        Amount
                                    </th>
                                </tr>
                                <?php
                                if ($invoice <> null) {
                                    if ($invoice->invoiceItems <> null) {
                                        foreach ($invoice->invoiceItems as $item) {
                                            ?>
                                            <tr>
                                                <td colspan="1">
                                                    <?= ($item->eventSubscription <> null) ? $item->eventSubscription->firstname . ' ' . $item->eventSubscription->lastname : ""; ?>
                                                    <br/> <?= ($item->eventSubscription <> null) ? $item->eventSubscription->eventData->title : ""; ?>
                                                </td>
                                                <td colspan="1" style="text-align: right;">
                                                    AED <?= $item->subtotal; ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <tr>
                                <div style="clear:both;width: 100%;border-top: #000 solid 1px;margin-top: 30px;">
                                    <table class="table" style="width: 100%; float: left;" cellpadding="5">
                                        <tbody>

                                        <tr>
                                            <td  colspan="1" style="text-align: left;">
                                                <strong>Subtotal</strong>
                                                <br/><strong>VAT(5%)</strong>
                                            </td>
                                            <td colspan="1" style="text-align: right;">
                                                <strong><?= $invoice->subtotal; ?></strong>
                                                <br/><strong><?= $invoice->tax; ?></strong>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="1" style="text-align: left;">
                                                <br/><strong>Total</strong>
                                            </td>
                                            <td colspan="1" style="text-align: right;">
                                                <br/>
                                                <strong>
                                                    AED <?= $invoice->subtotal + $invoice->tax; ?>
                                                    <input type="hidden"
                                                           value="<?= $invoice->subtotal + $invoice->tax; ?>"
                                                           id="actual_total"/>
                                                </strong>
                                            </td>
                                        </tr>

                                        <?php
                                        if ($invoice <> null) {
                                            if ($invoice->adjustment <> null) {
                                                $adjustment = $invoice->adjustment;
                                                ?>
                                                <tr>
                                                    <td colspan="1" style="text-align: left;"><strong>Adjustment</strong>
                                                    </td>
                                                    <td colspan="1" style="text-align: right;">
                                                        <strong>
                                                            <?= $adjustment->type . " " . $adjustment->adjustment; ?>
                                                        </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="1" style="text-align: left;"><strong>Total Payable</strong>
                                                    </td>
                                                    <td colspan="1" style="text-align: right;">
                                                        <strong>AED <?= $subscriber->fee_paid; ?></strong>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                </tr>
                                <?php
                                if ($subscriber->payment_status == 'pending') { ?>
                                    <tr>
                                        <td colspan="2">BBG members can pay online.Please login your BBG Dubai account and click
                                            below
                                            <b>Pay online</b> link
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <a href="http://bbgdubai.org/event/confirmpayment?id=<?php echo '1'; ?>">
                                                Pay online now
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <input type="hidden" value="#" id="payyyy"/>
                                        <td colspan="2">Below are the methods of payments acceptable by BBG:</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><p><b>CASH</b></p>
                                            <p><b>CHEQUE:</b> Payable to British Business Group</p></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"><p><b>BANK TRANSFER-(NET OF BANK CHARGES)</b></p>
                                            <table width="98%" align="left" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td width="25%">In favor of</td>
                                                    <td width="1%">:</td>
                                                    <td>British Business Group</td>
                                                </tr>
                                                <tr>
                                                    <td width="25%">Bank</td>
                                                    <td width="1%" valign="top">:</td>
                                                    <td>HSBC Bank Middle East, P.O. Box 3766, Jumeirah 1, Dubai, UAE
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="25%">Account No</td>
                                                    <td width="1%">:</td>
                                                    <td>030-123756-001</td>
                                                </tr>
                                                <tr>
                                                    <td width="25%">IBAN</td>
                                                    <td width="1%">:</td>
                                                    <td>AE880200000030123756001</td>
                                                </tr>
                                                <tr>
                                                    <td width="25%">Swift Code</td>
                                                    <td width="1%">:</td>
                                                    <td>BBMEAEAD</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center" style="color: red; font-style: italic;">
                            System Generated Invoice
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6"><h5> British Business Group, P.O. Box 9333 Dubai, UAE</h5>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </td>
    </tr>
</table>