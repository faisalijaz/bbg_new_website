<?php

$this->title = 'Booking Confirmation';
$payment_types = \Yii::$app->params['paymentTypes'];
$name = '';
$phone = '';
$email = '';
$address = '';
$area = '';
$city = '';
?>
    <div class="container">
        <div class="row padd-90">
            <div class="col-xs-12 col-md-12 ">
                <p>
                    <strong style="font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; margin: 0;">
                        <?php if (isset($bookingDetails) && count($bookingDetails)) {
                        if ($bookingDetails->account <> null) {
                            $acc = $bookingDetails->account;
                            echo 'Dear ' . $acc->first_name . ' ' . $acc->last_name . ',';
                        }
                        ?>
                    </strong>
                <p>
                    Thank you for booking with Tour Dubai. Your booking has been confirmed and guaranteed with ref.
                    no. <?= $bookingDetails->id; ?> and details are as follows:
                </p>

                <?php }
                ?>
                </p>
                <?php if (isset($bookingDetails) && count($bookingDetails)) {
                    if ($bookingDetails->account <> null) {
                        $acc = $bookingDetails->account;
                        $name = $acc->first_name . ' ' . $acc->last_name;
                        $phone = $acc->phone_number;
                        $email = $acc->email;

                        if ($bookingDetails->bookingAddress <> null) {
                            $add = $bookingDetails->bookingAddress;

                            if ($add->addressCity <> null) {
                                $city = $add->addressCity->name;
                            }
                            if ($add->addressArea <> null) {
                                $area = $add->addressArea->name;
                            }

                            $address = $add->address . ', ' . $add->street . ', ' . $area . ', ' . $city;
                        }
                    }
                    ?>
                    <div class="row" style="w">
                        <div class="col-md-9  m-t-30" style="width: 50%; float: left;">
                            <p>
                                Customer Name: <strong><?= $name; ?></strong><br/>
                                Phone Number: <strong><?= $phone; ?></strong><br/>
                                Email: <strong><?= $email; ?></strong><br/>
                                Address: <strong><?= $address; ?></strong><br/>
                            </p>

                        </div>
                        <div class="col-md-9  m-t-30" style="width: 50%; float: right;">
                            <p>Booking number: <strong><?= $bookingDetails->id; ?></strong><br/>
                                Booking Date:
                                <strong><?= Yii::$app->formatter->asDate($bookingDetails->created_at, 'yyyy-MM-dd');; ?></strong>
                                <br/>
                                Booking status: <strong><?php if ($bookingDetails->status == 'created') {
                                        echo 'Pending';
                                    } else {
                                        echo ucwords($bookingDetails->status);
                                    } ?></strong><br/>
                                <?php
                                if ($bookingDetails->cancellation_time <> "") {
                                    ?>
                                    Cancellation Time: <strong><?= $bookingDetails->cancellation_time; ?></strong> <br/>
                                <?php } ?>
                                Total <strong>AED <?= round($bookingDetails->total_sum, 2); ?></strong>
                                <br/>Payment Method:
                                <strong><?= $bookingDetails->payment_type ? $payment_types[$bookingDetails->payment_type] : 'Cash On Deliver'; ?></strong>
                            </p>
                        </div>
                    </div>
                    <br/>
                    <div class="detail-content-block">
                        <h3 class="large-title" style="clear: both">Booking Details</h3>
                        <div class="table-responsive">
                            <table class="table style-1 type-2 striped"
                                   style="width: 100%;">
                                <tbody>
                                <tr style="background: #f1f1f1;height: 40px;font-family: 'Cabin', sans-serif;font-size: 14px;line-height: 18px;font-weight: 400;">
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Experiences</strong></td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Tour Date</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Tour Time</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Adults</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Childrens</strong>
                                    </td>
                                    <td class="table-label color-grey" style="color: #000;padding-left: 20px;">
                                        <strong>Price</strong>
                                    </td>
                                </tr>
                                </tbody>

                                <tbody>
                                <?php
                                if (count($bookingDetails->tourBookingDetails) > 0) {
                                    foreach ($bookingDetails->tourBookingDetails as $tourDetails) {
                                        $isExclusive = $tourDetails->isExclusive;
                                        $style = '';
                                        $i = 0;
                                        if ($i % 2 == 0) {
                                            $style = 'background:#C4CDC6;color:#800000;';
                                        }
                                        ?>
                                        <tr style="<?= $style; ?> height: 40px;font-family: 'Cabin', sans-serif;font-size: 14px;line-height: 18px;font-weight: 400;">
                                            <td style="border: 1px solid #f1f1f1;color: #800000; padding-left: 20px;">
                                                <span class="item"><?= $tourDetails->tour->title; ?></span>
                                            </td>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                <span class="item"><?= $tourDetails->check_in; ?></span>
                                            </td>
                                            <?php if ($isExclusive) { ?>
                                                <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                    <span class="item"><?= $tourDetails->tour_time; ?></span>
                                                </td>
                                            <?php } ?>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                <span class="item"><?= $tourDetails->adults; ?>
                                                    <?php if (!$isExclusive) {
                                                        echo ' x ' . $tourDetails->adult_price . ' = ' . $tourDetails->adults * $tourDetails->adult_price;
                                                    } ?>
                                                </span>
                                            </td>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                <span class="item"><?= $tourDetails->childern; ?>
                                                    <?php if (!$isExclusive) {
                                                        echo ' x ' . $tourDetails->child_price . ' = ' . $tourDetails->childern * $tourDetails->child_price;
                                                    } ?>
                                                </span>
                                            </td>
                                            <td style="border: 1px solid #f1f1f1;color: #800000;padding-left: 20px;">
                                                AED <?= $tourDetails->price; ?>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>

                            <?php
                            if ($bookingDetails->touristInfo <> null) {
                            ?>
                        </div>
                        <div class="table-responsive">
                            <h3 class="large-title">Guests Information</h3>
                            <table class="table style-1 type-2 striped" style="width: 100%">
                                <tr class="table-heading">
                                    <td class="table-label color-grey" style="width: 10%">
                                        <strong>#</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width: 30%">
                                        <strong>Name</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width: 20%">
                                        <strong>Gender</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width: 10%">
                                        <strong>Age</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width: 10%">
                                        <strong>Nationality</strong>
                                    </td>
                                    <td class="table-label color-grey" style="width: 20%">
                                        <strong>Pickup From</strong>
                                    </td>
                                </tr>

                                <tbody>
                                <?php
                                $i = 0;
                                foreach ($bookingDetails->touristInfo as $touristInfo) {
                                    $style = '';
                                    if ($i % 2 == 0) {
                                        $style = 'style = "background:#C4CDC6;color: #800000;height:40px;font-family:Cabin,sans-serif;font-size:14px;line-height:18px;font-weight:400"';
                                    }
                                    ?>
                                    <tr <?= $style; ?>>
                                        <td class="table-label ">
                                            <span class="item"><?= $i + 1; ?></span>
                                        </td>
                                        <td class="table-label ">
                                            <span class="item"><?= $touristInfo->name; ?></span>
                                        </td>
                                        <td class="table-label">
                                            <span class="item"><?= $touristInfo->gender; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->age; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->nationality; ?></span>
                                        </td>
                                        <td class="table-label color-dark-2">
                                            <span class="item"><?= $touristInfo->pick_from_address; ?></span>
                                        </td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                                ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            if ($bookingDetails->bookingPickups <> null) {
                            $pickup = $bookingDetails->bookingPickups;
                            ?>
                        </div>
                        <div class="table-responsive" style="width:100%;">
                            <h3 class="large-title">Pickup Information</h3>
                            <table class="table style-1 type-2 striped">
                                <tr class="table-heading">
                                    <td class="table-label color-grey">
                                        <strong>Hotel Name</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Pickup Time</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Pickup Date</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Room Number</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Phone Number</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>Area</strong>
                                    </td>
                                    <td class="table-label color-grey">
                                        <strong>City</strong>
                                    </td>
                                </tr>
                                <tbody>
                                <tr>
                                    <td class="table-label ">
                                        <span class="item"><?= $pickup->pickup_hotel_id; ?></span>
                                    </td>
                                    <td class="table-label ">
                                        <span class="item"><?= $pickup->pickup_room_number; ?></span>
                                    </td>
                                    <td class="table-label">
                                        <span class="item"><?= $pickup->pickup_phone_number; ?></span>
                                    </td>
                                    <td class="table-label color-dark-2">
                                        <span class="item"><?= $pickup->pickup_area; ?></span>
                                    </td>
                                    <td class="table-label color-dark-2">
                                        <span class="item">
                                            <?php if ($pickup->pickupCity <> null) {
                                                echo $pickup->pickupCity->name;
                                            } ?>
                                        </span>
                                    </td>
                                </tr>
                                <?php
                                ?>
                                </tbody>
                            </table>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="row" style="clear: both">
                            <div class="col-md-3  m-t-30" style="float: right;margin-top: 50px;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td>Addons:</td>
                                        <td>
                                            <strong>AED <?= round($bookingDetails->getTourBookingDetails()->sum('addons_price'), 2); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td>
                                            <strong>AED <?= round($bookingDetails->price, 2); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tax:</td>
                                        <td>
                                            <strong>AED <?= $bookingDetails->getTourBookingDetails()->sum('taxes'); ?></strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            <strong>AED <?= round($bookingDetails->total_sum, 2); ?></strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="clear: both">
                            <div class="col-md-12  m-t-30" style="float: left;margin-top: 50px;">
                                <table style="width: 100%;">
                                    <tr>
                                        <td><strong>Special Instructions</strong></td>
                                    </tr>
                                    <tr>
                                        <td> <?= ($bookingDetails->special_instruction) ? $bookingDetails->special_instruction : '-'; ?> </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <p style="text-align: left;float: left;clear: both;margin-top: 50px;">Should you have any
                            questions please feel free to contact us 800TourDubai </p>
                        <?php
                        if ($bookingDetails->cancellation_time <> "") {
                            ?>
                            <div class="row"
                                 style="margin-top: 20px;font-size: 16px;color: red;font-weight: 400;font-family: inherit;">
                                <p>
                                    <strong>Note:</strong> We have tentatively blocked your booking request. Once
                                    payment is
                                    done you
                                    will
                                    receive our final booking confirmation.
                                </p>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                <?php } else { ?>
                    <div class="detail-content-block margin-20-bottom">
                        <p class="order-summry-title"></p>
                    </div>
                <?php }
                ?>
            </div>
        </div>
    </div>
    <div style="clear:both;margin-top: 150px;"></div>
<?= $this->registerJs('
    $(document).find("#Print").on("click", function() {
        //Print ele4 with custom options
        $("#PrintInvoice").print({
                globalStyles : true, 
                prepend : "Customer Inovice",
                stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
            });
        });
    ');
