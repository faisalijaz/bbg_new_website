<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

?>

<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
        body {
            font-family: "Century Gothic", Gadget, sans-serif;
            font-size: 13px;
            background: #ffffff;
            line-height: 1.5
        }

        h2 {
            font-size: 22px;
            font-weight: normal;
            color: #D11349;
            margin: 15px 0px 5px;
        }

        h3 {
            font-size: 16px;
            font-weight: bold;
            color: #002D5D;
            margin: 15px 0px;
        }

        p {
            margin: 20px 0px;
            font-size: 13px;
        }

        .eventLabel, .eventValue {
            color: #002D5D;
            margin: 5px 0px;
            line-height: 1.5;
            font-size: 13px;
            font-weight: bold;
        }

        .eventValue {
            color: #D11349;
            font-weight: normal;
        }

    </style>
</head>
<body>
<?php $this->beginBody() ?>
<table width="624"  border="0" align="center" cellpadding="0" cellspacing="0" id="Table_01"
       style="border: solid 1px #cccccc;">

    <tr class="custom_img_header">
        <td colspan="3">
            <img src="http://bbgdubai.org/_assets/_images/news_email_header.jpg" width="100%" alt="">
            <hr/>
        </td>
    </tr>

    <tr>
        <td valign="top" colspan="3" style="padding: 20px;">

    <!-- Logo -->

    <?= $content ?>


        </td>
    </tr>
    <tr>
        <td colspan="3">
            <hr/>
            <img src="http://bbgdubai.org/_assets/_images/email_template_footer.gif" width="624" height="127" alt="">
        </td>
    </tr>
</table>
</div>
<?php $this->endBody() ?>
</body>
</html>

<?php $this->endPage() ?>