<?php

namespace common\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Settings;
use common\models\MenuWidget;

/**
 * Country Model
 *
 * @author Nadeem Akhtar <nadeemakhtar.se@gmail.com>
 */
class AppSetting
{

    public $settings;

    public function __construct() {
        $this->settings = Settings::find()->one();
        //$this->MenuWidget = MenuWidget::find()->one();
        $this->MenuWidget = ArrayHelper::map(MenuWidget::find()->where(['parent_id' => 0])->orderBy(['sort_order'=>SORT_ASC])->all(), 'title', 'url');
    }

    public function getSettings() {
        return $this->settings;
    }

    public function getByAttribute($attribute) {
        return $this->settings->{$attribute};
    }

    public function getMenuItems() {
        return $this->MenuWidget;
    }

    public function getSubMenuItems($attribute) {
        $usr_data = MenuWidget::find()->where("title = '$attribute'")->all();
        foreach ($usr_data as $data){
            $id =  $data->id;
        }
       if($id){
           return ArrayHelper::map(MenuWidget::find()->where(['parent_id' => $id])->all(), 'title', 'url');
       }
    }

    /**
     * Get Random unique number with length
     * @param int $length string length
     * @param int $domd5  string
     * @return string
     */
    public function getRandomNumber($length = 6, $domd5 = 0)
    {

        $conso = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '!', '*', '#'];
        $vocal = ['A', 'E', 'I', 'O', 'U'];
        $password = '';
        srand((double)microtime() * 1000000);
        $max = $length / 2;
        for ($i = 1; $i <= $max; $i++) {
            $password .= $conso[rand(0, 19)];
            $password .= $vocal[rand(0, 4)];
        }
        if ($domd5) {
            return md5($password);
        } else {
            return $password;
        }
    }
}
