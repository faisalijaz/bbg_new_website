<?php

namespace common\helpers;

use yii;

/**
 * Common Helper component is the class that will take care of all system common methods
 *
 * Example by using emailHelper Component in common.php
 *
 * ~~~~
 * yii::$app->commonHelper->funName()
 *
 * */
class CommonHelper
{
    /**
     * @param $ratings
     * @return string
     */
    public function getStarts($ratings)
    {
        $html = '';
        for ($i = 1; $i <= $ratings; $i++) {
            $html .='<span class="fa fa-star color-yellow" ></span >';
        }

        for ($j = $i + 1; $i <= 5; $i++) {
            $html .='<span class="fa fa-star color-grey" ></span >';
        }

        return $html;

    }
}