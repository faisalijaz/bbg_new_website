<?php

namespace common\helpers;

use yii;


/**
 * MenuHelper for left side menu render
 *
 * @author Nadeem AKhtar <nadeem@myswich.com>
 * @since 1.0
 */
class MenuHelper
{

    /**
     * get Admin menu
     * this function will create admin menu with permission and return it back,
     * @return array $menuItems this is left side menu
     * */
    public static function getMenu()
    {
        return $menuItems = [

            [
                'label' => '<i class="fa fa-desktop"></i> <span class="hidden-sm"> Dashboard </span>',
                'url' => ['/site/index'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-vcard"></i> <span class="hidden-sm"> Memberships </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Members',
                        'url' => ['/account'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Business',
                        'url' => ['/account', 'member_type' => '1'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Individual',
                        'url' => ['/account', 'member_type' => '2'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Non-Profit',
                        'url' => ['/account', 'member_type' => '3'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Upcoming Renewals',
                        'url' => ['/account/upcoming-renewals'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Membership Upgrade',
                        'url' => ['/account/upgrade-membership'],
                        'visible' => \Yii::$app->user->can('accounts')
                    ],
                    [
                        'label' => 'Membership Types',
                        'url' => ['/group'],
                        'visible' => \Yii::$app->user->can('groups')
                    ],
                    [
                        'label' => 'Membership Categories',
                        'url' => ['/categories'],
                        'visible' => \Yii::$app->user->can('categories')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-calendar"></i> <span class="hidden-sm"> Events </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Events',
                        'url' => ['/event'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Event Types',
                        'url' => ['/event-types'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ],
                    [
                        'label' => 'Sponsors',
                        'url' => ['/sponsors'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-money"></i> <span class="hidden-sm"> Financials </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('account_menu'),
                'items' => [
                    [
                        'label' => 'Invoices',
                        'url' => ['/invoices'],
                        'visible' => \Yii::$app->user->can('events')
                    ],
                    [
                        'label' => 'Payments',
                        'url' => ['/payments'],
                        'visible' => \Yii::$app->user->can('event_types')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-newspaper-o"></i> <span class="hidden-sm"> News </span>',
                'url' => ['/news'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-envelope-o"></i> <span class="hidden-sm"> Newsletters </span>',
                'url' => ['/newslettertemplates'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ],
            [
                'label' => '<i class="fa fa-handshake-o"></i> <span class="hidden-sm"> Customer Service </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('customer_service_menu'),
                'items' => [

                    [
                        'label' => 'Promotions',
                        'url' => ['/promotions'],
                        'visible' => \Yii::$app->user->can('promotions')
                    ],
                    [
                        'label' => 'Categories',
                        'url' => ['/categories'],
                        'visible' => \Yii::$app->user->can('categories')
                    ],
                    [
                        'label' => 'Member Offers',
                        'url' => ['/member-offers'],
                        'visible' => \Yii::$app->user->can('categories')
                    ]


                ]
            ],
            [
                'label' => '<i class="fa fa-sitemap"></i> <span class="hidden-sm"> Manage Content </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => \Yii::$app->user->can('cms_page_menu'),
                'items' => [
                    [
                        'label' => 'Committee Members & Staff',
                        'url' => ['/committe-members'],
                        'visible' => \Yii::$app->user->can('CommitteMembers')
                    ],
                    [
                        'label' => ' CMS pages',
                        'url' => ['/cms-page'],
                        'visible' => \Yii::$app->user->can('cms-pages')
                    ],
                    [
                        'label' => 'Gallery',
                        'url' => ['/gallery'],
                        'visible' => \Yii::$app->user->can('gallery')
                    ],
                    [
                        'label' => 'Banners',
                        'url' => ['/banner'],
                        'visible' => \Yii::$app->user->can('banners')

                    ],
                    [
                        'label' => 'Partners',
                        'url' => ['/partners'],
                        'visible' => \Yii::$app->user->can('partners')

                    ],
                    [
                        'label' => 'Tags',
                        'url' => ['/tag'],
                        'visible' => \Yii::$app->user->can('tags')
                    ]
                ]
            ],
            [
                'label' => '<i class="fa fa-list-alt"></i> <span class="hidden-sm"> Settings </span>',
                'url' => '#',
                'template' => '<a href="{url}" class="waves-effect">{label}</a>',
                'visible' => yii::$app->user->can('site_settings_menu'),
                'items' => [

                    [
                        'label' => 'Settings',
                        'url' => ['/settings'],
                        'visible' => \Yii::$app->user->can('settings')
                    ],
                    [
                        'label' => 'Menu',
                        'url' => ['/menu-widget'],
                        'visible' => \Yii::$app->user->can('settings')
                    ],


                    [
                        'label' => 'Countries',
                        'url' => ['/country'],
                        'visible' => \Yii::$app->user->can('countries')
                    ],


                    [
                        'label' => 'Cities',
                        'url' => ['/city'],
                        'visible' => \Yii::$app->user->can('cities')
                    ],

                    [
                        'label' => 'areas',
                        'url' => ['/area'],
                        'visible' => \Yii::$app->user->can('areas')
                    ],
                    [
                        'label' => 'Payment Type',
                        'url' => ['/payment-type'],
                        'visible' => \Yii::$app->user->can('payment_type')
                    ],
                    [
                        'label' => 'Payment Reasons',
                        'url' => ['/payment-reason'],
                        'visible' => \Yii::$app->user->can('payment_reason')
                    ],
                    [
                        'label' => 'Awards & Certificates',
                        'url' => ['/awards'],
                        'visible' => \Yii::$app->user->can('Awards')
                    ],
                    [
                        'label' => "Client's Reviews",
                        'url' => ['/clients-review'],
                        'visible' => \Yii::$app->user->can('client_reviews')
                    ],

                    [
                        'label' => 'Taxes',
                        'url' => ['/taxrate'],
                        'visible' => \Yii::$app->user->can('tax')
                    ],
                    [
                        'label' => 'Tax Groups',
                        'url' => ['/tax-group'],
                        'visible' => \Yii::$app->user->can('tax')
                    ],
                    [
                        'label' => 'Dashboard Widgets',
                        'url' => ['/widget'],
                        'visible' => \Yii::$app->user->can('widgets')
                    ],

                ]
            ],
            [
                'label' => ' <i class="fa fa-user-circle"></i><span class="hidden-sm">Permissions</span>',
                'url' => '#',
                'visible' => yii::$app->user->can('UserManagement'),
                'items' => [
                    [
                        'label' => 'Users',
                        'url' => ['/user']
                    ],
                    [
                        'label' => 'Rule Assignment',
                        'url' => ['/admin/assignment']
                    ],
                    [
                        'label' => 'Routes',
                        'url' => ['/admin/route']
                    ],
                    [
                        'label' => 'Permission',
                        'url' => ['/admin/permission']
                    ],
                    [
                        'label' => 'Roles',
                        'url' => ['/admin/role']
                    ],
                    [
                        'label' => 'Settings',
                        'url' => ['/settings']
                    ]
                ],
                'options' => ['class' => 'has_sub'],
                'template' => '<a href="{url}" class="waves-effect">{label}</a>'
            ]

        ];
    }

    private function getMemberTypes(){

    }
}
