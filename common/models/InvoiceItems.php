<?php

namespace common\models;

/**
 * This is the model class for table "invoice_items".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $user_id
 * @property string $invoice_related_to
 * @property integer $invoice_rel_id
 * @property string $invoice_category
 * @property string $payment_status
 * @property integer $status
 * @property string $amount
 * @property string $pdf_name
 * @property string $invoice_date
 * @property string $created_date
 * @property string $subtotal
 * @property string $tax
 * @property integer $payment_id
 */
class InvoiceItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id'], 'required'],
            [['invoice_id', 'user_id', 'invoice_rel_id', 'status', 'payment_id'], 'integer'],
            [['invoice_related_to', 'payment_status'], 'string'],
            [['amount', 'subtotal', 'tax'], 'number'],
            [['created_date'], 'safe'],
            [['invoice_category'], 'string', 'max' => 100],
            [['pdf_name', 'invoice_date'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_id' => 'Invoice ID',
            'user_id' => 'User ID',
            'invoice_related_to' => 'Invoice Related To',
            'invoice_rel_id' => 'Invoice Rel ID',
            'invoice_category' => 'Invoice Category',
            'payment_status' => 'Payment Status',
            'status' => 'Status',
            'amount' => 'Amount',
            'pdf_name' => 'Pdf Name',
            'invoice_date' => 'Invoice Date',
            'created_date' => 'Created Date',
            'subtotal' => 'Subtotal',
            'tax' => 'Tax',
            'payment_id' => 'Payment ID',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $sub = 0;
        $tax = 0;
        $total = 0;

        if ($this->invoice_id) {

            $invoice = Invoices::findOne($this->invoice_id);

            $sub = $invoice->subtotal;
            $tax = $invoice->tax;
            $total = $invoice->total;

            $invoice->subtotal = $sub + $this->subtotal;
            $invoice->tax = $tax + $this->tax;
            $invoice->total = $total + $this->amount;

            $invoice->save();

            /*if ($this->invoice_related_to == 'event') {
                $event = EventSubscriptions::findOne($this->invoice_rel_id);
                if ($event <> null) {
                    if ($this->payment_status == 'paid'){
                        $event->payment_status = 'paid';
                        $event->save();
                    }
                }
            }*/
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventSubscription()
    {
        return $this->hasOne(EventSubscriptions::className(), ['id' => 'invoice_rel_id']);
    }
}
