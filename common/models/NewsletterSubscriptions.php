<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "newsletter_subscriptions".
 *
 * @property int $id
 * @property string $email
 * @property string $date_created
 * @property string $active
 */
class NewsletterSubscriptions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newsletter_subscriptions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['date_created'], 'safe'],
            [['active'], 'string'],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\NewsletterSubscriptions', 'message' => 'You are already subscribed!'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'date_created' => 'Date Created',
            'active' => 'Active',
        ];
    }
}
