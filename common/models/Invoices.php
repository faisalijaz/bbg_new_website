<?php

namespace common\models;

/**
 * This is the model class for table "invoices".
 *
 * @property integer $invoice_id
 * @property integer $user_id
 * @property string $invoice_related_to
 * @property integer $invoice_rel_id
 * @property string $invoice_category
 * @property string $payment_status
 * @property string $amount
 * @property string $pdf_name
 * @property string $invoice_date
 * @property string $created_date
 */
class Invoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'invoice_rel_id','payment_id'], 'integer'],
            [['invoice_related_to', 'payment_status'], 'string'],
            [['created_date','subtotal','tax','total'], 'safe'],
            [['invoice_category'], 'string', 'max' => 100],
            [['invoice_date'], 'string', 'max' => 50],
            [['paytabs_p_id', 'paytabs_payment_url'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'user_id' => 'User ID',
            'invoice_related_to' => 'Invoice Related To',
            'invoice_rel_id' => 'Invoice Rel ID',
            'invoice_category' => 'Invoice Category',
            'payment_status' => 'Payment Status',
            'invoice_date' => 'Invoice Date',
            'created_date' => 'Created Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceItems(){
        return $this->hasMany(InvoiceItems::className(), ['invoice_id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember(){
        return $this->hasOne(Members::className(), ['id' => 'user_id']);
    }

    public function getInvoiceEventReg(){
        return $this->hasMany(EventSubscriptions::className(), ['id' => 'invoice_rel_id']);
    }

    public function getAdjustment(){
        return $this->hasOne(InvoiceAdjustments::className(),['invoice_id' => 'invoice_id']);
    }
}