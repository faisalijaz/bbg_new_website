<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_user".
 *
 * @property int $id
 * @property string $category
 * @property string $type
 * @property int $parent_id
 * @property string $username
 * @property string $bbg_membershipid
 * @property string $old_membershipid
 * @property string $company_id
 * @property string $password
 * @property string $salt
 * @property string $activationKey
 * @property string $activationCode
 * @property int $createtime
 * @property int $lastvisit
 * @property int $lastaction
 * @property int $lastpasswordchange
 * @property int $superuser
 * @property int $status
 * @property int $approved
 * @property string $avatar
 * @property string $termsNconditions
 * @property string $profile_background
 * @property resource $logo
 * @property string $notifyType
 * @property string $honourary
 * @property string $focus_chair
 * @property string $committee
 * @property string $sponsor
 * @property string $charity
 * @property string $sticky
 * @property string $sync_date
 * @property int $failedloginattempts
 * @property int $lastactivity
 * @property int $delete_request
 * @property int $lastrenewal
 * @property int $request_renewal
 */
class TblUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'createtime', 'lastvisit', 'lastaction', 'lastpasswordchange', 'superuser', 'status', 'approved', 'failedloginattempts', 'lastactivity', 'delete_request', 'lastrenewal', 'request_renewal'], 'integer'],
            [['notifyType', 'honourary', 'focus_chair', 'committee', 'sponsor', 'charity', 'sticky'], 'string'],
            [['category', 'type'], 'string', 'max' => 50],
            [['username'], 'string', 'max' => 100],
            [['bbg_membershipid', 'old_membershipid', 'company_id'], 'string', 'max' => 250],
            [['password', 'salt', 'activationKey', 'activationCode'], 'string', 'max' => 128],
            [['avatar', 'termsNconditions', 'profile_background', 'logo'], 'string', 'max' => 255],
            [['sync_date'], 'string', 'max' => 25],
            [['username'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'type' => Yii::t('app', 'Type'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'username' => Yii::t('app', 'Username'),
            'bbg_membershipid' => Yii::t('app', 'Bbg Membershipid'),
            'old_membershipid' => Yii::t('app', 'Old Membershipid'),
            'company_id' => Yii::t('app', 'Company ID'),
            'password' => Yii::t('app', 'Password'),
            'salt' => Yii::t('app', 'Salt'),
            'activationKey' => Yii::t('app', 'Activation Key'),
            'activationCode' => Yii::t('app', 'Activation Code'),
            'createtime' => Yii::t('app', 'Createtime'),
            'lastvisit' => Yii::t('app', 'Lastvisit'),
            'lastaction' => Yii::t('app', 'Lastaction'),
            'lastpasswordchange' => Yii::t('app', 'Lastpasswordchange'),
            'superuser' => Yii::t('app', 'Superuser'),
            'status' => Yii::t('app', 'Status'),
            'approved' => Yii::t('app', 'Approved'),
            'avatar' => Yii::t('app', 'Avatar'),
            'termsNconditions' => Yii::t('app', 'Terms Nconditions'),
            'profile_background' => Yii::t('app', 'Profile Background'),
            'logo' => Yii::t('app', 'Logo'),
            'notifyType' => Yii::t('app', 'Notify Type'),
            'honourary' => Yii::t('app', 'Honourary'),
            'focus_chair' => Yii::t('app', 'Focus Chair'),
            'committee' => Yii::t('app', 'Committee'),
            'sponsor' => Yii::t('app', 'Sponsor'),
            'charity' => Yii::t('app', 'Charity'),
            'sticky' => Yii::t('app', 'Sticky'),
            'sync_date' => Yii::t('app', 'Sync Date'),
            'failedloginattempts' => Yii::t('app', 'Failedloginattempts'),
            'lastactivity' => Yii::t('app', 'Lastactivity'),
            'delete_request' => Yii::t('app', 'Delete Request'),
            'lastrenewal' => Yii::t('app', 'Lastrenewal'),
            'request_renewal' => Yii::t('app', 'Request Renewal'),
        ];
    }

    /**
     *
     */
    public function getProfile(){
        return $this->hasOne(TblProfile::className(),['user_id' => 'id']);
    }
}
