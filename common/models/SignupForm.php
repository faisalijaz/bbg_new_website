<?php

namespace common\models;


use common\helpers\EmailHelper;
use yii\base\Model;

/**
 * Signup form
 * @property string $email
 * @property integer $amazing_offers
 * @property integer $occasional_updates
 * @property string $account_type
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property integer $gender
 * @property integer $country
 * @property string $socialType
 * @property string $socialID
 * @property string $picture;
 * @property string $phone_number;
 * @property string $country_code;
 * @property string $company_name;
 * @property string $company_logo;
 * @property string $company_location;
 * @property string $company_contact;
 * @property integer $package_id;
 */
class SignupForm extends Model
{
    public $email;
    public $first_name;
    public $last_name;
    public $dob;
    public $account_type;
    public $country;
    public $gender;
    public $amazing_offers;
    public $occasional_updates;
    public $password;
    public $socialType;
    public $picture;
    public $phone_number;
    public $country_code;
    public $socialID;
    public $group_id;
    public $experience_selected;
    public $payment_type;
    public $amount_paid;
    public $currency;
    public $adults;
    public $children;

    // Company Informaiton
    public $company_name;
    public $company_email;
    public $company_logo;
    public $company_location;
    public $company_contact;
    public $package_id;
    public $experience_group;
    public $cc_emails;
    public $discount;
    public $booked_by;
    public $parent_id;
    public $walk_in_customer = false;

    /**
     * SCENARIO
     * */
    const SCENARIO_SELF = 'self';
    const SCENARIO_FACEBOOK = 'facebook';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            [['email', 'first_name', 'phone_number'], 'required'],
            ['email', 'trim'],
            ['email', 'email'],

            [['password', 'email'], 'required', 'on' => self::SCENARIO_SELF],

            [['socialType', 'socialID'], 'required', 'on' => self::SCENARIO_FACEBOOK],

            [['email', 'company_email'], 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Accounts', 'message' => 'This email address has already been taken.'],

            [['children', 'experience_group', 'amount_paid', 'experience_selected', 'picture', 'phone_number', 'country_code', 'dob', 'country', 'gender', 'cc_emails', 'last_name', 'company_name', 'company_logo', 'company_location', 'company_contact', 'package_id'], 'safe'],
            [['group_id'], 'integer'],

            [['amazing_offers', 'occasional_updates'], 'default', 'value' => 0],

            [['account_type'], 'default', 'value' => 'customer'],

            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new Accounts();

        $user->attributes = $this->attributes;
        $user->amazing_offers = ($this->amazing_offers ? 1 : 0);
        $user->occasional_updates = ($this->occasional_updates ? 1 : 0);
        $user->gender = ($this->gender == 'male' ? 1 : 0);
        $user->verification_code = \Yii::$app->security->generateRandomString() . '_' . time();

        $user->phone_verification = (string)((int)microtime(true));


        if ($this->scenario == self::SCENARIO_FACEBOOK) {
            $this->password = $this->socialID;
        }

        $user->setPassword($this->password);
        $user->generateAuthKey();

        if ($user->save()) {
            // Send email to customer
            $this->sendEmail($user);

            // If booking source is walk-in customer then save booking after signup of user
            if ($this->walk_in_customer) {

                \Yii::$app->session->set('customerSessionId', $user->id);

                $booking = new TourBookings([
                    'booking_by' => 'staff',
                    'price' => $this->amount_paid,
                    'total_sum' => $this->amount_paid,
                    'discount' => $this->discount,
                    'payment_type' => 'COD',
                    'account_id' => $user->id,
                    'booking_source' => 'walk_in',
                    'booked_user_name' => $this->booked_by,
                    'booked_user_id' => \Yii::$app->user->id,
                    'status' => 'created'
                ]);


                if ($booking->save()) {

                    $experience = TourPriceMapping::findOne($this->experience_group);
                    $bookingDetails = new TourBookingDetails([
                        'booking_id' => $booking->id,
                        'tour_id' => $experience->tour_id,
                        'price' => $this->amount_paid,
                        'check_in' => \Yii::$app->dateTime->getDate(),
                        'adults' => $this->adults,
                        'childern' => $this->children,
                        'adult_price' => ($this->adults * $experience->rate),
                        'child_price' => ($this->children * $experience->child_price),
                        'package_id' => $experience->packages->id,
                        'isMapped' => 1,
                    ]);

                    if ($bookingDetails->save()) {
                        $checkIn = new TourCheckin();
                        if (!$checkIn->SaveCheckin(['check_in_type' => 'walk_in', 'booking_details_id' => $bookingDetails->id, 'resource_id' => \Yii::$app->request->get('resource')])) {
                            $this->addErrors($checkIn->getErrors());
                            return false;
                        }
                        return $user;
                    } else {
                        $this->addErrors($bookingDetails->getErrors());
                        return false;
                    }
                } else {
                    $this->addErrors($booking->getErrors());
                    return false;
                }
            }
            return $user;
        } else {
            return null;
        }
    }

    /**
     * Send an email to user
     * */
    public function sendEmail($user)
    {

        (new EmailHelper())->sendEmail($user->email, [], 'Welcome to toursDubai!', 'account/signup', ['user' => $user]);
    }
}
