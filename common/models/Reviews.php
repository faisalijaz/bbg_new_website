<?php

namespace common\models;

use common\helpers\EmailHelper;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $tour_id
 * @property string $rating
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $status
 *
 * @property Tours $tour
 * @property Accounts $account
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['account_id', 'tour_id', 'rating', 'description'], 'required'],
            [['account_id', 'tour_id', 'created_at', 'updated_at'], 'integer'],
            [['status', 'description'], 'string'],
            [['rating'], 'string', 'max' => 255],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tours::className(), 'targetAttribute' => ['tour_id' => 'id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account ID'),
            'tour_id' => Yii::t('app', 'Tour ID'),
            'rating' => Yii::t('app', 'Rating'),
            'description' => Yii::t('app', 'Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'tour_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::className(), ['id' => 'account_id']);
    }

    public function SendAdminAlert($data)
    {
        (new EmailHelper())->sendEmail(Yii::$app->appSettings->getByAttribute('admin_email'), [], 'New Review!', 'tour/review-alert', ['data' => $data]);
    }
}
