<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "accounts".
 *
 * @property integer $id
 * @property string $email
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $amazing_offers
 * @property integer $occasional_updates
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $account_type
 * @property string $first_name
 * @property string $last_name
 * @property string $dob
 * @property integer $gender
 * @property integer $country
 * @property string $socialType
 * @property string $socialID
 * @property string $verification_code
 * @property string $phone_verification
 * @property string $picture
 * @property string $phone_number
 * @property string $country_code
 * @property string $group_id
 * @property integer $agent_parent
 * @property string $cc_emails
 *
 * @property AccountAddress[] $accountAddress
 * @property AccountBookings[] $accountBookings
 * @property EventsRegistered[] $eventsRegistered
 */
class Accounts extends ActiveRecord implements IdentityInterface
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_INVOICED = 2;
    const STATUS_DELETED = 3;
    const STATUS_RENEWAL_REQUIRED = 4;

    public $summary;
    public $compnayData;
    public $user_docs;

     /**
     * @inheritdoc
     * @return string
     */
    public static function tableName()
    {
        return 'accounts';
    }

    /**
     * Extra fields
     * @return  string | array
     * */
    public function extraFields()
    {
        return [
            'accountCountry'
        ];
    }

    /**
     * Fields
     * @return array
     * */
    public function fields()
    {
        $fields = parent::fields();
        $fields[] = 'summary';
        return $fields;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['email', 'auth_key', 'first_name', 'password_hash'], 'required'],
            [['status', 'amazing_offers', 'occasional_updates', 'created_at', 'updated_at', 'gender', 'country', 'group_id', 'parent_id'], 'integer'],

            [['account_type', 'cc_emails', 'socialType', 'verification_code', 'phone_verification', 'cc_emails'], 'string'],

            [['socialType'], 'default', 'value' => 0],

            [['last_renewal', 'expiry_date', 'registeration_date'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],

            [['dob', 'country_code'], 'safe'],
            [['email', 'password_hash', 'password_reset_token', 'first_name', 'last_name', 'socialID'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 64],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
            [['created_at', 'updated_at', 'picture', 'summary'], 'safe'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert) {
            $this->account_type = "member";
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->compnayData <> null) {

            $company = new AccountCompany();
            $company->attributes = $this->compnayData;

            if (!$company->save()) {
                $this->addError('company_error', $company->getErrors());
                return false;
            }

            $this->company = $company->id;
            $this->save();
        }

        if ($this->user_docs <> null) {

            $mem_docs = new MembershipDocuments();
            $mem_docs->attributes = $this->user_docs;
            $mem_docs->account_id = $this->id;

            if (!$mem_docs->save()) {
                $this->addError('company_error', $mem_docs->getErrors());
                return false;
            }
        }
    }

    /**
     * @param int|string $id to find
     * @return static
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @param mixed $token token
     * @param null $type type of user
     * @return static
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    /**
     * @param string $username username
     * @return static
     */
    public static function findByUsername($username)
    {
        return static::findOne(['email' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @param string $token user authToken
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * Generates new password reset token
     * @return string
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new password reset token
     * @return string
     */
    public function setVerificationCode()
    {
        $this->verification_code = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     * @return integer
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     * @param  string $authKey authentication key for user
     * @return string
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password password of user
     * @return string
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     * @return string
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    /**
     * Removes password reset token
     * @return string
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Get User Details
     * @param string $username username
     * @return  string | array
     * */
    public function getUserDetails($username)
    {
        $user = static::findByEmail($username);

        return ['user_id' => $user->getId()];
    }

    /**
     * Finds user by username
     *
     * @param string $email email
     *
     * @return null|static
     *
     * @author Nadeem Akhtar <nadeem@myswich.com>
     *
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Check User Credentials
     * @param string $username username
     * @param string $password Password of user
     * @return string
     * */
    public function checkUserCredentials($username, $password)
    {
        $user = static::findByEmail($username);
        if (empty($user)) {
            return false;
        }

        return $user->validatePassword($password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviews()
    {
        return $this->hasMany(Reviews::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoyalties()
    {
        return $this->hasMany(Loyalty::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(Groups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountAddress()
    {
        return $this->hasMany(Address::className(), ['account_id' => 'id']);
    }

    /**
     * @return string | array
     * @param integer $type type
     */
    public function findAllAccounts($type = '')
    {
        if ($type) {
            return Accounts::find()->where(['account_type' => $type])->count();
        } else {
            return 0;
        }
    }

    /**
     * @inheritdoc
     * @return  string | array
     */
    public function getPayments()
    {
        return $this->hasMany(Payments::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountBookings()
    {
        return $this->hasMany(TourBookings::className(), ['account_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountCompany()
    {
        return $this->hasOne(AccountCompany::className(), ['id' => 'company']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventsRegistered()
    {
        return $this->hasMany(EventSubscriptions::className(), ['registered_by' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountContacts()
    {
        return $this->hasMany(Accounts::className(), ['parent_id' => 'id']);
    }

    public function getPointOfContacts()
    {
        return $this->hasMany(PointOfContact::className(), ['user_id' => 'id']);
    }
}
