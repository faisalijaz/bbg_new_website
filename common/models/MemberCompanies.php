<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "member_companies".
 *
 * @property integer $id
 * @property integer $member_id
 * @property integer $member_company
 * @property string $date_added
 */
class MemberCompanies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'member_companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['member_id', 'member_company', 'date_added'], 'required'],
            [['member_id', 'member_company'], 'integer'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'member_id' => 'Member ID',
            'member_company' => 'Member Company',
            'date_added' => 'Date Added',
        ];
    }
}
