<?php

namespace common\models;

/**
 * This is the model class for table "clients_reviews".
 *
 * @property integer $id
 * @property string $rating
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $status
 */
class ClientsReviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clients_reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['status', 'description',], 'string'],
            [['rating',  'client_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rating' => 'Rating',
            'description' => 'Description',
            'client_name' => 'Client Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }
}
