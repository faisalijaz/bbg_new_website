<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_financials".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $invoice_id
 * @property string $amount
 * @property string $type
 * @property string $description
 * @property string $date
 */
class UserFinancials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_financials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'invoice_id', 'amount', 'type', 'description'], 'required'],
            [['user_id', 'invoice_id'], 'integer'],
            [['amount'], 'number'],
            [['type', 'description'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'invoice_id' => 'Invoice ID',
            'amount' => 'Amount',
            'type' => 'Type',
            'description' => 'Description',
            'date' => 'Date',
        ];
    }
}
