<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_profile".
 *
 * @property int $id
 * @property int $user_id
 * @property string $timestamp
 * @property string $privacy
 * @property string $title
 * @property string $lastname
 * @property string $firstname
 * @property int $show_friends
 * @property int $allow_comments
 * @property string $email
 * @property resource $secondery_email
 * @property string $street address
 * @property string $city
 * @property string $about
 * @property string $country
 * @property string $emirate
 * @property string $createDate
 * @property string $modifiedDate
 * @property string $companyname
 * @property string $companytype
 * @property string $pobox
 * @property string $phone
 * @property string $fax
 * @property string $mobile_number
 * @property string $nationality
 * @property string $companyurl
 * @property string $linkedinurl
 * @property string $hide_in_listing
 * @property string $position
 * @property string $ukurl
 * @property int $department
 * @property int $abc
 * @property resource $send_enquiry
 * @property string $subscribe_newsletter
 * @property string $howyouknow
 * @property string $member_notes
 * @property string $company_navisionnumber
 * @property string $address
 * @property string $twitterurl
 * @property string $syncevents
 * @property string $company_other_type
 * @property string $paymentMethod
 * @property string $vat_number
 */
class TblProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'show_friends', 'allow_comments', 'department', 'abc'], 'integer'],
            [['timestamp', 'createDate', 'modifiedDate'], 'safe'],
            [['privacy', 'about', 'hide_in_listing', 'address'], 'string'],
            [['title', 'syncevents'], 'string', 'max' => 10],
            [['lastname', 'firstname'], 'string', 'max' => 50],
            [['email', 'secondery_email', 'street', 'city', 'country', 'companyname', 'companytype', 'pobox', 'nationality', 'companyurl', 'linkedinurl', 'position'], 'string', 'max' => 255],
            [['emirate', 'send_enquiry', 'subscribe_newsletter', 'company_navisionnumber'], 'string', 'max' => 25],
            [['phone', 'fax', 'mobile_number'], 'string', 'max' => 45],
            [['ukurl', 'howyouknow', 'member_notes', 'paymentMethod', 'vat_number'], 'string', 'max' => 250],
            [['twitterurl'], 'string', 'max' => 1000],
            [['company_other_type'], 'string', 'max' => 150],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'timestamp' => Yii::t('app', 'Timestamp'),
            'privacy' => Yii::t('app', 'Privacy'),
            'title' => Yii::t('app', 'Title'),
            'lastname' => Yii::t('app', 'Lastname'),
            'firstname' => Yii::t('app', 'Firstname'),
            'show_friends' => Yii::t('app', 'Show Friends'),
            'allow_comments' => Yii::t('app', 'Allow Comments'),
            'email' => Yii::t('app', 'Email'),
            'secondery_email' => Yii::t('app', 'Secondery Email'),
            'street' => Yii::t('app', 'Street'),
            'city' => Yii::t('app', 'City'),
            'about' => Yii::t('app', 'About'),
            'country' => Yii::t('app', 'Country'),
            'emirate' => Yii::t('app', 'Emirate'),
            'createDate' => Yii::t('app', 'Create Date'),
            'modifiedDate' => Yii::t('app', 'Modified Date'),
            'companyname' => Yii::t('app', 'Companyname'),
            'companytype' => Yii::t('app', 'Companytype'),
            'pobox' => Yii::t('app', 'Pobox'),
            'phone' => Yii::t('app', 'Phone'),
            'fax' => Yii::t('app', 'Fax'),
            'mobile_number' => Yii::t('app', 'Mobile Number'),
            'nationality' => Yii::t('app', 'Nationality'),
            'companyurl' => Yii::t('app', 'Companyurl'),
            'linkedinurl' => Yii::t('app', 'Linkedinurl'),
            'hide_in_listing' => Yii::t('app', 'Hide In Listing'),
            'position' => Yii::t('app', 'Position'),
            'ukurl' => Yii::t('app', 'Ukurl'),
            'department' => Yii::t('app', 'Department'),
            'abc' => Yii::t('app', 'Abc'),
            'send_enquiry' => Yii::t('app', 'Send Enquiry'),
            'subscribe_newsletter' => Yii::t('app', 'Subscribe Newsletter'),
            'howyouknow' => Yii::t('app', 'Howyouknow'),
            'member_notes' => Yii::t('app', 'Member Notes'),
            'company_navisionnumber' => Yii::t('app', 'Company Navisionnumber'),
            'address' => Yii::t('app', 'Address'),
            'twitterurl' => Yii::t('app', 'Twitterurl'),
            'syncevents' => Yii::t('app', 'Syncevents'),
            'company_other_type' => Yii::t('app', 'Company Other Type'),
            'paymentMethod' => Yii::t('app', 'Payment Method'),
            'vat_number' => Yii::t('app', 'Vat Number'),
        ];
    }

    public function getCity(){
        return $this->hasOne(TblCity::className(),['id' => 'city']);
    }
}
