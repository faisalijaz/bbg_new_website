<?php

namespace common\models;

use common\helpers\EmailHelper;
use Yii;

/**
 * This is the model class for table "payments".
 *
 * @property int $id
 * @property int $user_id
 * @property int $invoice_id
 * @property double $amount
 * @property string $amount_payable
 * @property int $transaction_id
 * @property string $response_code
 * @property string $detail
 * @property string $payment_date
 * @property string $status
 * @property string $payment_method
 * @property int $received_by
 * @property string $shipping_address
 * @property string $shipping_city
 * @property string $shipping_country
 * @property string $shipping_state
 * @property string $shipping_postalcode
 * @property string $currency
 * @property string $phone_num
 * @property string $customer_name
 * @property string $email
 */
class Payments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payments';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'invoice_id', 'transaction_id', 'received_by'], 'integer'],
            [['amount', 'transaction_id', 'response_code'], 'required'],
            [['amount', 'amount_payable'], 'number'],
            [['detail', 'status', 'payment_method', 'shipping_address', 'shipping_city', 'shipping_country', 'shipping_state', 'shipping_postalcode', 'currency', 'phone_num', 'customer_name', 'email'], 'string'],
            [['payment_date'], 'safe'],
            [['first_4_digits', 'last_4_digits', 'card_brand', 'secure_sign', 'response_code', 'shipping_address', 'shipping_city', 'shipping_country', 'shipping_state', 'shipping_postalcode', 'currency', 'phone_num', 'customer_name', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'amount' => Yii::t('app', 'Amount'),
            'amount_payable' => Yii::t('app', 'Amount Payable'),
            'transaction_id' => Yii::t('app', 'Transaction ID'),
            'response_code' => Yii::t('app', 'Response Code'),
            'detail' => Yii::t('app', 'Detail'),
            'payment_date' => Yii::t('app', 'Payment Date'),
            'status' => Yii::t('app', 'Status'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'received_by' => Yii::t('app', 'Received By'),
            'shipping_address' => Yii::t('app', 'Shipping Address'),
            'shipping_city' => Yii::t('app', 'Shipping City'),
            'shipping_country' => Yii::t('app', 'Shipping Country'),
            'shipping_state' => Yii::t('app', 'Shipping State'),
            'shipping_postalcode' => Yii::t('app', 'Shipping Postalcode'),
            'currency' => Yii::t('app', 'Currency'),
            'phone_num' => Yii::t('app', 'Phone Num'),
            'customer_name' => Yii::t('app', 'Customer Name'),
            'email' => Yii::t('app', 'Email'),
        ];
    }


    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /***
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if (count(Yii::$app->session->get('event_invoices')) > 0) {
            foreach (Yii::$app->session->get('event_invoices') as $id) {
                /*
                 * $invoice = InvoiceItems::findOne(['invoice_id' => $id]);
                 * $invoice->payment_id = $this->id;
                 * $invoice->save();
                */
            }
        }

    }

    public function paytabs_make_payment($response){

        if ($response <> null && count($response) > 0) {

            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {

                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);

                if ($invoice <> null) {

                    $invoice->payment_status = 'paid';
                    if($response['transaction_amount'] < $invoice->total){
                        $invoice->payment_status = 'partialy_paid';
                    }

                    if($invoice->invoice_related_to == "event"){

                        $subscriptions = EventSubscriptions::find()->where(['gen_invoice' => $invoice_id])->all();

                        foreach ($subscriptions as $subscription){
                            $subscription->payment_status = 'paid';
                            $subscription->save();
                        }
                    }

                    if($invoice->invoice_related_to == "renewal"){

                        $member = Members::findOne($invoice->invoice_rel_id);
                        $member->last_renewal = $response['trans_date'];
                        $member->expiry_date = date("Y-m-d", strtotime($response['trans_date'] . " +1 year"));
                        $member->renw_invoice_gen = 0;
                        $member->save();
                    }

                    $invoice->save();
                }

                $payment = Payments::findOne(['invoice_id' => $invoice_id]);
                if ($payment == null) {
                    $payment = new Payments();
                }

                $payment->invoice_id = (isset($response['order_id'])) ? $response['order_id'] : "";
                $payment->transaction_id = (isset($response['transaction_id'])) ? $response['transaction_id'] : "";
                $payment->response_code = (isset($response['response_code'])) ? $response['response_code'] : "";
                $payment->currency = (isset($response['transaction_currency'])) ? $response['transaction_currency'] : "";
                $payment->amount = (isset($response['transaction_amount'])) ? $response['transaction_amount'] : "";
                $payment->transaction_id = (isset($response['response_code'])) ? $response['response_code'] : "";
                $payment->detail = (isset($response['response_message'])) ? $response['response_message'] : "";
                $payment->status = 'done';
                $payment->payment_date = (isset($response['trans_date'])) ? $response['trans_date'] : "";
                $payment->user_id = Yii::$app->user->id;

                $payment->customer_name = (isset($response['customer_name'])) ? $response['customer_name'] : "";
                $payment->email = (isset($response['customer_email'])) ? $response['customer_email'] : "";
                $payment->phone_num = (isset($response['customer_phone'])) ? $response['customer_phone'] : "";
                $payment->last_4_digits = (isset($response['last_4_digits'])) ? $response['last_4_digits'] : "";
                $payment->first_4_digits = (isset($response['first_4_digits'])) ? $response['first_4_digits'] : "";
                $payment->card_brand = (isset($response['card_brand'])) ? $response['card_brand'] : "";

                $payment->shipping_address = (isset($response['address_shipping'])) ? $response['address_shipping'] : "";
                $payment->shipping_city = (isset($response['city_shipping'])) ? $response['city_shipping'] : "";
                $payment->shipping_country = (isset($response['country_shipping'])) ? $response['country_shipping'] : "";
                $payment->shipping_state = (isset($response['state_shipping'])) ? $response['state_shipping'] : "";
                $payment->shipping_postalcode = (isset($response['postal_code_shipping'])) ? $response['postal_code_shipping'] : "";
                $payment->secure_sign = (isset($response['secure_sign'])) ? $response['secure_sign'] : "";

                if($payment->save()){
                    (new EmailHelper())->sendEmail($payment->email,[],'Payment Recieved','payments/receipt_pdf',[ 'payment'  => $payment ]);
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $amount
     * @param $returnurl
     * @param $user_id
     * @param int $payment_for
     * @param string $invoice_id
     * @return string
     */
    public function payment_migs($amount, $returnurl, $user_id, $payment_for = 1, $invoice_id = '')
    {

        $trans_ref = $user_id . "-" . Yii::app()->session['payment_session_data']; // add by jawwad for Query DR
        //
        $trans_ref = $user_id . "-" . $invoice_id;

        $array_value = array("Title" => "BBG Payment",
            "vpc_AccessCode" => "AE042719",  //3D47999E
            "vpc_Amount" => $amount,
            "vpc_Command" => "pay",
            "vpc_Locale" => "en",
            "vpc_MerchTxnRef" => $trans_ref,
            "vpc_Merchant" => "001113654636",//TEST001113654636
            "vpc_OrderInfo" => $payment_for,
            "vpc_ReturnURL" => $returnurl,
            "vpc_TicketNo" => "",
            "vpc_TxSourceSubType" => "",
            "vpc_Version" => 1,
            "virtualPaymentClientURL" => "https://migs.mastercard.com.au/vpcpay",
            'vpc_SecureHashType' => 'SHA256'
        );

        $SECURE_SECRET = "A542B5BA4B191C67582D3DF7F772B4E0";//21D6AF25E0F3CC8AB09CEAB579302512
        $vpcURL = $array_value["virtualPaymentClientURL"] . "?";
        $md5HashData = $SECURE_SECRET;

        ksort($array_value);
        $appendAmp = 0;

        foreach ($array_value as $key => $value) {
            if (strlen($value) > 0) {
                if ($appendAmp == 0) {
                    $vpcURL .= urlencode($key) . '=' . urlencode($value);
                    $appendAmp = 1;
                } else {
                    $vpcURL .= '&' . urlencode($key) . "=" . urlencode($value);
                }
                $md5HashData .= $value;
            }
        }

        if (strlen($SECURE_SECRET) > 0) {
            $vpcURL .= "&vpc_SecureHash=" . $this->getHash($array_value, $SECURE_SECRET);
        }

        try { // transaction log.
            $paymentrequestLog = new PaymentrequestLog;

            $paymentrequestLog->vpc_MerchTxnRef = $trans_ref;
            $paymentrequestLog->vpc_Amount = $amount;
            $paymentrequestLog->vpc_Command = "pay";
            $paymentrequestLog->vpc_OrderInfo = $payment_for;
            $paymentrequestLog->vpcURL = $returnurl;
            $paymentrequestLog->save();

        } catch (Exception $e) {
            $e;
        }

        return $vpcURL;
    }

    /**
     * @param $card
     * @param $amount
     * @param $payment_id
     * @param $event
     * @return array
     * @throws \Exception
     */
    public function make_payment($card, $amount, $payment_id, $event)
    {
        $url = 'https://migs.mastercard.com.au/vpcdps';
        $curl_post_data = [

            'vpc_Version' => '1',
            'vpc_Command' => 'pay',
            'vpc_AccessCode' => '3D47999E',  //'3D47999E',
            'vpc_MerchTxnRef' => $payment_id . " - PaymentId",
            "vpc_Merchant" => "TEST001113654636",
            'vpc_OrderInfo' => 'Event Registration',
            'vpc_Amount' => $amount * 100,
            'vpc_Currency' => 'AED',
            'vpc_ReturnAuthResponseData' => 'Y',
            'vpc_SecureHashType' => 'SHA256',
            "vpc_Locale" => "en",
            'vpc_CardNum' => "4005550000000001",
            'vpc_cardExp' => "1305",
            'vpc_CardSecurityCode' => $card['card_security_token']

            /* 'vpc_CardNum' => $card['card_number'],
             'vpc_cardExp' => $card['card_exp_year'] . $card['card_exp_month'],
             'vpc_CardSecurityCode' => $card['card_security_token']*/

            // "vpc_ReturnURL" => 'http://bbg.new/events/payment?id=29',
            //'vpc_MerchantId' => 'TEST001113654636',   //'TEST001113654636',
        ];

        $curl_post_data['vpc_SecureHash'] = $this->getHash($curl_post_data, "21D6AF25E0F3CC8AB09CEAB579302512");

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($curl_post_data));
        $response = curl_exec($ch);
        // var_export($response);

        $data = [];

        $res = explode('&', $response);

        foreach ($res as $r) {

            $index = explode('=', $r);
            $data[$index[0]] = $index[1];
        }

        /* echo '<pre>';
         print_r($data);
         die();
         */
        $this->updatePayment($data);

        if ($data['vpc_TxnResponseCode'] == 0) {
            return ['success', $this->getResponseDescription($data['vpc_TxnResponseCode'])];
        }

        return ['error', $this->getResponseDescription($data['vpc_TxnResponseCode'])];
    }

    /**
     * @param $data
     * @param $secret
     * @return string
     */
    protected function getHash($data, $secret)
    {
        ksort($data);
        $hash = null;
        foreach ($data as $k => $v) {
            if (in_array($k, array('vpc_SecureHash', 'vpc_SecureHashType'))) {
                continue;
            }
            if ((strlen($v) > 0) && ((substr($k, 0, 4) == "vpc_") || (substr($k, 0, 5) == "user_"))) {
                $hash .= $k . "=" . $v . "&";
            }
        }
        $hash = rtrim($hash, "&");
        return strtoupper(hash_hmac('SHA256', $hash, pack('H*', $secret)));
    }

    /**
     * @param $responseCode
     * @return string
     */
    public function getResponseDescription($responseCode)
    {

        switch ($responseCode) {
            case "0" :
                $result = "Transaction Successful. Thank you for your payment. To view your Payment Receipt, please check your email.";
                break;
            case "?" :
                $result = "Transaction status is unknown";
                break;
            case "1" :
                $result = "Unknown Error";
                break;
            case "2" :
                $result = "Bank Declined Transaction";
                break;
            case "3" :
                $result = "Transaction Declined- No reply from Bank";
                break;
            case "4" :
                $result = "Transaction Declined- Expired Card";
                break;
            case "5" :
                $result = "Transaction Declined- Insufficient funds";
                break;
            case "6" :
                $result = "Transaction Declined - Bank system error";
                break;
            case "7" :
                $result = "Payment Server Processing Error - Typically caused by invalid input data such as an invalid credit card number. Processing errors can also occur. (This is only relevant for Payment Servers that enforce the  uniqueness of this field) Processing errors can also occur.";
                break;
            case "8" :
                $result = " Transaction Declined - Transaction Type Not Supported";
                break;
            case "9" :
                $result = "Bank declined transaction (Do not contact Bank)";
                break;
            case "A" :
                $result = "Transaction Aborted";
                break;
            case "B" :
                $result = 'Transaction Blocked - Returned when: - The Verification Security Level has a value of "07". B - If the merchant has 3-D Secure Blocking enabled, the transaction will not proceed. - The overall risk assessment result returns a "Reject" or "System Reject".';
                break;
            case "C" :
                $result = "Transaction Cancelled";
                break;
            case "D" :
                $result = "Deferred transaction has been received and is awaiting processing";
                break;
            case "F" :
                $result = "3D Secure Authentication failed";
                break;
            case "I" :
                $result = "Card Security Code verification failed";
                break;
            case "L" :
                $result = "Shopping Transaction Locked (Please try the transaction again later)";
                break;
            case "N" :
                $result = "Cardholder is not enrolled in Authentication scheme";
                break;
            case "P" :
                $result = "Transaction has been received by the Payment Adaptor and is being processed";
                break;
            case "R" :
                $result = "Transaction was not processed - Reached limit of retry attempts allowed";
                break;
            case "S" :
                $result = "Duplicate SessionID (OrderInfo)";
                break;
            case "T" :
                $result = "Address Verification Failed";
                break;
            case "U" :
                $result = "Card Security Code Failed";
                break;
            case "V" :
                $result = "Address Verification and Card Security Code Failed";
                break;
            default  :
                $result = "Unable to be determined";
        }
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentUser()
    {
        return $this->hasOne(Members::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentReceivedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'received_by']);
    }

    public function getInvoice()
    {
        return $this->hasOne(Invoices::className(), ['invoice_id' => 'invoice_id']);
    }
}
