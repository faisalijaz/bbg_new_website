<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "point_of_contact".
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $email
 * @property resource $number
 * @property string $designation
 * @property string $third_field
 * @property string $create_date
 */
class PointOfContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'point_of_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['create_date'], 'safe'],
            [['name', 'number', 'designation', 'third_field'], 'string', 'max' => 25],
            [['email'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'number' => Yii::t('app', 'Number'),
            'designation' => Yii::t('app', 'Designation'),
            'third_field' => Yii::t('app', 'Third Field'),
            'create_date' => Yii::t('app', 'Create Date'),
        ];
    }

    public function getMember(){
        $this->hasOne(Members::className(),['id' => 'user_id']);
    }
}
