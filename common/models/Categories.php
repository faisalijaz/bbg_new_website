<?php

namespace common\models;
use backend\models\Tours;
/**
 * This is the model class for table "categories".
 *
 * @property integer $id
 * @property string $title
 * @property string $short_description
 * @property string $description
 * @property string $image
 * @property string $baner_image
 * @property integer $parent_id
 * @property string $meta_title
 * @property string $meta_description
 * @property integer $status
 *
 * @property Categories $parent
 * @property Categories[] $categories
 */
class Categories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title' , 'type'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['type'], 'string'],
            [['title', 'short_description', 'description', 'image', 'baner_image', 'meta_title', 'meta_description'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'type' => 'Type',
            'short_description' => 'Short Description',
            'description' => 'Description',
            'image' => 'Image',
            'baner_image' => 'Baner Image',
            'parent_id' => 'Parent ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Categories::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Categories::className(), ['parent_id' => 'id']);
    }


    public function getCompanies(){
        return $this->hasMany(AccountCompany::className(), ['category' => 'id']);
    }
}
