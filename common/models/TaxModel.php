<?php

namespace common\models;

use yii\base\Model;
use yii\web\NotFoundHttpException;
/**
 * Class TaxModel
 * @package common\models
 */
class TaxModel extends Model
{
    public $taxes;

    /**
     * SCENARIO
     * */
    const SCENARIO_AGENT = 'agent';
    const SCENARIO_CUSTOMER = 'customer';

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['taxes'], 'integer'],
            [['taxes'], 'safe'],
        ];
    }

    /**
     * @param integer $TaxGroupId Tax Group Id against Item
     * @return string
     */
    public function findItemTax($TaxGroupId)
    {
        $taxGroup = $this->FindTaxGroup($TaxGroupId);

        foreach ($taxGroup->taxGroupRates as $groupRate) {
            $toatlTax[] = [
                'title' => $groupRate->tax->title,
                'rate' => $groupRate->tax->rate,
                'type' => $groupRate->tax->type
            ];
        }

        return $toatlTax;
    }

    /**
     * @param integer $value Tax Group Id
     * @return array|null|\yii\db\ActiveRecord
     */
    protected function FindTaxGroup($value)
    {
        $model = TaxGroups::find()->where(['id' => $value])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
