<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * This is the model class for table "event_subscriptions".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $user_type
 * @property integer $registered_by
 * @property integer $event_id
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $userNote
 * @property string $mobile
 * @property string $company
 * @property string $walkin
 * @property string $attended
 * @property string $fee_paid
 * @property string $payment_status
 * @property string $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $gen_invoice
 * @property string $focGuest
 * @property string $paymentMethod
 *
 * @property MemberData[] = $memberData
 * @property InvoiceData[] = $InvoiceData
 * @property EventData[] = $eventData
 */
class EventSubscriptions extends \yii\db\ActiveRecord
{
    public $invoice;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event_subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'event_id', 'created_at', 'updated_at'], 'integer'],
            [['user_type', 'user_type_relation', 'userNote', 'walkin', 'attended', 'payment_status', 'status', 'focGuest', 'paymentMethod'], 'string'],
            [['registered_by', 'gen_invoice', 'fee_paid', 'tax_group_id', 'tax', 'subtotal', 'cancel_request'], 'number'],
            [['firstname', 'lastname', 'mobile'], 'string', 'max' => 25],
            [['email'], 'email'],
            [['company'], 'string', 'max' => 150],
            [['invoice'],  'safe']
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Member',
            'user_type' => 'User Type',
            'registered_by' => 'Registered By',
            'event_id' => 'Event ID',
            'firstname' => 'First Name',
            'lastname' => 'Last Name',
            'email' => 'Email',
            'userNote' => 'User Note',
            'mobile' => 'Mobile',
            'company' => 'Company',
            'walkin' => 'Walkin',
            'attended' => 'Attended',
            'fee_paid' => 'Fee Paid',
            'payment_status' => 'Payment Status',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'gen_invoice' => 'Gen Invoice',
            'focGuest' => 'Foc Guest',
            'paymentMethod' => 'Payment Method',
            'user_type_relation' => 'User Type'
        ];
    }

    /**
     * @param string $q
     * @return mixed
     */
    public function getMembersList($q = '')
    {

        $query = new Query;

        $query->select('first_name')
            ->from('accounts')
            ->where('name LIKE "%' . $q . '%"')
            ->orderBy('first_name');
        $command = $query->createCommand();
        $data = $command->queryAll();
        $out = [];
        foreach ($data as $d) {
            $out[] = ['value' => $d['first_name']];
        }
        return Json::encode($out);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        //if ($insert) {

            $event = Events::findOne($this->event_id);
            $subscriptions = EventSubscriptions::find()->where(['event_id' => $this->event_id])->count();

            if ($event <> null && $subscriptions < $event->group_size) {

                if ($this->user_type == 'guest') {
                    $this->fee_paid = ($event <> null) ? $event->nonmember_fee : 0;
                }

                if ($this->focGuest) {
                    $this->fee_paid = $event->focus_chair_fee;
                }

                if ($this->fee_paid == 0) {
                    $this->payment_status = 'paid';
                }

                if ($this->walkin) {
                    $this->attended = 1;
                    $this->status = 'approved';
                }

                $this->subtotal = $this->fee_paid;
                $this->tax = $this->itemTaxInformation($this->tax_group_id, $this->fee_paid);
                $this->fee_paid = $this->subtotal + $this->tax;

            } else {
                $this->addError('error', 'Your registration cannot proceed all seats are reserved!');
                return false;
            }

        // }
        return parent::beforeSave($insert);
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {

        if ($this->gen_invoice) {

            $invoiceItems = new InvoiceItems();

            $invoiceItems->invoice_id = $this->gen_invoice;
            $invoiceItems->user_id = $this->user_id;
            $invoiceItems->invoice_related_to = 'event';
            $invoiceItems->invoice_rel_id = $this->id;
            $invoiceItems->invoice_category = $this->eventData->title;
            $invoiceItems->payment_status = 'unpaid';

            if ($this->payment_status == 'paid') {
                $invoiceItems->payment_status = 'paid';
            }

            $invoiceItems->amount = $this->fee_paid;
            $invoiceItems->subtotal = $this->subtotal;
            $invoiceItems->tax = $this->tax;
            $invoiceItems->invoice_date = date('Y-m-d');

            if (!$invoiceItems->save()) {
                $this->addError('error', $invoiceItems->getErrors());
            }

            if($this->invoice){

                $updateInv = Invoices::findOne($this->invoice);

                $updateInv->subtotal = $this->subtotal;
                $updateInv->tax = $this->tax;
                $updateInv->total = $this->fee_paid;

                if (!$updateInv->save()) {
                    return ['errors' => \yii\widgets\ActiveForm::validate($updateInv)];
                }
            }

        }

        if ($this->payment_status == 'paid') {

            $eventInvoice = Invoices::findOne($this->gen_invoice);
            $eventInvoice->payment_status = 'paid';
            $eventInvoice->save();

        }

        if ($this->focGuest) {

        }

    }

    public function memberEventRegisteration($data)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

            $html = '';
            $count = 1;
            $subtotal = 0;
            $tax_group = 1;

            if (count($data) > 0) {

                $invoice = new Invoices();
                $invoice->user_id = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                $invoice->invoice_related_to = 'event';
                $invoice->invoice_rel_id = \Yii::$app->request->post('event_id');
                $invoice->invoice_category = "Event Registration";
                $invoice->payment_status = 'unpaid';

                if (!$invoice->save()) {
                    print_r($invoice->getErrors());
                    die();
                }

                $registered_ids = [];

                if (isset($data['contact']) && count($data['contact'])) {

                    $contacts = $data['contact'];
                    $member_fee = $data['contactfee'];

                    if (count($contacts) > 0) {

                        foreach ($contacts as $id) {

                            $user = Members::findOne($id);

                            $model = new EventSubscriptions();

                            $model->event_id = \Yii::$app->request->post('event_id');
                            $model->registered_by = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                            $model->user_id = $id;
                            $model->user_type = 'member';
                            $model->firstname = $user->first_name;
                            $model->lastname = $user->last_name;
                            $model->email = $user->email;
                            $model->mobile = $user->phone_number;
                            $model->company = ($user->companyData <> null) ? $user->companyData->name : '';
                            $model->fee_paid = $data['member_fee'];
                            $model->tax_group_id = 1;
                            $model->gen_invoice = $invoice->invoice_id;

                            if (!$model->save()) {
                                echo '<pre>';
                                print_r($model->getErrors());
                                die();
                            }

                            $html .= $this->makeHtml($model->firstname . ' ' . $model->lastname, 'Member', $model->subtotal, $model->tax, $model->fee_paid, $model->id, $count);

                            $count++;
                            $subtotal = $subtotal + $model->fee_paid;
                        }
                    }
                }

                if (isset($data['guestInfo']) && count($data['guestInfo']) > 0) {

                    $guestInfo = $data['guestInfo'];

                    foreach ($guestInfo as $guest) {
                        $name = '';

                        if ($guest['name'] <> "") {

                            if (isset($guest['name'])) {
                                $name = explode(' ', $guest['name']);
                            }

                            $model = new EventSubscriptions();

                            $model->event_id = $data['event_id'];
                            $model->registered_by = (!Yii::$app->user->isGuest) ? \Yii::$app->user->identity->id : 0;
                            $model->user_type = 'guest';
                            $model->firstname = (isset($name[0])) ? $name[0] : "";
                            $model->lastname = (isset($name[1])) ? $name[1] : "";
                            $model->email = $guest['email'];
                            $model->mobile = $guest['phone_number'];
                            $model->company = $guest['company'];
                            $model->fee_paid = $data['nonmember_fee'];
                            $model->tax_group_id = 1;
                            $model->gen_invoice = $invoice->invoice_id;

                            if (!$model->save()) {
                                return ['errors' => \yii\widgets\ActiveForm::validate($model)];
                            }

                            $html .= $this->makeHtml($model->firstname . ' ' . $model->lastname, 'Member', $model->subtotal, $model->tax, $model->fee_paid, $model->id, $count);

                            $count++;
                            $subtotal = $subtotal + $model->fee_paid;;
                        }
                    }
                }

                /* $updateInv = Invoices::findOne($invoice->invoice_id);

                $updateInv->subtotal = $subtotal;
                $updateInv->tax = $this->itemTaxInformation($model->tax_group_id, $subtotal);
                $updateInv->total = $updateInv->subtotal + $updateInv->tax;

                if (!$updateInv->save()) {
                    return ['errors' => $updateInv->getErrors()];
                }

                */

                $transaction->commit();

                return $invoice->invoice_id;

            }

            return ['msg' => true, 'users' => $html,'total' => $subtotal];

        } catch (\Exception $exception) {

            $transaction->rollback();
            throw $exception;
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEventData()
    {
        return $this->hasOne(Events::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoices::className(), ['invoice_id' => 'gen_invoice']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMember()
    {
        return $this->hasOne(Members::className(), ['id' => 'user_id']);
    }


    public function makeHtml($name, $type, $subtotal, $tax, $amount, $id, $serial)
    {
        $html = '  <tr>';
        $html .= '     <td class="text-center">' . $serial . '</td>';
        $html .= '     <td class="text-center">' . $name . '</td>';
        $html .= '     <td class="text-center">' . ucwords($type) . '</td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $subtotal . '</span></td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $tax . '</span></td>';
        $html .= '     <td class="text-center"><span class="pull-right pricebox">' . $amount . '</span></td>';
        $html .= '     <td class="text-center">';
        $html .= '         <span class="Editbtn">';
        $html .=                Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', '#', [
                                    'class' => 'showModalButton',
                                    'value' => \yii\helpers\Url::to(['/event/event-subscribers-badges', 'event_id' => 1]),
                                    'title' => Yii::t('yii', 'Edit'),
                                ]);
        $html .= '         </span>';
        $html .= '         <span class="delbtn">';
        $html .=                Html::a('<i class="fa fa-trash-o" aria-hidden="true"></i>', '#', [
                                    'class' => 'showModalButton',
                                    'value' => \yii\helpers\Url::to(['/event/event-subscribers-badges', 'event_id' => 1]),
                                    'title' => Yii::t('yii', 'Delete'),
                                ]);
        $html .= '         </span>';
        $html .= '     </td>';
        $html .= '</tr>';
        return $html;
    }

    /**
     * @param $tax_group
     * @param $price
     * @return float|int
     */
    public function itemTaxInformation($tax_group, $price)
    {
        $taxModel = new TaxModel();
        $itemTax = 0;

        if ($tax_group) {
            $taxes = $taxModel->findItemTax($tax_group);
            foreach ($taxes as $tax) {
                if ($tax['type'] == 'percentage') {
                    $taxAmount = ($tax['rate'] * $price) / 100;
                } else {
                    $taxAmount = $tax['rate'];
                }
                $itemTax = $taxAmount + $itemTax;
            }
            return $itemTax;
        } else {
            return $itemTax;
        }
    }
}
