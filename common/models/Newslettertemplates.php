<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "newslettertemplates".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property int $event_id
 * @property int $onlySubscription
 * @property string $category
 * @property string $testemail
 * @property string $image
 * @property string $active
 * @property string $createDate
 * @property string $subject
 */
class Newslettertemplates extends \yii\db\ActiveRecord
{
    public $news;
    public $categories;
    public $subscribers;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'newslettertemplates';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['subject'], 'required'],
            [['id', 'event_id', 'onlySubscription', 'active'], 'integer'],
            [['description', 'content', 'category'], 'string'],
            [['createDate', 'news', 'categories', 'subscribers'], 'safe'],
            [['title', 'subject'], 'string', 'max' => 250],
            [['testemail', 'image'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'content' => Yii::t('app', 'Content'),
            'event_id' => Yii::t('app', 'Event ID'),
            'onlySubscription' => Yii::t('app', 'Only Subscription'),
            'category' => Yii::t('app', 'Category'),
            'testemail' => Yii::t('app', 'Testemail'),
            'image' => Yii::t('app', 'Image'),
            'active' => Yii::t('app', 'Active'),
            'createDate' => Yii::t('app', 'Create Date'),
            'subject' => Yii::t('app', 'Subject'),
        ];
    }
}
