<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "special_events_fee".
 *
 * @property integer $id
 * @property integer $event_id
 * @property string $fee_type
 * @property string $amount
 */
class SpecialEventsFee extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'special_events_fee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_id', 'fee_type', 'amount'], 'required'],
            [['event_id'], 'integer'],
            [['amount'], 'number'],
            [['fee_type'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_id' => 'Event ID',
            'fee_type' => 'Fee Type',
            'amount' => 'Amount',
        ];
    }
}
