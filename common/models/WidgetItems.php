<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "widget_items".
 *
 * @property integer $id
 * @property integer $widget_id
 * @property integer $tour_id
 * @property string $display_type
 * @property integer $sort_order
 *
 * @property Tours $tour
 * @property Widgets $widget
 */
class WidgetItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'widget_items';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['widget_id', 'tour_id'], 'required'],
            [['widget_id', 'tour_id', 'sort_order'], 'integer'],
            [['display_type'], 'string', 'max' => 255],
            [['tour_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tours::className(), 'targetAttribute' => ['tour_id' => 'id']],
            [['widget_id'], 'exist', 'skipOnError' => true, 'targetClass' => Widgets::className(), 'targetAttribute' => ['widget_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'widget_id' => Yii::t('app', 'Widget ID'),
            'tour_id' => Yii::t('app', 'Tour ID'),
            'display_type' => Yii::t('app', 'Display Type'),
            'sort_order' => Yii::t('app', 'Sort Order'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTour()
    {
        return $this->hasOne(Tours::className(), ['id' => 'tour_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWidget()
    {
        return $this->hasOne(Widgets::className(), ['id' => 'widget_id']);
    }
}
