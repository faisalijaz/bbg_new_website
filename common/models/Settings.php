<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 * All general applicaiton will be stored in this table
 *
 * @property integer $id
 * @property string $app_name
 * @property string $app_owner
 * @property string $admin_email
 * @property string $app_url
 * @property string $from_email
 * @property string $address
 * @property string $app_logo
 * @property string $footer_logo
 * @property string $currency
 * @property string $location
 * @property string $Geocode
 * @property string $telephone
 * @property string $copyright_text
 * @property string $about
 * @property string $facebook
 * @property string $twitter
 * @property string $instagram
 * @property string $youtube
 * @property string $meta_title
 * @property string $meta_tag
 * @property string $meta_tag_description
 * @property string $smtp_email
 * @property string $smtp_username
 * @property string $smtp_password
 * @property string $smtp_hash
 * @property integer $smtp_port
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     * @return mixed
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function rules()
    {
        return [
            [['app_name', 'admin_email', 'from_email', 'event_email', 'membership_email', 'app_logo', 'meta_title'], 'required'],
            [['admin_email', 'from_email', 'event_email', 'membership_email'], 'email'],
            /*[['youtube_icon', 'linkedin_icon', 'pinterest_icon', 'facebook_icon', 'instagram_icon' , 'twitter_icon'], 'required'],*/
            [['location', 'fax'], 'safe'],
            [['about', 'invoice_payment_info', 'smtp_hash', 'youtube','youtube_icon' , 'linkedin', 'linkedin_icon' , 'pinterest', 'pinterest_icon' , 'facebook', 'facebook_icon' , 'twitter', 'twitter_icon' ,'instagram', 'instagram_icon' , 'app_url'], 'string'],
            [['smtp_port', 'grid_size_width', 'grid_size_height', 'listing_size_height', 'listing_size_width', 'additional_image_size_width', 'additional_image_size_height', 'popup_size_height', 'popup_size_width', 'page_banner_size_width', 'page_banner_size_height', 'home_banner_size_height', 'home_banner_size_width'], 'integer'],
            [['points_on_spent', 'points_of_spent', 'cancel_grace_period', 'min_redeem_amount'], 'integer'],
            [['app_name', 'app_vat', 'app_owner', 'admin_email', 'from_email', 'address', 'app_logo', 'footer_logo', 'currency', 'Geocode', 'telephone', 'copyright_text', 'meta_title', 'meta_tag', 'meta_tag_description', 'smtp_email', 'smtp_username', 'smtp_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'app_name' => Yii::t('app', 'App Name'),
            'app_owner' => Yii::t('app', 'App Owner'),
            'admin_email' => Yii::t('app', 'Admin Email'),
            'from_email' => Yii::t('app', 'From Email'),
            'app_url' => Yii::t('app', 'App URL'),
            'address' => Yii::t('app', 'Address'),
            'app_logo' => Yii::t('app', 'App Logo'),
            'footer_logo' => Yii::t('app', 'Footer Logo'),
            'currency' => Yii::t('app', 'Currency'),
            'location' => Yii::t('app', 'Location'),
            'Geocode' => Yii::t('app', 'Geocode'),
            'telephone' => Yii::t('app', 'Telephone'),
            'fax' => Yii::t('app', 'Telephone'),
            'copyright_text' => Yii::t('app', 'Copyright Text'),
            'about' => Yii::t('app', 'About'),
            'instagram' => Yii::t('app', 'Instagram URL'),
            'youtube' => Yii::t('app', 'Youtube URL'),
            'twitter' => Yii::t('app', 'Twitter URL'),
            'facebook' => Yii::t('app', 'Facebook URL'),
            'linkedin' => Yii::t('app', 'LinkedIn URL'),
            'pinterest' => Yii::t('app', 'Pinterest URL'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_tag' => Yii::t('app', 'Meta Tag'),
            'meta_tag_description' => Yii::t('app', 'Meta Tag Description'),
            'smtp_email' => Yii::t('app', 'Smtp Email'),
            'smtp_username' => Yii::t('app', 'Smtp Username'),
            'smtp_password' => Yii::t('app', 'Smtp Password'),
            'smtp_hash' => Yii::t('app', 'Smtp Hash'),
            'smtp_port' => Yii::t('app', 'Smtp Port'),
            'grid_size_width' => Yii::t('app', 'Grid Image Width'),
            'grid_size_height' => Yii::t('app', 'Grid Image Height'),
            'listing_size_width' => Yii::t('app', 'Listing Image Width'),
            'listing_size_height' => Yii::t('app', 'Listing Image Height'),
            'additional_image_size_width' => Yii::t('app', 'Additional Image Width'),
            'additional_image_size_height' => Yii::t('app', 'Additional Image Height'),
            'popup_size_width' => Yii::t('app', 'PopUp Width'),
            'popup_size_height' => Yii::t('app', 'PopUp Height'),
            'page_banner_size_width' => Yii::t('app', 'Page Banner Image Width'),
            'page_banner_size_height' => Yii::t('app', 'Page Banner Image Height'),
            'home_banner_size_width' => Yii::t('app', 'Home Page Banner Image Width'),
            'home_banner_size_height' => Yii::t('app', 'Home Page Banner Image Height'),
        ];
    }
}
