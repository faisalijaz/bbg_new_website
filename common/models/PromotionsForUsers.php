<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "promotions_for_users".
 *
 * @property integer $id
 * @property string $promotion_code_id
 * @property string $user_account_id
 */
class PromotionsForUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'promotions_for_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotion_code_id'], 'required'],
            [['promotion_code_id', 'user_account_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promotion_code_id' => 'Promotion Code ID',
            'user_account_id' => 'User Account ID',
        ];
    }
}
