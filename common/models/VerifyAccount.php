<?php

namespace common\models;

use common\helpers\EmailHelper;
use yii\base\Model;
use yii\base\InvalidParamException;

/**
 * Password reset form
 */
class VerifyAccount extends Model
{
    /**
     * @var \common\models\User
     */
    private $_user;


    /**
     * Creates a form model given a token.
     *
     * @param string $token
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @throws \yii\base\InvalidParamException if token is empty or not valid
     */
    public function __construct($token, $config = [])
    {
        if (empty($token) || !is_string($token)) {
            throw new InvalidParamException('Password reset token cannot be blank.');
        }

        $this->_user = Accounts::find()->where(['verification_code' => $token])->one();

        if ( !$this->_user || empty($this->_user->verification_code) ) {
            throw new InvalidParamException('Wrong verification code.');
        }
        parent::__construct($config);
    }

    /**
     * Verify account.
     *
     * @return boolean if password was reset.
     */
    public function verify()
    {
        $account = $this->_user;
        $account->status = Accounts::STATUS_ACTIVE;
        $account->verification_code = null;
        return $account->save(false);
    }
}
