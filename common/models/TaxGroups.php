<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tax_groups".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property integer $status
 *
 * @property TaxGroupRate[] $taxGroupRates
 */
class TaxGroups extends \yii\db\ActiveRecord
{

    /**
     * @var array
     */
    public $rates;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_groups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'status'], 'required'],
            [['status'], 'integer'],
            [['rates'], 'safe'],
            [['description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxGroupRates()
    {
        return $this->hasMany(TaxGroupRate::className(), ['tax_group_id' => 'id']);
    }

    /**
     * @param type $insert data to insert
     * @param type $changedAttributes Data attributes
     * @return mixed
     */
    public function afterSave($insert, $changedAttributes)
    {
        TaxGroupRate::deleteAll(['tax_group_id' => $this->id]);
        foreach ($this->rates as $tax) {
            $taxRates = new TaxGroupRate();
            $taxRates->attributes = $tax;
            $taxRates->tax_group_id = $this->id;
            $taxRates->save();
        }
    }
}
