<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "committe_members".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $designation
 * @property string $email
 * @property string $phonenumber
 * @property string $image
 */
class CommitteMembers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'committe_members';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'designation'], 'required'],
            [['full_name', 'designation', 'email', 'phonenumber', 'image','type'], 'string', 'max' => 255],
            [['sort_order'], 'number'],
            [['description','status'],'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'designation' => 'Designation',
            'email' => 'Email',
            'phonenumber' => 'Phonenumber',
            'image' => 'Image',
            'type' => 'Type',
            'sort_order' => 'Sort Order',
            'description' => 'Description',
            'status' => 'Status'
        ];
    }
}
