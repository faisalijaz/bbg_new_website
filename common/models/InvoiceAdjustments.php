<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "invoice_adjustments".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $adjusted_by
 * @property string $adjustment
 * @property string $type
 * @property string $reason
 * @property string $date_added
 */
class InvoiceAdjustments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_adjustments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'adjusted_by', 'adjustment', 'type', 'reason'], 'required'],
            [['invoice_id', 'adjusted_by'], 'integer'],
            [['adjustment'], 'number'],
            [['type', 'reason'], 'string'],
            [['date_added'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invoice_id' => Yii::t('app', 'Invoice ID'),
            'adjusted_by' => Yii::t('app', 'Adjusted By'),
            'adjustment' => Yii::t('app', 'Adjustment'),
            'type' => Yii::t('app', 'Type'),
            'reason' => Yii::t('app', 'Reason'),
            'date_added' => Yii::t('app', 'Date Added'),
        ];
    }


}
