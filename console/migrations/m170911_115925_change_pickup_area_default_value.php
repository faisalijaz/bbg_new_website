<?php

use yii\db\Migration;

class m170911_115925_change_pickup_area_default_value extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_booking_pickup}}', 'pickup_area', 'INT(11) null  DEFAULT "0" ');
    }

    public function safeDown()
    {
    }
}