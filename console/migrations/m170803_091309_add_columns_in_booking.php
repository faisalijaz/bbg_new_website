<?php

use yii\db\Migration;

class m170803_091309_add_columns_in_booking extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_bookings}}', 'booking_by', 'ENUM("agent", "customer", "staff") DEFAULT "customer"');
        $this->addColumn('{{%tour_bookings}}', 'booking_source', 'ENUM("online", "walk_in") DEFAULT "online"');
        $this->addColumn('{{%tour_bookings}}', 'booking_staff_name', 'VARCHAR(512)');
        $this->addColumn('{{%tour_bookings}}', 'booking_staff_id', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_bookings}}', 'booking_source');
        $this->dropColumn('{{%tour_bookings}}', 'booking_staff_name');
        $this->dropColumn('{{%tour_bookings}}', 'booking_staff_id');
    }
}
