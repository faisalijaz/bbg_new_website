<?php

use yii\db\Migration;

class m170606_180911_destination extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%destinations}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50)->notNull(),
            'tag' => $this->string(50),
            'capacity' => $this->smallInteger()->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%destinations}}');
    }
}
