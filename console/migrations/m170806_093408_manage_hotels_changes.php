<?php

use yii\db\Migration;

class m170806_093408_manage_hotels_changes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('hotel_area', '{{%hotels}}', 'area');
        $this->createIndex('hotel_city', '{{%hotels}}', 'city');
        $this->addForeignKey('hotel_area_fk', '{{%hotels}}', 'area', '{{%areas}}', 'id');
        $this->addForeignKey('hotel_city_fk', '{{%hotels}}', 'city', '{{%cities}}', 'id');

    }

    public function safeDown()
    {

    }
}
