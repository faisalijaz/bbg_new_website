<?php

use yii\db\Migration;

/**
 * Class m170624_070147_tour_destinations
 */
class m170624_070147_tour_destinations extends Migration
{
    /**
     * Add column
     */
    public function safeUp()
    {
        $this->addColumn('{{%tours}}', 'tour_destination', 'INT(11)');

        $this->createIndex('tour_destination', '{{%tours}}', 'tour_destination');
        $this->addForeignKey('tour_destination_fk', '{{%tours}}', 'tour_destination', '{{%destinations}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     *drop column
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tours}}', 'tour_destination');
    }
}
