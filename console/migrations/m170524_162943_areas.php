<?php

use yii\db\Migration;

class m170524_162943_areas extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%areas}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'city_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0)
        ], $tableOptions);

        $this->createIndex('area_city', '{{%areas}}', 'city_id');
        $this->addForeignKey('area_city_fk', '{{%areas}}', 'city_id', 'cities', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%areas}}');
    }

}
