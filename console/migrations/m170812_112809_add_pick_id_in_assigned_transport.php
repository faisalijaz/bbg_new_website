<?php

use yii\db\Migration;

class m170812_112809_add_pick_id_in_assigned_transport extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%assign_transport}}', 'pickup_ref', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%assign_transport}}', 'pickup_ref');
    }
}
