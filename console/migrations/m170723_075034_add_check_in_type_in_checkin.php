<?php

use yii\db\Migration;

class m170723_075034_add_check_in_type_in_checkin extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_checkin}}', 'checkin_type', 'ENUM("online_booking","walk_in") DEFAULT "online_booking"');
    }

    public function safeDown()
    {
        $this->addColumn('{{%tour_checkin}}', 'checkin_type');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170723_075034_add_check_in_type_in_checkin cannot be reverted.\n";

        return false;
    }
    */
}
