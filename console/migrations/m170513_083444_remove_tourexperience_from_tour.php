<?php

use yii\db\Migration;

class m170513_083444_remove_tourexperience_from_tour extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->dropColumn('{{%tours}}', 'experience_id');
        $this->addColumn('{{%tour_price_mapping}}', 'experience_id', 'INT(11) NOT NULL DEFAULT 0');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->addColumn('{{%tours}}', 'experience_id', 'INT(11) NOT NULL DEFAULT 0');
        $this->dropColumn('{{%tour_price_mapping}}', 'experience_id');
    }
}
