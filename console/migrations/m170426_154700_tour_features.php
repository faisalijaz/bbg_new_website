<?php

use yii\db\Migration;

class m170426_154700_tour_features extends Migration
{

    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_features}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'feature_id' => $this->integer()->notNull(),
            'price' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('tour_feature', '{{%tour_features}}', 'tour_id');
        $this->addForeignKey( 'tour_feature_fk', '{{%tour_features}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('feature', '{{%tour_features}}', 'feature_id');
        $this->addForeignKey( 'feature_fk', '{{%tour_features}}', 'feature_id', '{{%features}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_features}}');
    }
}
