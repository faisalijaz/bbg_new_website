<?php

use yii\db\Migration;

class m170219_100041_user_widget extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeUp()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user_widget}}', [
            'id'                => $this->primaryKey(),
            'widget_id'         => $this->integer()->notNull(),
            'user_id'           => $this->integer()->notNull(),
            'sort_order'        => $this->integer()->defaultValue(0),
            'screen_size'       => "ENUM('full','half') DEFAULT 'half'",
        ], $tableOptions);


        $this->createIndex('user_widget_index', '{{%user_widget}}', 'user_id');
        $this->createIndex('widget_index', '{{%user_widget}}', 'widget_id');


        $this->addForeignKey('user_widget_FK', '{{%user_widget}}', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('widget_FK', '{{%user_widget}}', 'widget_id', 'widgets', 'id', 'CASCADE', 'CASCADE');


    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeDown()
    {
        $this->dropTable('{{%user_widget}}');
    }
}
