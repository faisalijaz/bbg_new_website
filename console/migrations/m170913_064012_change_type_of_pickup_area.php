<?php

use yii\db\Migration;

class m170913_064012_change_type_of_pickup_area extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_booking_pickup}}', 'pickup_area', 'VARCHAR(512)');
    }

    public function safeDown()
    {
    }
}
