<?php

use yii\db\Migration;

class m170719_132631_addition_in_destination_resources extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%destination_resources}}', 'capacity', 'INT(11) DEFAULT 0');
    }

    public function safeDown()
    {
        $this->addColumn('{{%destination_resources}}', 'capacity');
    }
}
