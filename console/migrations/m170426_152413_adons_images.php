<?php

use yii\db\Migration;

class m170426_152413_adons_images extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%addon_images}}', [
            'id' => $this->primaryKey(),
            'addon_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'image' => $this->string(),
            'description' => $this->string(),
            'sort_order' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('addon_image', '{{%addon_images}}', 'addon_id');
        $this->addForeignKey( 'addon_image_fk', '{{%addon_images}}', 'addon_id', '{{%addons}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%addon_images}}');
    }
}
