<?php

use yii\db\Migration;

class m170426_152118_tour_images extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_images}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'title' => $this->string(),
            'image' => $this->string(),
            'description' => $this->string(),
            'sort_order' => $this->integer()->defaultValue(0),
        ], $tableOptions);

        $this->createIndex('tour_image', '{{%tour_images}}', 'tour_id');
        $this->addForeignKey( 'tour_image_fk', '{{%tour_images}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_images}}');
    }
}
