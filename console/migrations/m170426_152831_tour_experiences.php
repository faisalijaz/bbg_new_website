<?php

use yii\db\Migration;

class m170426_152831_tour_experiences extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tour_experiences}}', [
            'id' => $this->primaryKey(),
            'tour_id' => $this->integer()->notNull(),
            'experience_id' => $this->integer()->notNull(),
            'price' => $this->string(),
            'status' => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('tour_experience', '{{%tour_experiences}}', 'tour_id');
        $this->addForeignKey( 'tour_experience_fk', '{{%tour_experiences}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('experience_experience', '{{%tour_experiences}}', 'experience_id');
        $this->addForeignKey( 'experience_experience_fk', '{{%tour_experiences}}', 'experience_id', '{{%experiences}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_experiences}}');
    }

}
