<?php

use yii\db\Migration;

class m170711_145325_tour_carousl_homePage extends Migration
{
    public function safeUp()
    {
        $this->batchInsert(
            '{{%widgets}}',
            ['name', 'code', 'status', 'type'],
            [
                ['Tour Carousel', 'tour-main-carousel', 1, 'frontend'],
            ]
        );
    }

    /**
     * Empty Table
     * @return integer
     */
    public function safeDown()
    {
    }
}
