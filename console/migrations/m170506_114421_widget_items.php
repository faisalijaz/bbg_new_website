<?php

use yii\db\Migration;

class m170506_114421_widget_items extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%widget_items}}', [
            'id' => $this->primaryKey(),
            'widget_id' => $this->integer()->notNull(),
            'tour_id' => $this->integer()->notNull(),
            'display_type' => $this->string()->defaultValue('col-lg-4 col-sm-12'),
            'sort_order' => $this->integer()->defaultValue(0),

        ], $tableOptions);

        $this->createIndex('widget_items', '{{%widget_items}}', 'widget_id');
        $this->addForeignKey( 'widget_items_fk', '{{%widget_items}}', 'widget_id', '{{%widgets}}', 'id', 'CASCADE', 'CASCADE');


        $this->createIndex('tour_widget', '{{%widget_items}}', 'tour_id');
        $this->addForeignKey( 'tour_widget_fk', '{{%widget_items}}', 'tour_id', '{{%tours}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%widget_items}}');
    }
}
