<?php

use yii\db\Migration;

class m170429_141442_update_about_content_in_settings extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->alterColumn('{{%settings}}', 'about', 'TEXT');
    }


    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->alterColumn('{{%settings}}', 'about', 'VARCHAR(255)');
    }
}
