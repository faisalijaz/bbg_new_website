<?php

use yii\db\Migration;

class m170629_141524_widget_latest_booking extends Migration
{
    /**
     * Insert Default Values to widgets table in order to create widgets
     * @return array
     */
    public function safeUp()
    {
        $this->execute('delete from user_widget');
        $this->execute('delete from widgets');
        $this->truncateTable('{{%widgets}}');

        $this->batchInsert(
            '{{%widgets}}',
            ['name', 'code', 'status', 'type'],
            [
                ['Home page banner items', 'Home-page-banner-items', 1, 'frontend'],
                ['Home page top listing items', 'Home-page-top-listing-items', 1, 'frontend'],
                ['Home page bottom items', 'Home-page-bottom-items', 1, 'frontend'],
                ['Total Customer', 'total_customer', 1, 'backend'],
                ['Today Customer', 'today_customer', 1, 'backend'],
                ['Total Members', 'total_agents', 1, 'backend'],
                ['Home page banner items', 'Home-page-banner-items', 1, 'frontend'],
                ['Home page top listing items', 'Home-page-top-listing-items', 1, 'frontend'],
                ['Home page bottom items', 'Home-page-bottom-items', 1, 'frontend'],
                ['Today Sales', 'today_sales', 1, 'backend'],
                ['Total Sales', 'total_sales', 1, 'backend'],
                ['Agent Sales', 'agent_sales', 1, 'backend'],
                ['Customer Sales', 'customer_sales', 1, 'backend'],
                ['Members Expired Limits', 'agents_with_expired_selling_limit', 1, 'backend'],
                ['Sales Comparison', 'sales_comparison', 1, 'backend'],
                ['Latest Bookings', 'latest_bookings', 1, 'backend']

            ]
        );
    }

    /**
     * Empty Table
     * @return integer
     */
    public function safeDown()
    {
        $this->execute('delete from user_widget');
        $this->execute('delete from widgets');
    }
}
