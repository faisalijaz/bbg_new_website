<?php

use yii\db\Migration;

class m170516_075001_loyalties extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%loyalties}}', [
            'id'                => $this->primaryKey(),
            'points'            => $this->integer()->notNull(),
            'action'            => "ENUM('+', '-') DEFAULT '+'",
            'account_id'        => $this->integer()->notNull(),
            'booking_id'        => $this->integer()->notNull(),
            'created_at'        => $this->timestamp()->notNull(),
            'updated_at'        => $this->timestamp()->notNull(),
            'point_type'        => "ENUM('preorder', 'referral', 'sharing') DEFAULT 'preorder'",
            'is_deleted'        => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%loyalties}}');
    }
}