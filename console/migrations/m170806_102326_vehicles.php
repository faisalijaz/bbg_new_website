<?php

use yii\db\Migration;

class m170806_102326_vehicles extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vehicles}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->null(),
            'registration_number' => $this->string()->null(),
            'status' => "ENUM('active', 'In-Active')",
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%vehicles}}');
    }
}
