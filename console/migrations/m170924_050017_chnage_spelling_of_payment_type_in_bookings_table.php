<?php

use yii\db\Migration;

class m170924_050017_chnage_spelling_of_payment_type_in_bookings_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%tour_bookings}}', 'payment_status', 'ENUM("pending","received") DEFAULT "pending"');
    }

    public function safeDown()
    {
    }


}
