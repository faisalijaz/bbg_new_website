<?php

use yii\db\Migration;

class m170531_045239_update_tour_booking_details extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%tour_booking_details}}', 'status', 'TINYINT(11) NOT NULL DEFAULT 1');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('tour_booking_details', '{{%status    }}');
    }
}
