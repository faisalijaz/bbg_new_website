<?php
use yii\db\Migration;

/**
 * Class m170601_031806_populate_widgets_table
 */
class m170601_031806_populate_widgets_table extends Migration
{
    /**
     * Insert Default Values to widgets table in order to create widgets
     * @return array
     */
    public function safeUp()
    {

        $this->batchInsert(
            '{{%widgets}}',
            ['name', 'code', 'status', 'type'],
            [

                [1, 'Total Customer', 'total_customer', 1, 'backend'],
                [2, 'Today Customer', 'today_customer', 1, 'backend'],
                [3, 'Total Members', 'total_agents', 1, 'backend'],
                [4, 'Home page banner items', 'Home-page-banner-items', 1, 'frontend'],
                [5, 'Home page top listing items', 'Home-page-top-listing-items', 1, 'frontend'],
                [6, 'Home page bottom items', 'Home-page-bottom-items', 1, 'frontend'],
                [7, 'Today Sales', 'today_sales', 1, 'backend'],
                [8, 'Total Sales', 'total_sales', 1, 'backend'],
                [9, 'Agent Sales', 'agent_sales', 1, 'backend'],
                [10, 'Customer Sales', 'customer_sales', 1, 'backend'],
                [11, 'Sales Comparison', 'sales_comparison', 1, 'backend']
            ]
        );
    }

    /**
     * Empty Table
     * @return integer
     */
    public function safeDown()
    {
        $this->truncateTable('{{%widgets}}');
    }
}
