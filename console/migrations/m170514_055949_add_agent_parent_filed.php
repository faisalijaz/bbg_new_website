<?php

use yii\db\Migration;

class m170514_055949_add_agent_parent_filed extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('{{%accounts}}', 'agent_parent', 'INT(11) DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%accounts}}', 'agent_parent');
    }
}
