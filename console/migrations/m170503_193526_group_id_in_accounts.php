<?php

use yii\db\Migration;

class m170503_193526_group_id_in_accounts extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
       $this->addColumn('{{%accounts}}', 'group_id', 'INT(11) NOT NULL DEFAULT 1');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('{{%accounts}}', 'group_id');
    }
}
