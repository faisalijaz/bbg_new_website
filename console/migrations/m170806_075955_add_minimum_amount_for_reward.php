<?php

use yii\db\Migration;

class m170806_075955_add_minimum_amount_for_reward extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%settings}}', 'min_redeem_amount', 'INT(11) DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%settings}}', 'min_redeem_amount');
    }
}
