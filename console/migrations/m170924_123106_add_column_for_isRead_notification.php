<?php

use yii\db\Migration;

class m170924_123106_add_column_for_isRead_notification extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_bookings}}','isRead','ENUM("0","1") DEFAULT "0"');
    }

    public function safeDown()
    {
    }
}
