<?php

use yii\db\Migration;

class m170809_113207_change_type_of_cc_emails extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%accounts}}','cc_emails','text');
    }

    public function safeDown()
    {

    }
}
