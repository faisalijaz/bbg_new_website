<?php

use yii\db\Migration;

class m170219_100015_widgets extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%widgets}}', [
            'id'                => $this->primaryKey(),
            'name'              => $this->string()->notNull(),
            'code'              => $this->string()->notNull(),
            'status'            => $this->smallInteger()->notNull()->defaultValue(1),
        ], $tableOptions);

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function safeDown()
    {
        $this->dropTable('{{%widgets}}');
    }
}
