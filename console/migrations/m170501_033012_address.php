<?php

use yii\db\Migration;

class m170501_033012_address extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%address}}', [
            'id' => $this->primaryKey(),
            'account_id' => $this->integer()->notNull(),
            'address' => $this->string()->notNull(),
            'street' => $this->string(),
            'area' => $this->string()->notNull(),
            'city' => $this->string()->notNull(),
            'country' => $this->string(),
        ], $tableOptions);

        $this->createIndex('address_account', '{{%address}}', 'account_id');
        $this->addForeignKey('address_account_fk', '{{%address}}', 'account_id', '{{%accounts}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%address}}');
    }
}
