<?php

use yii\db\Migration;

class m170617_082811_update_dates_types_in_loyalty extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->alterColumn('{{%loyalties}}', 'created_at', 'INT(11)');
        $this->alterColumn('{{%loyalties}}', 'updated_at', 'INT(11)');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->alterColumn('{{%loyalties}}', 'created_at', 'INT(11)');
        $this->alterColumn('{{%loyalties}}', 'updated_at', 'INT(11)');
    }
}
