<?php

use yii\db\Migration;

class m170514_090907_tax_rates extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tax_rates}}', [
            'id' => $this->primaryKey(),
            'title' =>$this->string()->notNull(),
            'rate' => $this->integer()->notNull(),
            'type' => "ENUM('fixed', 'percentage')",
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),

        ], $tableOptions);

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tax_rates}}');
    }
}
