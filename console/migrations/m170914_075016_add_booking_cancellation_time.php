<?php

use yii\db\Migration;

class m170914_075016_add_booking_cancellation_time extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_bookings}}', 'cancellation_time', 'TIME');
    }

    public function safeDown()
    {

    }


}
