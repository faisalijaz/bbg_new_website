<?php

use yii\db\Migration;

class m161004_022401_create_country extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        $this->createTable('{{%country}}', [
            'id' => $this->primaryKey(),
            'country_code' => $this->string()->notNull(),
            'country_name' => $this->string()->notNull()
        ], $tableOptions);

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%country}}');
    }
}
