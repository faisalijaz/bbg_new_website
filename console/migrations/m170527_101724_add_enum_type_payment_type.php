<?php

use yii\db\Migration;

class m170527_101724_add_enum_type_payment_type extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%tour_bookings}}', 'payment_type', "ENUM('CCO', 'COD', 'RP') DEFAULT 'CCO'");
    }

    public function down()
    {
        $this->alterColumn('{{%tour_bookings}}', 'payment_type', "ENUM('CCO', 'COD', 'RP') DEFAULT 'CCO'");
    }
}
