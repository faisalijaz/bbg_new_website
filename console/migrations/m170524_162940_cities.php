<?php

use yii\db\Migration;

class m170524_162940_cities extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%cities}}', [
            'id'   => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(0)
        ], $tableOptions);

        $this->createIndex('city_country', '{{%cities}}', 'country_id');
        $this->addForeignKey('city_country_fk', '{{%cities}}', 'country_id', 'country', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%cities}}');
    }
}
