<?php

use yii\db\Migration;

class m170624_092107_destination_image extends Migration
{
    public function up()
    {
        $this->addColumn('{{%destinations}}', 'image', 'VARCHAR(255) DEFAULT NULL');
    }

    public function down()
    {
        $this->dropColumn('{{%destinations}}', 'image');
    }
}
