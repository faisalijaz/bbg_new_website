<?php
use yii\db\Migration;

class m170709_080639_edit_payment_table_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%payments}}', 'reason', "INT(11)");
        $this->addColumn('{{%payments}}', 'payment_method', "ENUM('online', 'cash','cheque') DEFAULT 'online'");
        $this->addColumn('{{%payments}}', 'received_by', "INT(11)");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%payments}}', 'reason');
        $this->dropColumn('{{%payments}}', 'payment_method');
        $this->dropColumn('{{%payments}}', 'received_by');
    }
}
