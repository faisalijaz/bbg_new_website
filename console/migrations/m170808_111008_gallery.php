<?php

use yii\db\Migration;

class m170808_111008_gallery extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%gallery}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'image' => $this->string()->notNull(),
            'size' => "ENUM('small', 'large') DEFAULT  'small'",
            'position' => "ENUM('left', 'right') DEFAULT 'left'",
            'status' => "ENUM('active', 'In-Active') DEFAULT 'active'",
        ], $tableOptions);
   }

    public function safeDown()
    {
        $this->dropTable('{{%gallery}}');
    }
}
