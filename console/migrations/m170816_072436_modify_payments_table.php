<?php

use yii\db\Migration;

class m170816_072436_modify_payments_table extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%payments}}', 'transaction_no');
        $this->alterColumn('{{%payments}}', 'status', 'ENUM("done", "cancelled", "pending","authorized","captured","refunded","partially_refunded","failed") DEFAULT "pending"');

        $this->addColumn('{{%payments}}', 'currency', 'varchar(10)');
        $this->addColumn('{{%payments}}', 'email', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'ip_address', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'ip_country', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'pf_payment_id', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'pf_account_id', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'pf_auth_code', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'pf_token_id', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'created_at', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'updated_at', 'varchar(255)');

        $this->addColumn('{{%payments}}', 'card_id', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_last4', 'INT(4)');
        $this->addColumn('{{%payments}}', 'card_brand', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_exp_year', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_exp_month', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_name', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_pf_customer_id', 'varchar(255)');
        $this->addColumn('{{%payments}}', 'card_created_at', 'varchar(255)');
    }

    public function safeDown()
    {

    }

}
