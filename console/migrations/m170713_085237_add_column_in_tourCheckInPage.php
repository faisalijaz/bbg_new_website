<?php

use yii\db\Migration;

class m170713_085237_add_column_in_tourCheckInPage extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_checkin}}', 'booking_details_id', 'INT(11)');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%tour_checkin}}', 'booking_details_id');
    }
}
