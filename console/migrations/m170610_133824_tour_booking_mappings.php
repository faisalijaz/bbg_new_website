<?php

use yii\db\Migration;

class m170610_133824_tour_booking_mappings extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%tour_booking_mappings}}', [
            'id' => $this->primaryKey(),
            'destination_id' => $this->integer()->notNull(),
            'bookingId' => $this->integer()->notNull(),
            'status' => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('destination_id', '{{%tour_booking_mappings}}', 'destination_id');
        $this->addForeignKey('destination_id_fk', '{{%tour_booking_mappings}}', 'destination_id', '{{%destinations}}', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex('bookingId', '{{%tour_booking_mappings}}', 'bookingId');
        $this->addForeignKey('bookingId_fk', '{{%tour_booking_mappings}}', 'bookingId', '{{%tour_bookings}}', 'id', 'CASCADE', 'CASCADE');

    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropTable('{{%tour_booking_mappings}}');
    }
}
