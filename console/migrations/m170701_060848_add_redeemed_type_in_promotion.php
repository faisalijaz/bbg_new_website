<?php

use yii\db\Migration;

class m170701_060848_add_redeemed_type_in_promotion extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%promotions}}', 'redeem_type', "ENUM('single', 'multiple') DEFAULT 'single'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%promotions}}', 'redeem_type');
    }
}
