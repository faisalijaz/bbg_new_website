<?php

use yii\db\Migration;

class m170924_080349_add_columns_for_exclusive_bookings extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%tour_bookings}}', 'isExclusive', 'ENUM("0","1") DEFAULT "0"');
        $this->addColumn('{{%tour_booking_details}}', 'exclusive_title', 'VARCHAR(512)');
        $this->addColumn('{{%tour_booking_details}}', 'tour_time', 'time');
        $this->addColumn('{{%tour_booking_details}}', 'destination', 'INT(11)');
        $this->addColumn('{{%tour_booking_mappings}}', 'resource_assigned', 'ENUM("full","partial") DEFAULT "partial"');
    }

    public function safeDown()
    {
    }
}
