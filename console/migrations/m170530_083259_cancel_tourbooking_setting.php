<?php

use yii\db\Migration;

class m170530_083259_cancel_tourbooking_setting extends Migration
{
    /**
     * Up function will add migraiton into database table
     * @return boolean true or false
     * */
    public function up()
    {
        $this->addColumn('{{%settings}}', 'cancel_grace_period', 'TINYINT(11) NOT NULL DEFAULT 1');
    }

    /**
     * Down function will add migraiton into database table
     * @return boolean true or false
     * */
    public function down()
    {
        $this->dropColumn('settings', '{{%cancel_grace_period}}');
    }
}
