<?php

namespace backend\widgets\controls;

use common\models\UserWidget;
use Yii;
use yii\base\Widget;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class DashboardWidget extends Widget
{

    /**
     * Report View title
     * @var string
     */
    public $title = 'My Report';

    public $position = 'pull-right';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        DashboardWidgetAsset::register($this->getView());

        $model = new UserWidget();
        $model->user_id = Yii::$app->user->id;

        if (Yii::$app->request->isPost) {
            $inputs = Yii::$app->request->post();
            $model->load($inputs);

            UserWidget::deleteAll(['user_id' => $model->user_id]);

            foreach( $model->widgets as $widget ) {
                $userWidget = new UserWidget();
                $userWidget->user_id = $model->user_id;
                $userWidget->widget_id = $widget;

                $userWidget->save();
            }

            Yii::$app->controller->redirect(['site/index']);

        }

        return $this->render('dashboard-widgets', [
            'title' => $this->title,
            'model' => $model,
            'position' => $this->position,
        ]);
    }

}
