<?php

namespace backend\widgets\statistics\todaySales;

use app\models\AccountSearch;
use backend\models\TourBookingSearch;
use common\models\Accounts;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\Invoices;
use common\models\Members;
use common\models\TourBookings;
use Yii;
use yii\base\Widget;

/**
 * Class TodaySalesWidget
 * @package backend\widgets\statistics\todaySales
 */
class TodaySalesWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = '_view';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Today Sales';

    /**
     * Report View title
     * @var string
     */
    public $code = 'TS';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        TodaySalesWidgetAsset::register($this->getView());
        $data = 0;
        $sales = [];
        $searchModel = '';


        if ($this->code == 'total_members') {

            $data = Members::find()->count();
            $this->view = 'customer-view';

        } elseif ($this->code == 'today_members') {

            $data = Members::find()->where(['registeration_date' => date('Y-m-d')])->count();
            $this->view = 'customer-view';

        } elseif ($this->code == 'total_events') {

            $data = Events::find()->count();
            $this->view = 'customer-view';

        } elseif ($this->code == 'today_events') {

            $data = Events::find()->where(['event_startDate' => Yii::$app->dateTime->getDate()
            ])->count();
            $this->view = 'customer-view';

        } elseif ($this->code == 'past_events') {

            $data = Events::find()->where(['<=', 'event_startDate', date('Y-m-d')])->count();
            $this->view = 'customer-view';

        }
        elseif ($this->code == 'upcoming_events') {

            $data = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])->count();
            $this->view = 'customer-view';

        }

        elseif ($this->code == 'total_events_reg') {

            $data = EventSubscriptions::find()->count();
            $this->view = 'customer-view';

        } elseif ($this->code == 'today_events_reg') {

            $data = EventSubscriptions::find()->where([
                'FROM_UNIXTIME(created_at,"%Y-%m-%d")' => Yii::$app->dateTime->getDate()
            ])->count();
            $this->view = 'customer-view';

        }
        elseif ($this->code == 'pending_invoices') {

            $data = Invoices::find()->where(['payment_status' => 'unpaid'])->count();
            $this->view = 'sales-view';

        } elseif ($this->code == 'paid_invoices') {

            $data = Invoices::find()->where(['payment_status' => 'paid'])->count();
            $this->view = 'sales-view';

        }



        $data = [
            'data' => $data,
            'title' => $this->title
        ];

        return $this->render($this->view, [
            'data' => $data,
            'sales' => $sales,
            'searchModel' => $searchModel
        ]);
    }
}
