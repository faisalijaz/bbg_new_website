<div class="card-box">
    <div class="bar-widget">
        <div class="table-box">
            <div class="table-detail">
                <div class="iconbox bg-custom">
                    <i class="icon-trophy"></i>
                </div>
            </div>

            <div class="table-detail">
                <h4 class="m-t-0 m-b-5"><b><?= $data['title'] ?></b></h4>
                <h5 class="text-muted m-b-0 m-t-0"><?= $data['data']; ?></h5>
            </div>
            <div class="table-detail text-right">
                        <span data-plugin="peity-pie" data-colors="#5fbeaa,#ebeff2" data-width="50"
                              data-height="45">1/5</span>
            </div>

        </div>
    </div>
</div>