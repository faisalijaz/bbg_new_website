<div class="card-box">
    <h4 class="m-t-0 header-title"><b><?= $data['title'] ?></b></h4>
    <p class="text-muted m-b-30 font-13">
        <?= $data['title'] ?>
    </p>
    <div id="horizontal-bar-chart" class="ct-chart ct-golden-section"></div>
</div>


<?php
echo $this->registerJS('
        
       var agent_sales = ' . $sales['agent_sales'] . ';
       var cusotmer_sales = ' . $sales['cusotmer_sales'] . ';
    
       var $data  = [
  		    { y: "2017", a: cusotmer_sales,  b: agent_sales },
//        	{ y: "2016", a: 135,  b: 115 },
//        	{ y: "2015", a: 140,  b: 130 },
//          { y: "2014", a: 155,  b: 415 },
//          { y: "2013", a: 620,  b: 530 }, 
          ];
       // $.DashboardCRM.init($data);
       $.DashboardCRM.createBarChart("horizontal-bar-chart", $data, "y", ["a", "b"], ["Customer Sales ", "Agent Sales "], ["#81c868", "#ffbd4a"]);
  ');
?>