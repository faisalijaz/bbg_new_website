<?php

namespace app\models;

use backend\models\BookingItem;
use backend\models\Tours;
use common\helpers\EmailHelper;
use common\models\Accounts;
use common\models\Payments;
use common\models\TourBookingDetails;
use common\models\TourBookingMappings;
use common\models\TourBookings;
use Yii;
use yii\base\Model;
use yii\behaviors\TimestampBehavior;

/**
 * Class CheckoutModel
 * @package common\models
 */
class CheckoutModel extends Model
{
    public $total_sum;
    public $total;
    public $total_tax;
    public $discount;
    public $promoCode;
    public $payment_type;
    public $account_id;
    public $address_id;
    public $address;
    public $account;
    public $cart;
    public $special_instructions;
    public $price;
    public $cardNumber;
    public $expiryMonth;
    public $expiryYear;
    public $cvCode;
    public $amount_paid;
    public $amount_to_collect;
    public $cancellation_time;
    public $booking_status;
    /**
     * SCENARIO
     * */
    const SCENARIO_GUEST = 'guest';
    const SCENARIO_CUSTOMER = 'customer';

    public function init()
    {
        parent::init();

        //Set cart object to checkout model to access everything from tehre.
        $this->cart = (new BookingItem());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [

            [['payment_type'], 'required'],
            ['payment_type', 'in', 'range' => ['RP', 'COD', 'CCO', 'CP']],

            // Check if customer is not signup then he has provide informaiton related to profile & address
            [['payment_type'], 'required', 'on' => self::SCENARIO_GUEST],

            // Check if customer is logedn then he has to select his . her address from frontend
            // [['address_id', 'account_id'], 'required', 'on' => self::SCENARIO_CUSTOMER],
            ['address_id', 'compare', 'compareValue' => 0, 'operator' => '>', 'on' => self::SCENARIO_CUSTOMER],

            [['cardNumber', 'expiryMonth', 'expiryYear', 'cvCode'], 'required', 'when' => function ($model) {
                return $model->payment_type == 'CCO';
            }],

            [['promoCode'], 'safe'],

            [['price', 'total_sum', 'total_tax', 'total', 'discount'], 'number'],

            [['cardNumber', 'expiryMonth', 'expiryYear', 'cvCode'], 'number'],
            [['cardNumber', 'addons', 'CP', 'customer', 'address', 'cvCode', 'expiryMonth', 'expiryYear', 'payment_type', 'address_id', 'cart', 'special_instructions', 'amount_paid', 'amount_to_collect', 'cancellation_time', 'booking_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     * @return array of attribute labels
     */
    public function attributeLabels()
    {
        return [
            'total_sum' => Yii::t('app', 'Total Sum'),
            'total' => Yii::t('app', 'Total'),
            'total_tax' => Yii::t('app', 'Taxes'),
            'discount' => Yii::t('app', 'Discounts'),
            'promoCode' => Yii::t('app', 'Promo Code'),
            'payment_type' => Yii::t('app', 'Payment Type'),
            'account_id' => Yii::t('app', 'Account'),
            'address_id' => Yii::t('app', 'Address')
        ];
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // check if user is signed up & is seller
        if (!Yii::$app->user->isGuest) {
            if (\Yii::$app->session->get('customerSessionId')) {
                $user = Accounts::findOne(\Yii::$app->session->get('customerSessionId'));
            } else {
                $user = Yii::$app->user->getIdentity();
            }

            if ($user->account_type == 'seller') {
                $totalBookingAmount = $this->getSellerBookingLimit($user->id, $user->paymentCycle);

                if ($totalBookingAmount >= $user->account_type) {
                    $this->addError('limit_exceed', 'You already used available limit.');
                    Yii::$app->session->setFlash('warning', 'You already used available limit.');
                    return false;
                }
            }
        }

        //Calculate if seats are available or not. TODO: Need improvements
        $cart_items = Yii::$app->session->get('adminCartItems');
        foreach ($cart_items as $cart_item) {
            $tour_id = $cart_item['tour'];
            $checkin = $cart_item['checkin'];
            $tourdata = Tours::findOne($tour_id);
            $toalSeats_forTour = $tourdata->seats;

            $booked_seats = TourBookingDetails::find()->where(['tour_id' => $tour_id])
                ->andWhere(['check_in' => $checkin])
                ->sum('adults + childern');

            if ($cart_item['resource_assigned']) {
                $assigned_seats = TourBookingMappings::find()->where(['resource_id' => $cart_item['resource_assigned']])
                    ->andWhere(['checkin_date' => $checkin])
                    ->sum('persons');

                if ($assigned_seats >= $toalSeats_forTour) {
                    $this->addError('seats_not_available', $tourdata->title . ' is already booked. No seat available.');
                    Yii::$app->session->setFlash('warning', $tourdata->title . ' is already booked. No seat available');
                    return false;
                }
            }

            if ($booked_seats >= $toalSeats_forTour) {
                $this->addError('seats_not_available', $tourdata->title . ' is already booked. No seat available.');
                Yii::$app->session->setFlash('warning', $tourdata->title . ' is already booked. No seat available');
                return false;
            }
        }

        // Calculate total sum from cart .
        $cartTotalSummary = $this->cart->totalCartAmount();
        $this->total = $cartTotalSummary['price'];
        $this->total_tax = $cartTotalSummary['totalTax'];
        $this->total_sum = $cartTotalSummary['price'] + $cartTotalSummary['totalTax'] + $cartTotalSummary['pickup_fee'];

        if ($this->payment_type === 'RP' && $this->scenario === self::SCENARIO_CUSTOMER) {

            // get customer information
            $account = $this->getCustomer();
            $setting = \Yii::$app->appSettings->getSettings();

            if ($this->total_sum >= $setting->min_redeem_amount) {
                $pointsEarned = $account->getLoyalties()->where(['action' => '+'])->sum('points');
                $pointsRedeemed = $account->getLoyalties()->where(['action' => '-'])->sum('points');
                $netPoints = $pointsEarned - $pointsRedeemed;

                $requiredPoints = $setting->points_of_spent * $this->total_sum;

                if ($requiredPoints > $netPoints) {
                    $this->addError('loyalty', 'Sorry!, you do not have sufficient point to checkout');
                    return false;
                }
            } else {
                $this->addError('loyalty', 'Sorry!, you need to place order of minimum ' . $setting->min_redeem_amount . ' to redeem points');
                return false;
            }
        }

        // Validate Promo code if exist.
        if (Yii::$app->session->has('promoCode')) {
            $this->discount = Yii::$app->session->get('promoDiscount');
        }

        return parent::beforeValidate();
    }

    /*
     * @return Total sum of price.
     */
    public function getSellerBookingLimit($user_id, $payment_cycle)
    {
        return $a = TourBookings::find()->where(['account_id' => $user_id])
            ->andWhere(['>=', 'created_at', date(strtotime('-' . $payment_cycle . ' day', time()))])
            ->sum('total_sum');
    }

    /**
     * @throws \Exception
     */
    public function saveBooking()
    {
        $cartTotalSummary = $this->cart->totalCartAmount();
        $adminCartItems = $this->cart->getItems();

        // Begin database Transaction
        $transaction = \Yii::$app->db->beginTransaction();
        /*   echo '<pre>';
           print_r($adminCartItems);
           die();*/
        try {
            // Booking Tours Details
            $tourBooking = new TourBookings();

            // get customer information
            $customer = $this->getCustomer();

            // Check booking by based on customer type
            if ($customer->account_type === 'customer') {
                $bookingBy = 'customer';
            } elseif ($customer->account_type === 'seller') {
                $bookingBy = 'agent';
            }

            $tourBooking->booking_by = $bookingBy;
            $tourBooking->account_id = $customer->id;
            $tourBooking->price = $cartTotalSummary['price'];
            $tourBooking->discount = $this->discount;
            $tourBooking->total_sum = ($cartTotalSummary['price'] - $this->discount) + $cartTotalSummary['addonsPrice'] + $cartTotalSummary['totalTax'] + $cartTotalSummary['pickup_fee'];
            $tourBooking->payment_type = $this->payment_type;

            if ($this->cancellation_time <> "") {
                $tourBooking->cancellation_time = $this->cancellation_time;
            }
            if ($this->booking_status <> "") {
                $tourBooking->status = $this->booking_status;
            }

            if ($tourBooking->payment_type == 'RP') {
                $tourBooking->discount = 'recived';
                $tourBooking->status = 'confirmed';
            }

            $tourBooking->special_instruction = $this->special_instructions;
            $tourBooking->booking_address = $this->address_id;
            $tourBooking->amount_to_collect = $this->amount_to_collect;

            if (!$tourBooking->save()) {
                $this->addErrors($tourBooking->getErrors());
                return false;
            }

            if (count($adminCartItems) > 0) {
                foreach ($adminCartItems as $item) {

                    $taxAmount = $this->cart->itemTaxInformation($item['tour'], $item['total_price']);

                    // Save Booking details data
                    $bookingDetails = new TourBookingDetails([
                        'booking_id' => $tourBooking->id,
                        'package_id' => $item['package_id'],
                        'tour_id' => $item['tour'],
                        'price' => $item['total_price'],
                        'check_in' => date('Y-m-d', strtotime($item['checkin'])),
                        'addons_price' => $item['addonsTotal'],
                        'adult_price' => $item['price'],
                        'child_price' => $item['child_price'],
                        'taxes' => $taxAmount,
                        'adults' => $item['adults'],
                        'childern' => ($item['children'] == null || $item['children'] == '') ? 0 : $item['children'],
                        'addons' => $item['addons']
                    ]);

                    $bookingDetails->resource = $item['resource_assigned'];

                    if (isset($item['isExclusive']) && $item['isExclusive']) {
                        $bookingDetails->isExclusive = 1;
                        $bookingDetails->exclusive_title = $item['exclusive_tour_name'];
                        $bookingDetails->tour_time = $item['tour_time'];
                        $bookingDetails->destination = $item['destination'];
                    }

                    if (!$bookingDetails->save()) {
                        $this->addErrors($bookingDetails->getErrors());
                        return false;
                    }

                    if (isset($item['pickup']) && count($item['pickup']) > 0 && $item['pickup']['city'] <> "") {
                        $pickup = $item['pickup'];
                        $pickup_data = new \common\models\TourBookingPickup([
                            'tour_booking_id' => $tourBooking->id,
                            'pickup_booking_details' => $bookingDetails->id,
                            'pickup_city_id' => $pickup['city'],
                            'pickup_area' => $pickup['area'],
                            'pickup_hotel_id' => $pickup['hotel'],
                            'pickup_room_number' => $pickup['room'],
                            'pickup_phone_number' => $pickup['phone_number'],
                            'pickup_time' => $pickup['pickup_time'],
                            'pickup_price' => $pickup['pickup_rate'],
                            'pickup_date' => $bookingDetails->check_in,
                        ]);

                        if (!$pickup_data->save()) {
                            $this->addErrors($pickup_data->getErrors());
                            return false;
                        }
                    }
                }
            }

            if ($this->amount_paid) {
                $payments = new Payments();
                $payment_type = 'cash';

                if ($this->payment_type == 'CCO' || $this->payment_type == 'RP') {
                    $payment_type = 'online';
                }

                if ($this->payment_type == 'COD' || $this->payment_type == 'CP') {
                    $payment_type = 'cash';
                }

                $payments->user_id = $tourBooking->account_id;
                $payments->amount = $this->amount_paid;
                $payments->payment_date = \Yii::$app->dateTime->getTime();
                $payments->payment_method = $payment_type;
                $payments->received_by = \Yii::$app->user->id;
                $payments->payment_note = 'Admin booking payment';

                if (!$payments->save()) {
                    $this->addErrors($payments->getErrors());
                    return false;
                }
            }

            $transaction->commit();
            return $tourBooking;
        } catch (\Exception $exception) {
            $transaction->rollback();
            throw $exception;
        }
    }

    /**
     * Send order confirmation to user
     *
     */
    public function sendEmail($email, $booking, $cartTotalSummary)
    {
        $cc = [];
        $account = Accounts::findOne(['email' => $email]);

        if (!empty($account->cc_emails) && strpos($account->cc_emails, ',') != False) {
            $cc = explode(',', $account->cc_emails);
        }

        (new EmailHelper())->sendBookingEmail($email, $cc, 'Booking Confirmation', 'booking/order-confirmation', ['bookingDetails' => $booking, 'cartTotal' => $cartTotalSummary]);
    }

    /**
     * @return null|\yii\web\IdentityInterface|static
     */
    public function getCustomer()
    {

        if (\Yii::$app->session->get('customerSessionId')) {
            $customer = Accounts::findOne(\Yii::$app->session->get('customerSessionId'));
        } else {
            $customer = Yii::$app->user->getIdentity();
        }

        return $customer;
    }
}
