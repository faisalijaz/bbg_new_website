<?php

namespace app\models;

use common\models\Accounts;
use common\models\Members;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AccountSearch represents the model behind the search form about `common\models\Accounts`.
 */
class AccountSearch extends Members
{

    public $findExpiredLimit;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'occasional_updates', 'created_at', 'updated_at'], 'integer'],
            [['email', 'account_type', 'first_name', 'last_name', 'gender', 'country', 'status', 'findExpiredLimit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accounts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $role = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());

        if (!isset($role['admin'])) {
            // $this->parent_id = \Yii::$app->user->getId();
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'occasional_updates' => $this->occasional_updates,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'account_type' => $this->account_type,
            'group_id' => $this->group_id,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'parent_id', $this->parent_id]);

        if ($this->findExpiredLimit) {
            $query->join('LEFT JOIN', 'invoices inv', 'accounts.id = inv.user_id');
            $query->join('LEFT JOIN', 'payments pay', 'accounts.id = pay.user_id');
            $query->andWhere(['=', 'inv.status', 'pending']);
            $query->andWhere(['>', 'accounts.', 'SUM(inv.amount) - SUM(pay.amount) as difference']);
        }

        /*
            echo $query->createCommand()->getRawSql();
            die();
        */

        return $dataProvider;
    }
}
