(function() {
  'use strict';

  angular
    .module('tourApp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, categoryService) {
    var vm = this;

    init();

    function init() {

      categoryService.getCategories().then(function(data) {
        vm.categories = data;
      });

    }

    /**
     * @param ratings
     * @return string
     */
    vm.getStarts = function(rating)
    {

      var html = '';

      if(rating > 0 ) {

        for (var i = 1; i <= rating; i++) {
          html +='<span class="fa fa-star color-yellow" ></span >';
        }

        for (var j = i + 1; i <= 5; i++) {
          html +='<span class="fa fa-star color-grey" ></span >';
        }

      }


      return html;

    };

    vm.tourDetail = function(tour) {
      console.log(tour);
    }

  }
})();
