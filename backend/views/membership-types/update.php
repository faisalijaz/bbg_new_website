<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MembershipTypes */

$this->title = 'Update Membership Types: ' . $model->mt_id;
$this->params['breadcrumbs'][] = ['label' => 'Membership Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mt_id, 'url' => ['view', 'id' => $model->mt_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="membership-types-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
