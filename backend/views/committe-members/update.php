<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\CommitteMembers */

$this->title = 'Update Committe Members: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Committe Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="committe-members-update card-box">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
