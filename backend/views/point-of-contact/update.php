<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PointOfContact */

$this->title = 'Update Point Of Contact: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Point Of Contacts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="point-of-contact-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
