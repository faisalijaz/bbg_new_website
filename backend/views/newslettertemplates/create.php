<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Newslettertemplates */

$this->title = 'Create Newslettertemplates';
$this->params['breadcrumbs'][] = ['label' => 'Newslettertemplates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="newslettertemplates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
