<div id="news_section_<?= $news->id; ?>">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-bottom: dashed 1px #CCCCCC;">
        <tr>
            <td height="20" colspan="2"></td>
        </tr>
        <tr>
            <td width="35%" valign="top" align="left">
                <a href="<?= ($news <> null) ? Yii::$app->basePath . '/news/news-details?id=' . $news->id : ''; ?>">
                    <img width="140" height="92" class="newsImage"
                         src="<?= ($news <> null) ? $news->image : ''; ?>">
                </a>

            </td>
            <td width="65%" valign="top" align="left" rowspan="2">
                <a href="#" style="
                                float: right;
                                color: #ffffff;
                                padding: 1px 5px;
                                background: red;
                                border-radius: 10px;
                            " id="<?= $news->id; ?>" class="close_news_div"><i class="fa fa-trash-o bg-danger"></i></a>
                <h3>
                    <a href="<?= ($news <> null) ? Yii::$app->basePath . '/news/news-details?id=' . $news->id : ''; ?>"><?= ($news <> null) ? $news->title : ''; ?></a>
                </h3>
                <p class="newsDesc"><?php echo substr(strip_tags(($news <> null) ? $news->description : ''), 0, 100); ?></p>
            </td>
        </tr>
        <tr>
            <td height="40" align="left" valign="top"></td>
        </tr>
        <tr>
            <td height="10" colspan="2"></td>
        </tr>
    </table>
</div>