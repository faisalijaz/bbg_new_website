<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */

$this->title = 'Create Member Offers';
$this->params['breadcrumbs'][] = ['label' => 'Member Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-offers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
