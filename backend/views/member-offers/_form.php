<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="member-offers-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div id="images" class="form-group">
                <label class="control-label" for="input-image">Member Offer Image</label>
                <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                    <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>"
                         alt="" width="125" height="125" title=""
                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                </a>
                <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
            </div>
        </div>
    </div>

    <?= $form->field($model, 'title')->textInput() ?>

    <?= $form->field($model, 'category_id')->dropDownList(ArrayHelper::map(\common\models\Categories::find()->where(['type'=>'offers'])->all(), 'id', 'title'), ['prompt' => 'Select Category']); ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
