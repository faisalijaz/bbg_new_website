<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MemberOffers */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Member Offers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            ['attribute' => 'category_id',
                'format'=>'raw',
                'value' => function($model){
                return $model->memberCategories->title;
            }],
            'short_description',
            'description',
            'created_at',
            [
                'attribute' => 'Image',
                'format' => ['image', []],
                'value' => $model->image,


            ],
        ],
    ]) ?>

</div>
