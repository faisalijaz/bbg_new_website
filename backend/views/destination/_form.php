<?php

use common\models\Categories;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Destinations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destinations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tag')->dropDownList($tags, ['prompt' => 'Select Tag']); ?>

    <?= $form->field($model, 'category')->dropDownList(ArrayHelper::map(Categories::find()->all(), 'id', 'title'), ['prompt' => 'Category']); ?>

    <?= $form->field($model, 'capacity')->textInput() ?>

    <div id="images" class="form-group">
        <label class="control-label" for="input-image">Banner Image</label>
        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
            <img src="<?= $model->image ?>" alt="" width="125" height="125" title=""
                 data-placeholder="no_image.png"/>
        </a>
        <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>

    </div>

    <?= $form->field($model, 'status')->dropDownList(['In - Active', 'Active'], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn - success' : 'btn btn - primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
