<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Features */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Features',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Features'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="features-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
