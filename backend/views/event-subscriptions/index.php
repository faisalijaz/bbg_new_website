<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\EventSubscriptionsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Event Subscriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-subscriptions-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Event Subscriptions', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'user_type',
            'registered_by',
            'event_id',
            // 'firstname',
            // 'lastname',
            // 'email:email',
            // 'userNote:ntext',
            // 'mobile',
            // 'company',
            // 'walkin',
            // 'attended',
            // 'fee_paid',
            // 'payment_status',
            // 'status',
            // 'created_at',
            // 'updated_at',
            // 'gen_invoice',
            // 'focGuest',
            // 'paymentMethod',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
