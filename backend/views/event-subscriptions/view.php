<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EventSubscriptions */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Event Subscriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="event-subscriptions-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'user_type',
            'registered_by',
            'event_id',
            'firstname',
            'lastname',
            'email:email',
            'userNote:ntext',
            'mobile',
            'company',
            'walkin',
            'attended',
            'fee_paid',
            'payment_status',
            'status',
            'created_at',
            'updated_at',
            'gen_invoice',
            'focGuest',
            'paymentMethod',
        ],
    ]) ?>

</div>
