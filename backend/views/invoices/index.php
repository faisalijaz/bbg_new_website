<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\InvoicesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Invoices';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invoices-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
       <!-- --><?/*= Html::a('Create Invoices', ['create'], ['class' => 'btn btn-success']) */?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'label' => 'Invoice #',
                'attribute' => 'invoice_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                        ['/invoices/invoice-pdf', 'invoice' => $model->invoice_id],
                        [
                            'target' => '_blank',
                            'data-pjax' => "0"
                        ]);
                }
            ],
            [
                'attribute' => 'invoice_related_to',
                'label' => 'Related to',
                'format' => 'raw',
                'value' => function ($model) {
                    return ucwords($model->invoice_related_to);
                },
                'filter' => ['member' => 'Member', 'event' => 'Events'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Description',
                'attribute' => 'invoice_category',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->invoice_category;
                }
            ],
            [
                'label' => 'Payment Status',
                'attribute' => 'payment_status',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->payment_status == 'paid') ? '<span class="label label-success">Paid</span>' : '<span class="label label-danger">Unpaid</span>';
                },
                'filter' => ['paid' => 'Paid', 'unpaid' => 'Pending', 'partialy_paid' => 'Partially Paid'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Amount',
                'attribute' => 'total',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->total;
                }
            ],
            [
                'attribute' => 'invoice_date',
                'label' => 'Date',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->invoice_date;
                }
            ],
            [
                'attribute' => 'invoice_sent',
                'label' => 'Invoice sent',
                'format' => 'raw',
                'value' => function ($model) {
                    return ($model->invoice_sent) ? '<span class="label label-success">Sent</span>' : '<span class="label label-danger">Not Sent</span>';
                },
                'filter' => ['1' => 'Sent', '0' => 'Not Sent'],
                'filterInputOptions' => ['prompt' => 'Select ...', 'class' => 'form-control', 'id' => null],
            ],
            [
                'label' => 'Payment Reference',
                'attribute' => 'payment_id',
                'format' => 'raw',
                'value' => function ($model) {
                    return (!$model->payment_id) ? "-" : $model->payment_id;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{record_payment}',
                'buttons' => [
                    'record_payment' => function ($url, $model) {
                        return Html::a('<span class="label label-info"><span class="fa fa-plus"></span> Record Payment</span>', '#', [
                            'class' => 'showModalButton',
                            'value' => Url::to(['/invoices/make-payemnt/', 'invoice' => $model->invoice_id]),
                            'title' => Yii::t('yii', 'Make Payment'),
                            'id' => 'sendInvoice-' . $model->invoice_id
                        ]);
                    }
                ]
            ]
        ],
        'emptyText' => ' - ',
        'showFooter' => true,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'toolbar' => [
            '{export}',
        ],
        'export' => [
            'fontAwesome' => true,
        ],
        'exportConfig' => [
            GridView::EXCEL => true,
            GridView::PDF => true,
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => false,
        ],
    ]); ?>

</div>
