<?php

use common\models\Members;
use kartik\form\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Invoices */

$this->title = 'Create Invoices';
$this->params['breadcrumbs'][] = ['label' => 'Invoices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row card-box">
    <div class="invoices-create">

        <?php $form = ActiveForm::begin(); ?>

        <div class="col-md-4">
            <?= $form->field($model, 'invoice_id')->textInput([
                'readonly' => 'readonly',
                'required' => 'required'
            ])->label('Invoice Number') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'user_id')->dropDownList(ArrayHelper::map(Members::find()->all(), 'id', function ($model) {
                return $model->first_name . " " . $model->last_name;
            }), [
                'readonly' => 'readonly',
                'required' => 'required'
            ]); ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'amount_payable')->textInput(['readonly' => 'readonly'])->label('Amount Payable') ?>
        </div>

        <div class="col-md-12">
            <?= $form->field($model, 'detail')->textarea([
                'maxlength' => true,
                'required' => 'required'
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'amount')->textInput()->label('Amount Paid') ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'payment_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'Registration start date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'status')->dropDownList(
                [
                    'done' => 'Paid',
                    'cancelled' => 'Cancelled',
                    'pending' => 'Pending',
                    'authorized' => 'Authorized',
                    'captured' => 'Captured',
                    'refunded' => 'Refunded',
                    'partially_refunded' => 'Partially refunded',
                    'failed' => 'Failed'
                ],
                [
                    'prompt' => 'Select ...',
                    'required' => 'required'
                ]); ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'payment_method')->dropDownList([
                'online' => 'Online',
                'cash' => 'Cash',
                'cheque' => 'Cheque'
            ], ['prompt' => 'Select ...'], [
                'required' => 'required'
            ]);
            ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'transaction_id')->textInput(['maxlength' => true])->label("Transaction Reference") ?>
        </div>

        <div class="col-md-12">
            <div class="form-group">
                <button type="button" class="btn btn-default  pull-left" data-dismiss="modal">Close</button>
                <?= Html::submitButton($model->isNewRecord ? 'Pay' : 'Update', [
                    'class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-left'
                ]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<?= $this->registerJs("
    
");