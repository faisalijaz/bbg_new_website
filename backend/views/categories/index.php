<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\CategoriesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index card-box">
    <p class="pull-right">
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'title',
            'type',
            'short_description',
            'description',
            /*
            [
                'attribute' => 'Image',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<img src="'. $model->image .'" width="100">';
                }
            ],
            */
            // 'baner_image',
            // 'parent_id',
            // 'meta_title',
            // 'meta_description',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
