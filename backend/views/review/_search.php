<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use backend\models\Tours;
use common\models\Accounts;

/* @var $this yii\web\View */
/* @var $model backend\models\ReviewSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="reviews-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-4 col-sm-12">

            <?= $form->field($model, 'account_id')->dropDownList(ArrayHelper::map(Accounts::find()->all(), 'id', function($model) {
                return $model->first_name . ' '. $model->last_name;
            }), ['prompt' => 'Select Account']) ?>

        </div>
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'tour_id')->dropDownList(ArrayHelper::map(Tours::find()->all(), 'id', 'title'), ['prompt' => 'Select Tour']) ?>

        </div>
        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'rating') ?>
        </div>
    </div>

    <div class="row">

        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'description') ?>
        </div>


        <div class="col-lg-4 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(['In-Active' => 'In-Active', 'Active' => 'Active'], ['prompt' => 'Select status']) ?>
        </div>

        <div class="col-lg-4 col-sm-12">
            <label>&nbsp;</label>
            <div class="form-group pull-right">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
