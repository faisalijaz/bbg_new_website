<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Reviews */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Reviews',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="reviews-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
