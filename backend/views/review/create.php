<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Reviews */

$this->title = Yii::t('app', 'Create Reviews');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reviews-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
