<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\CmsPages;
use kartik\select2\Select2;
use common\models\MenuWidget;

$extra_pages_list =
    array(
        '/sponsors' => 'Sponsors',
        '/site/committee-info' => 'Committee-info',
        'events' => 'Upcoming Events',
        '/events?type=past' => 'Past Events',
        '/membership/directory' => 'Members Directory',
        '/company/directory' => 'Company Directory',
        '/apply/membership' => 'Join Us',
        '/news?type=member' => 'Member News',
        '/newsletter/subscribe' => 'Subscribe',
    );

$cms_pages = ArrayHelper::map(CmsPages::find()->all(), 'seo_url', 'seo_url');
$saved_cms_pages = ArrayHelper::map(MenuWidget::find()->where(['parent_id' => $model->id])->all(), 'id', 'title');
foreach ($extra_pages_list as $key => $saved_list) {
    foreach ($saved_cms_pages as $ky => $saved_cms) {
       if($saved_list == $saved_cms){
           $saved_cms_pages[$ky]= $key;
       }
    }
}

/* @var $this yii\web\View */
/* @var $model app\models\MenuWidget */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="menu-widget-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>


    <?php
    echo '<label class="control-label">Submenu - Extra Pages</label>';
    echo Select2::widget([
        'name' => 'MenuWidget[extra_pages]',
        'value' => $saved_cms_pages,
        'data' => $extra_pages_list,
        'maintainOrder' => true,
        'options' => ['placeholder' => 'Select a submenu ...', 'multiple' => true],
        'pluginOptions' => [
            'maximumInputLength' => 10
        ],
    ]);
    echo '<br/>';
    ?>


    <?php
    echo '<label class="control-label">Submenu - CMS Pages</label>';
    echo Select2::widget([
        'name' => 'MenuWidget[cms_pages]',
        'value' => $saved_cms_pages,
        'data' => $cms_pages,
        'maintainOrder' => true,
        'options' => ['placeholder' => 'Select a submenu ...', 'multiple' => true],
        'pluginOptions' => [
            'maximumInputLength' => 10
        ],
    ]);
    echo '<br/>';
    ?>


    <?= $form->field($model, 'sort_order')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList([ 'active' => 'Active', 'inactive' => 'Inactive', ], ['prompt' => 'Select status']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
