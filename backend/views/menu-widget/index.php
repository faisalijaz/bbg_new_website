<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SearchMenuWidget */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Menu');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-widget-index card-box">

    <p>
        <?= Html::a('<i class="fa fa-plus" aria-hidden="true"></i> Add Menu', ['create'], ['class' => 'btn btn-success btn-sm pull-right']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'title',
            'url:url',
            'sort_order',
            [
                'attribute' => 'status',
                'filter' => true,
                'value' => function($data) {
                    return ($data->status == 'active' ? 'Atcive' : 'In-active');
                },
                'format' => 'raw'
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
