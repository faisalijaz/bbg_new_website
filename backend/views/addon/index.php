<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\AddonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Addons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<div class="addons-index card-box">

    <p class="pull-right">
        <?= Html::a(Yii::t('app', 'Create Addons'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        // 'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',
            'price',
            'short_description',
            //'description',
            // 'image',
            // 'status',
            // 'quantity',
            // 'sort_order',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
