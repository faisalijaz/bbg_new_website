<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Addons */

$this->title = Yii::t('app', 'Create Addons');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Addons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="addons-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
