<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddonSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="addons-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-lg-4 col-sm-12"> <?= $form->field($model, 'name') ?></div>
        <div class="col-lg-4 col-sm-12"><?= $form->field($model, 'price') ?></div>
        <div class="col-lg-4 col-sm-12"><?= $form->field($model, 'short_description') ?></div>
    </div>
    
    
   
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group pull-right">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
