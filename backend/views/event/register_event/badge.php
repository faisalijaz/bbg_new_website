<?= Yii::$app->view->render('download_badge', [ 'subscribers' => $subscribers]); ?>
<div class="modal-footer">
    <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">
        Close
    </button>
</div>