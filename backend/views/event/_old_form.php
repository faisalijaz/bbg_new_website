<?php

use common\models\EventTypes;
use dosamigos\ckeditor\CKEditor;
use kartik\checkbox\CheckboxX;
use kartik\time\TimePicker;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">
                <li class="active tab">
                    <a href="#details" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">General</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="#feeDetails" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Fee & Timings</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="#EventImages" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Images</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content m-t-25">
        <div class="tab-pane active m-t-25" id="details">
            <hr/>
            <div class="col-md-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'venue')->textInput(['maxlength' => true])->label('Venue') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type')->dropDownList(
                    ArrayHelper::map(EventTypes::find()->all(), 'id', 'name'),
                    ['prompt' => 'Select Type']
                ); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'group_size')->textInput(['maxlength' => true, 'type' => 'numbers']); ?>
            </div>
            <div class="col-md-1">
                <br/>
                <br/>
                <?= $form->field($model, 'tba')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'fb_gallery')->textInput(['maxlength' => true])->label('Facebook Gallery URL') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
            </div>


            <div class="col-md-12">
                <h4 class="custom_color">
                    <b><u>Timings</u></b>
                </h4>
            </div>

            <div class="col-md-4">
                <?= $form->field($model, 'registration_start')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration start date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'registration_end')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration end date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'cancellation_tillDate')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration start date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'event_startDate')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration end date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'event_endDate')->widget(DateTimePicker::classname(), [
                    'options' => ['placeholder' => 'Registration end date'],
                    'type' => DateTimePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'event_startTime')->widget(TimePicker::classname(), [
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => true,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'event_endTime')->widget(TimePicker::classname(), [
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => true,
                        'minuteStep' => 1,
                        'secondStep' => 5,
                    ]
                ]);
                ?>
            </div>


            <div class="col-md-12">

                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'basic',
                    'clientOptions' => [
                        'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'ckeditor']),
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'active')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-2">

                <?= $form->field($model, 'sticky')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'userNote')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'id' => 'dietryPref',
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'dietry_options')->dropDownList(
                    ['Standard' => 'Standard', 'Vegetarian' => 'Vegetarian', 'other' => 'Other Dietary'],
                    ['class' => 'hidden form-control', 'prompt' => 'Select Dietry Option...', 'id' => 'select_diet_pref'])
                    ->label(false); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'other_dietry_description')->textarea(
                    [
                        'style' => 'width: 100%;height: inherit;',
                        'class' => 'hidden extra_diet_instruction',

                    ])->label(false); ?>
            </div>
        </div>

        <div class="tab-pane" id="feeDetails">
            <hr/>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="custom_color">
                            <b><u>Timings</u></b>
                        </h4>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'registration_start')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration start date'],
                            'type' => DateTimePicker::TYPE_INPUT,

                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd',
                                'removeButton' => 'time',
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'registration_end')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'cancellation_tillDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration start date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);
                        ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_startDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_endDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_startTime')->widget(TimePicker::classname(), [
                            'name' => 't1',
                            'pluginOptions' => [
                                'showSeconds' => false,
                                'showMeridian' => true,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_endTime')->widget(TimePicker::classname(), [
                            'name' => 't1',
                            'pluginOptions' => [
                                'showSeconds' => false,
                                'showMeridian' => true,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <h4 class="custom_color">
                        <b><u>Members Fee</u></b>
                    </h4>
                </div>

                <?php
                /** This an array that contains all value of membership groups and type fee so in order to make things
                 * easy kept group as an index and in that type are keys like fee[1]['nominee'] = 120
                 *
                 *
                 * **/
                $fee = [];
                if (isset($member_groups_fee)) {
                    if ($member_groups_fee <> null) {
                        foreach ($member_groups_fee as $typeFee) {
                            $fee[$typeFee->member_group][$typeFee->member_type] = $typeFee->fee;
                        }
                    }
                }
                foreach ($groups as $group) {
                    ?>
                    <hr/>

                    <div class="col-md-12" style="margin-bottom: 30px;">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="custom_color"><?= $group->title; ?> Fee</label>
                            </div>
                            <div class="col-md-3">
                                <label>Nominee </label>
                                <?= Html::input('text', 'Events[MemberTypeFee][' . $group->id . '][nominee]', (isset($fee[$group->id]['nominee']) && count($fee) > 0) ? $fee[$group->id]['nominee'] : 0,
                                    ['class' => 'form-control', 'type' => 'number']); ?>
                            </div>

                            <div class="col-md-3">
                                <label>Alternate</label>
                                <?= Html::input('text', 'Events[MemberTypeFee][' . $group->id . '][alternate]', (isset($fee[$group->id]['alternate']) && count($fee) > 0) ? $fee[$group->id]['alternate'] : 0,
                                    ['class' => 'form-control', 'type' => 'number']); ?>
                            </div>

                            <div class="col-md-3">
                                <label>Additional Member</label>
                                <?= Html::input('text', 'Events[MemberTypeFee][' . $group->id . '][additional]', (isset($fee[$group->id]['additional']) && count($fee) > 0) ? $fee[$group->id]['additional'] : 0,
                                    ['class' => 'form-control', 'type' => 'number']); ?>
                            </div>

                            <div class="col-md-3">
                                <label>Named Associate</label>
                                <?= Html::input('text', 'Events[MemberTypeFee][' . $group->id . '][named_associate]', (isset($fee[$group->id]['named_associate']) && count($fee) > 0) ? $fee[$group->id]['named_associate'] : 0,
                                    ['class' => 'form-control', 'type' => 'number']); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>


            </div>
            <div class="col-md-6">
                <div class="col-md-12">
                    <h4 class="custom_color">
                        <b><u>Fee</u></b>
                    </h4>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'member_fee')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'nonmember_fee')->textInput(['type' => 'number']) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'committee_fee')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'honourary_fee')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'sponsor_fee')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'focus_chair_fee')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-12">
                    <h5 class="custom_color"><b><u>Special Events</u></b></h5>
                    <div id="special_events_form">
                        <?php
                        if ($model->specialEventsFee <> null) {
                            $i = 0;
                            foreach ($model->specialEventsFee as $sp_event) {
                                ?>
                                <div class="form-group special_events_list" style="margin: 10px;">
                                    <div class="col-md-5">
                                        <input type="text" class="form-control"
                                               placeholder="Enter Fee Type"
                                               name="Events[specialEvents][<?= $i; ?>][fee_type]"
                                               value="<?= $sp_event->fee_type; ?>"/>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="number" class="form-control"
                                               placeholder="Enter Fee Amount"
                                               name="Events[specialEvents][<?= $i; ?>][amount]"
                                               value="<?= $sp_event->amount; ?>"/>
                                    </div>
                                    <div class="col-md-2">
                                        <a href="#" class="remove_special_event btn btn-sm btn-danger pull-left"
                                           style="margin: 10px;">
                                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                            Remove </a>
                                    </div>
                                </div>
                                <?php
                                $i++;
                            }
                        }
                        ?>
                    </div>
                    <div class="col-md-12 m-t-25">
                        <a href="#" id="add_special_event" class="btn btn-sm btn-primary pull-right">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="EventImages">
        <div class="col-md-12">

        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

<?= $this->registerJs('
      
      $("#select_diet_pref").change(function(){ 
            if($(this).val() == "other"){
                $(".extra_diet_instruction").removeClass("hidden");
            }else{
                $(".extra_diet_instruction").val("");
                $(".extra_diet_instruction").addClass("hidden");
            }
      });
      
      $("#dietryPref").click(function(){ 
            if($(this).is(":checked")){
                $("#select_diet_pref").removeClass("hidden");
            }else{ 
                $("#select_diet_pref").addClass("hidden");
            }
      });
      
'); ?>

