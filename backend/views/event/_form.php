<?php

use common\models\EventTypes;
use dosamigos\ckeditor\CKEditor;
use kartik\checkbox\CheckboxX;
use kartik\time\TimePicker;
use kartik\widgets\DateTimePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;



/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>
<style>.tab-content {
        box-shadow: none !important;
        color: #777777;
    }</style>
<div class="events-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">
                <li class="active tab">
                    <a href="#details" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">General</span>
                    </a>
                </li>
                <li class="tab">
                    <a href="#EventImages" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Images</span>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tab-content m-t-25">
        <div class="tab-pane active m-t-25" id="details">
            <hr/>
            <div class="col-md-6">
                <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'venue')->textInput(['maxlength' => true])->label('Venue') ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'type')->dropDownList(
                    ArrayHelper::map(EventTypes::find()->all(), 'id', 'name'),
                    ['prompt' => 'Select Type']
                ); ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-2">
                <?= $form->field($model, 'group_size')->textInput(['maxlength' => true, 'type' => 'numbers']); ?>
            </div>
            <div class="col-md-1">
                <br/>
                <br/>
                <?= $form->field($model, 'tba')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-5">
                <?= $form->field($model, 'fb_gallery')->textInput(['maxlength' => true])->label('Facebook Gallery URL') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'short_description')->textarea(['rows' => 6]) ?>
            </div>

            <div class="col-md-6"><!---->
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="custom_color">
                            <b><u>Timings</u></b>
                        </h4>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'registration_start')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration start date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'registration_end')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'cancellation_tillDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration start date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_startDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_endDate')->widget(DateTimePicker::classname(), [
                            'options' => ['placeholder' => 'Registration end date'],
                            'type' => DateTimePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-MM-dd'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_startTime')->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'showSeconds' => false,
                                'showMeridian' => true,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'event_endTime')->widget(TimePicker::classname(), [
                            'pluginOptions' => [
                                'showSeconds' => false,
                                'showMeridian' => true,
                                'minuteStep' => 1,
                                'secondStep' => 5,
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="custom_color">
                            <b><u>Fee</u></b>
                        </h4>
                    </div>
                    <div class="col-md-12">
                        <br/><br/>
                        <?= $form->field($model, 'special_events')->widget(CheckboxX::classname(), [
                            'initInputType' => CheckboxX::INPUT_CHECKBOX,
                            'options' => [
                                'class' => 'specialEventsCheck',
                            ],
                            'pluginOptions' => [
                                'theme' => 'krajee-flatblue',
                                'enclosedLabel' => true,
                                'threeState' => false,
                            ]
                        ])->label(false); ?>

                        <div class="col-md-12 hidden" id="specialEventsFee">
                            <h5 class="custom_color"><b><u>Special Events</u></b></h5>
                            <div id="special_events_form">
                                <?php
                                if ($model->specialEventsFee <> null) {
                                    $i = 0;
                                    foreach ($model->specialEventsFee as $sp_event) {
                                        ?>
                                        <div class="form-group special_events_list" style="margin: 10px;">
                                            <div class="col-md-5">
                                                <input type="text" class="form-control"
                                                       placeholder="Enter Fee Type"
                                                       name="Events[specialEvents][<?= $i; ?>][fee_type]"
                                                       value="<?= $sp_event->fee_type; ?>"/>
                                            </div>
                                            <div class="col-md-5">
                                                <input type="number" class="form-control"
                                                       placeholder="Enter Fee Amount"
                                                       name="Events[specialEvents][<?= $i; ?>][amount]"
                                                       value="<?= $sp_event->amount; ?>"/>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="#" class="remove_special_event btn btn-sm btn-danger pull-left"
                                                   style="margin: 10px;">
                                                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                    Remove </a>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="col-md-12 m-t-25">
                                <a href="#" id="add_special_event" class="btn btn-sm btn-primary pull-right">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i> Add
                                </a>
                            </div>
                        </div>

                    </div>
                    <div class="normal_fee">
                        <div class="col-md-6">
                            <?= $form->field($model, 'member_fee')->textInput(['class' => 'validate_float_number form-control']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'nonmember_fee')->textInput(['class' => 'validate_float_number form-control']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'committee_fee')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'honourary_fee')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'sponsor_fee')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'focus_chair_fee')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-md-12">

                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'options' => ['rows' => 6],
                    'preset' => 'basic',
                    'clientOptions' => [
                        'filebrowserImageBrowseUrl' => yii\helpers\Url::to(['imagemanager/manager', 'view-mode' => 'iframe', 'select-type' => 'ckeditor']),
                    ]
                ]);
                ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'active')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>

            <div class="col-md-2">
                <?= $form->field($model, 'members_only')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 'members_only',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>

            <div class="col-md-2">

                <?= $form->field($model, 'sticky')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'diet_option')->widget(CheckboxX::classname(), [
                    'initInputType' => CheckboxX::INPUT_CHECKBOX,
                    'options' => [
                        'id' => 'dietryPref',
                        'class' => 'HasDietryPrefrence',
                    ],
                    'pluginOptions' => [
                        'theme' => 'krajee-flatblue',
                        'enclosedLabel' => true,
                        'threeState' => false,
                    ]
                ])->label(false); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'diet_option_description')->dropDownList(
                    ['Standard' => 'Standard', 'Vegetarian' => 'Vegetarian', 'other' => 'Other Dietary'],
                    ['class' => 'hidden form-control', 'prompt' => 'Select Dietry Option...', 'id' => 'select_diet_pref'])
                    ->label(false); ?>
            </div>
            <div class="col-md-12">
                <?= $form->field($model, 'other_diet_option_description')->textarea(
                    [
                        'style' => 'width: 100%; height: 150px;;',
                        'class' => 'hidden extra_diet_instruction',

                    ])->label(false); ?>
            </div>
        </div>


        <div class="tab-pane" id="EventImages">
            <div class="row">
                <div class="col-sm-6">
                    <div id="images" class="form-group">
                        <label class="control-label" for="input-image">Main Image</label>
                        <a href="" id="thumb-image" data-toggle="image" class="img-thumbnail">
                            <img src="<?= ($model->image != "" && $model->image <> null) ? $model->image : Yii::$app->params['no_image']; ?>" alt="" width="125" height="125" title=""
                                 data-placeholder="<?= Yii::$app->params['no_image'];?>"/>
                        </a>
                        <?= $form->field($model, 'image')->hiddenInput(['maxlength' => true, 'id' => 'input-image'])->label(false) ?>
                    </div>
                </div>

                <table id="images" class="table table-striped table-bordered table-hover images-table upload-preview">
                    <thead>
                    <tr>
                        <td class="text-left">Title</td>
                        <td class="text-left">Description</td>
                        <td class="text-left">Image</td>
                        <td class="text-right">Sort Order</td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $image_row = 0; ?>
                    <?php foreach ($model->eventImages as $image) {
                        ?>
                        <tr id="image-row<?php echo $image_row; ?>">

                            <td class="">
                                <div class="form-group">
                                    <input type="text" name="Events[images][<?php echo $image_row; ?>][title]"
                                           value="<?= $image->title ?>" placeholder="Title"
                                           class="form-control"/>
                                </div>
                            </td>

                            <td class="">
                                <div class="form-group">
                                    <input type="text"
                                           name="Events[images][<?php echo $image_row; ?>][description]"
                                           value="<?= $image->description ?>" placeholder="description"
                                           class="form-control"/>
                                </div>
                            </td>

                            <td class="">
                                <a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image"
                                   class="img-thumbnail">
                                    <img src="<?php echo $image->image; ?>" alt="" title=""
                                         data-placeholder="<?php echo $image->image; ?>"/>
                                </a>
                                <input type="hidden" name="Events[images][<?php echo $image_row; ?>][image]"
                                       value="<?php echo $image->image; ?>"
                                       id="input-image<?php echo $image_row; ?>"/>
                            </td>

                            <td class="">
                                <div class="form-group">
                                    <input type="text"
                                           name="Events[images][<?php echo $image_row; ?>][sort_order]"
                                           value="<?php echo $image->sort_order; ?>" placeholder="Sort order"
                                           class="form-control"/>
                                </div>
                            </td>

                            <td class="text-left">
                                <button type="button"
                                        onclick="$('#image-row<?php echo $image_row; ?>, .tooltip').remove();"
                                        data-toggle="tooltip" title="Remove" class="btn btn-danger"><i
                                            class="fa fa-minus-circle"></i></button>
                            </td>
                        </tr>
                        <?php $image_row++; ?>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4"></td>
                        <td class="text-left">
                            <button type="button" onclick="addImage();" data-toggle="tooltip" title="Add"
                                    class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
                        </td>
                    </tr>
                    </tfoot>
                </table>
        </div>
    </div>
</div>

    <div class="col-md-12">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

    <?= $this->registerJs('
      
      $("#select_diet_pref").change(function(){ 
            if($(this).val() == "other"){
                $(".extra_diet_instruction").removeClass("hidden");
            }else{
                $(".extra_diet_instruction").val("");
                $(".extra_diet_instruction").addClass("hidden");
            }
      });
      
      $("#dietryPref").click(function(){ 
            if($(this).is(":checked")){
                $("#select_diet_pref").removeClass("hidden");
            }else{ 
                $("#select_diet_pref").addClass("hidden");
                 $(".extra_diet_instruction").addClass("hidden");
            }
      });
       
       $(".specialEventsCheck").click(function(){ 
        
            if($(this).is(":checked")){
                $(".normal_fee").addClass("hidden");
                $("#specialEventsFee").removeClass("hidden");
            }else{ 
                $("#specialEventsFee").addClass("hidden");
                $(".normal_fee").removeClass("hidden");
            }
       });
       
       $(document).ready(function(){
           
           if($(".specialEventsCheck").is(":checked")){
                $(".normal_fee").addClass("hidden");
                $("#specialEventsFee").removeClass("hidden");
           } else { 
                $("#specialEventsFee").addClass("hidden");
                $(".normal_fee").removeClass("hidden");
           }
           
            if($("#dietryPref").is(":checked")){
                
                $("#select_diet_pref").removeClass("hidden");
                
                if($("#select_diet_pref:selected").val() == "other"){
                    $(".extra_diet_instruction").removeClass("hidden");
                }
                
            } else { 
                $("#select_diet_pref").addClass("hidden");
                $(".extra_diet_instruction").addClass("hidden");
            }
            
       });
      
      
'); ?>


    <script type="text/javascript">
        //Start of Images as tour_images.
        var image_row = <?= $image_row ?>;

        function addImage() {
            html = '<tr id="image-row' + image_row + '">';
            html += '<td>';
            html += '    <div class="form-group">';
            html += '      <input type="text" name="Events[images][' + image_row + '][title]" value="" placeholder="Title" class="form-control" />';
            html += '    </div>';
            html += '  </td>';

            html += '  <td class="">';
            html += '    <div class="form-group">';
            html += '      <input type="text" name="Events[images][' + image_row + '][description]" value="" placeholder="description" class="form-control" />';
            html += '    </div>';
            html += '  </td>';

            html += '  <td class=""><a href="" id="thumb-image' + image_row + '" data-toggle="image" class="img-thumbnail"><img src="<?= Yii::$app->params['no_image']; ?>" alt="" title="" data-placeholder="<?= Yii::getAlias('@web') . '/images/no_image.png' ?>" /></a><input type="hidden" name="Events[images][' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';

            html += '  <td style="width: 10%;">';
            html += '    <div class="form-group">';
            html += '   <input type="text" name="Events[images][' + image_row + '][sort_order]" value="" placeholder="Sort order" class="form-control" />';
            html += '    </div>';
            html += '  </td>';

            html += '  <td class=""><button type="button" onclick="$(\'#image-row' + image_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';

            html += '</tr>';

            $('#images tbody').append(html);

            image_row++;
        }

    </script>