<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\TaxGroups */

$this->title = 'Create Tax Groups';
$this->params['breadcrumbs'][] = ['label' => 'Tax Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tax-groups-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
        'tax_rates' => $tax_rates,
    ]) ?>

</div>
