<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Cruise */

$this->title = 'Create Cruise';
$this->params['breadcrumbs'][] = ['label' => 'Cruises', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cruise-create card-box">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
