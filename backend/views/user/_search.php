<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'username') ?>
        </div>


        <div class="col-sm-3">
            <?php echo $form->field($model, 'email') ?>
        </div>

        <div class="col-sm-3">
            <?php echo $form->field($model, 'status')->dropDownList(['In-Active', 'Active'], ['prompt' => 'Select Status']) ?>
        </div>


        <div class="col-sm-3">
            <?php echo $form->field($model, 'type')->dropDownList(['Super Admin' => 'Super Admin','Admin' => 'Admin', 'Driver' => 'Driver', 'Agent' => 'Agent', 'Supervisor' => 'Supervisor'], ['prompt' => 'Select Type']) ?>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">

                <p class="pull-right">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
                </p>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
