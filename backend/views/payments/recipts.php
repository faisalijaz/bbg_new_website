<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="">
    <?= $this->render('receipt_pdf', [
        'model' => $model,
    ]); ?>
</div>
<div class="row">
    <div class="modal-footer">
        <?= \yii\helpers\Html::a(
            'Print Pdf',
            ['/payments/payment-receipt-pdf/', 'id' => $model->id], [
                'target' => '_blank',
                'class' => 'btn btn-success'
            ]
        ); ?>
        <button type="button" class="btn btn-default" id="member_send_receipt">Send Receipt</button>
        <button type="button" class="btn btn-default" data-dismiss="modal" id="closeModalBox">Close</button>
    </div>
</div>

<script>
    $("#member_send_receipt").click(function (e) {

        e.preventDefault();
        $(this).attr("disabled", true);
        var id = "<?= $model->id;?>";

        $.ajax({
            url: "<?= Yii::$app->request->baseUrl . '/payments/send-receipt-email/';?>",
            type: "POST",
            data: {paymentId: id},
            success: function (data) {
                if (data) {
                    swal({
                        title: "Success!",
                        text: "Receipt Sent!",
                        timer: 10000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                }
                $(this).attr("disabled", false);
            },
            error: function (data) {
                console.log(data);
                $(this).attr("disabled", false);
            }
        });
    });
</script>