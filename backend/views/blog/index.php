<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BlogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">

    <div class="card card-box">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p class="pull-right">
            <?= Html::a('Create Blog', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                'seo_url:url',
                [
                    'attribute' => 'banner_id',
                    'value' => function($model) {
                        return ($model->banner_id ? $model->banner->name : '');
                    }
                ],
                //'short_description:ntext',
                //'description:ntext',
                // 'meta_title',
                // 'meta_description',
                // 'meta_keywords',
                // 'seo_keywords',
                'sort_order',
                // 'banner_image',
                'status',

                ['class' => 'common\helpers\CustomActionColumn'],
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>

</div>
