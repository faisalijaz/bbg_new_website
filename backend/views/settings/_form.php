<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Settings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="settings-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">

        <div class="col-lg-12">
            <ul class="nav nav-tabs tabs">

                <li class="active tab">
                    <a href="#general" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                        <span class="hidden-xs">General</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#settings11" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Data</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#loyalty" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Loyalty</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#configuration" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Email setup</span>
                    </a>
                </li>

                <li class="tab">
                    <a href="#social-data" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Social Data</span>
                    </a>
                </li>


                <li class="tab">
                    <a href="#image-sizes-data" data-toggle="tab" aria-expanded="false">
                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                        <span class="hidden-xs">Image Sizes</span>
                    </a>
                </li>


            </ul>

            <div class="tab-content">

                <div class="tab-pane active" id="general">

                    <div class="row">

                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'app_name')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-lg-3 col-sm-12">
                            <?= $form->field($model, 'app_url')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-lg-3 col-sm-12">
                            <?= $form->field($model, 'app_owner')->textInput(['maxlength' => true]) ?>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'admin_email')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'from_email')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'event_email')->textInput(['maxlength' => true]) ?>
                        </div>

                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'membership_email')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'app_vat')->textInput(['maxlength' => true])->label('VAT #') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'fax')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'address')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                    <?= $form->field($model, 'location')->textarea(['maxlength' => true,])->label('Map Location') ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'about')->textarea(['rows' => 6, 'id' => 'elm1']) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= $form->field($model, 'invoice_payment_info')->textarea(['rows' => 6, 'id' => 'elm1']) ?>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="settings11">

                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <label class="control-label" for="input-image">Main Logo</label>
                            <a href="" id="thumb-image1" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $model->app_logo; ?>" alt="" width="125" height="125" title=""
                                     data-placeholder="no_image.png"/>
                            </a>
                            <input type="hidden" name="Settings[app_logo]" value="<?php echo $model->app_logo; ?>"
                                   id="input-image1"/>
                        </div>

                        <div class="col-lg-6 col-sm-12">
                            <label class="control-label" for="input-image">Footer Logo</label>
                            <a href="" id="thumb-image2" data-toggle="image" class="img-thumbnail">
                                <img src="<?php echo $model->footer_logo; ?>" alt="" width="125" height="125" title=""
                                     data-placeholder="no_image.png"/>
                            </a>
                            <input type="hidden" name="Settings[footer_logo]" value="<?php echo $model->footer_logo; ?>"
                                   id="input-image2"/>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'currency')->dropDownList(['AED' => 'Dirham', 'USD' => 'United state Dollar'], ['prompt' => 'Select Currency']) ?>
                        </div>

                        <div class="col-lg-6 col-sm-12">
                            <?= $form->field($model, 'cancel_grace_period')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>


                    <?= $form->field($model, 'Geocode')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'copyright_text')->textarea(['maxlength' => true]) ?>

                    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'meta_tag')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'meta_tag_description')->textarea(['maxlength' => true]) ?>

                </div>

                <div class="tab-pane" id="loyalty">

                    <?= $form->field($model, 'points_on_spent')->textInput() ?>
                    <?= $form->field($model, 'points_of_spent')->textInput() ?>
                    <?= $form->field($model, 'min_redeem_amount')->textInput(['type' => 'number']); ?>

                </div>

                <div class="tab-pane" id="configuration">

                    <?= $form->field($model, 'smtp_email')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'smtp_username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'smtp_password')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'smtp_hash')->dropDownList(['ssl' => 'Ssl', 'tls' => 'Tls',], ['prompt' => 'Hash type']) ?>

                    <?= $form->field($model, 'smtp_port')->textInput() ?>

                </div>

                <div class="tab-pane" id="social-data">

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'youtube')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'youtube_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'twitter')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'twitter_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'facebook')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'facebook_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'linkedin')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'linkedin_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'pinterest')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'pinterest_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'instagram')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'instagram_icon')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>

                </div>

                <div class="tab-pane" id="image-sizes-data">
                    <!--      Start of grid size data             -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'grid_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'grid_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of grid size data             -->

                    <!--      Start of Listing size data        -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'listing_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'listing_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of Listing size data         -->


                    <!--      Start of Additional image sizes        -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'additional_image_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'additional_image_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of Additional image sizes        -->


                    <!--      Start of Additional image sizes        -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'popup_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'popup_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of Additional image sizes        -->

                    <!--      Start of Additional image sizes        -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'page_banner_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'page_banner_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of Additional image sizes        -->


                    <!--      Start of Additional image sizes        -->
                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'home_banner_size_width')->textInput(['maxlength' => true]) ?>
                    </div>

                    <div class="col-lg-6 col-sm-12">
                        <?= $form->field($model, 'home_banner_size_height')->textInput(['maxlength' => true]) ?>
                    </div>
                    <!--      End of Additional image sizes        -->


                </div>


            </div>

        </div>

    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
