<?php

use kartik\sortinput\SortableInput;


// Scenario # 3: Extend scenario # 2 to show how items automatically reorder based on initial value passed.
// Also any reordered values are reset, when the form is reset.
echo SortableInput::widget([
    'name'=> 'sort_list_2',
    'value'=>'3,4,2,1,5',
    'items' => [
        1 => ['content' => '<i class="glyphicon glyphicon-cog"></i> Item # 1'],
        2 => ['content' => '<i class="glyphicon glyphicon-cog"></i> Item # 2'],
        3 => ['content' => '<i class="glyphicon glyphicon-cog"></i> Item # 3'],
        4 => ['content' => '<i class="glyphicon glyphicon-cog"></i> Item # 4'],
        5 => ['content' => '<i class="glyphicon glyphicon-cog"></i> Item # 5', 'disabled'=>true]
    ],
    'hideInput' => false,
    'options' => ['class'=>'form-control', 'readonly'=>true]
]);

