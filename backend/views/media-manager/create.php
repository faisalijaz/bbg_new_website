<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\MediaManager */

$this->title = 'Create Media Manager';
$this->params['breadcrumbs'][] = ['label' => 'Media Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-manager-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
