<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Sponsors */

$this->title = Yii::t('app', 'Create Sponsor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sponsors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sponsors-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
