<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Promotions */

$this->title = 'Create Promotions';
$this->params['breadcrumbs'][] = ['label' => 'Promotions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promotions-create card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
