<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'News', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-view card-box">

    <p class="pull-right">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn  btn-sm  btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h2><?= Html::encode($this->title) ?></h2>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'short_description:ntext',
            'description:ntext',
            'news_type',
            [
                'label' => 'Link',
                'format' => 'raw',
                'value' => function ($data) {
                    return Html::a($data->link, [$data->link]);
                },
            ],
            [
                'attribute' => 'Image',
                'format' => ['image', []],
                'value' => $model->image,


            ],
            [
                'attribute' => 'Published',
                'value' => ($model->isPublished == 1 ? 'Yes' : 'No')
            ]
        ],
    ]) ?>

</div>
