<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\AssignTransport */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Assign Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assign-transport-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'booking_detail_id',
            'vehicle_id',
            'driver_id',
            'pickup_time',
            'pickup_date',
            'assigned_by',
            'status',
            'date_created',
        ],
    ]) ?>

</div>
