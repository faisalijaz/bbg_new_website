<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AssignTransport */

$this->title = 'Create Assign Transport';
$this->params['breadcrumbs'][] = ['label' => 'Assign Transports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assign-transport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
