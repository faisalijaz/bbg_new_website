<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AssignTransportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Assign Transports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assign-transport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Assign Transport', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'booking_detail_id',
            'vehicle_id',
            'driver_id',
            'pickup_time',
            // 'pickup_date',
            // 'assigned_by',
            // 'status',
            // 'date_created',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
