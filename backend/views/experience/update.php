<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Experiences */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Experiences',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Experiences'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="experiences-update card-box">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
