<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Widgets */
/* @var $form yii\widgets\ActiveForm */

$displays = [
    'col-lg-3 col-sm-12' => '4 in a row',
    'col-lg-4 col-sm-12' => '3 in a row',
    'col-lg-6 col-sm-12' => '2 in a row',
    'col-sm-12' => '1 in a row',
];

?>

<div class="widgets-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>

        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'code')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'type')->dropDownList(['frontend' => 'Front End', 'backend' => 'Backend'], ['prompt' => 'Select Type']) ?>

        </div>
        <div class="col-lg-6 col-sm-12">
            <?= $form->field($model, 'sorting_order')->input('number') ?>
        </div>
    </div>

    <?= $form->field($model, 'status')->dropDownList(['0' => 'In-Active', '1' => 'Active'], ['prompt' => 'Select Status']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success btn-custom btn-rounded waves-effect waves-light' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

