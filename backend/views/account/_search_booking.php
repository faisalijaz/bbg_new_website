<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TourBookingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tour-bookings-search">

    <?php $form = ActiveForm::begin([
        //'action' => [($action) ? $action : 'index'],
        'method' => 'get',
    ]); ?>

    <div class="row">

        <?php if (in_array('account_search', $filters)) { ?>
            <div class="col-lg-6 col-sm-12 pull-left">
                <?= $form->field($model, 'account_search') ?>
            </div>
        <?php } ?>

        <?php if (in_array('start_date', $filters)) { ?>
            <div class="col-lg-2 col-sm-12 pull-left">

                <?=
                $form->field($model, 'start_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Start Date'],
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'todayBtn' => true,
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true
                    ]
                ]);
                ?>
            </div>
        <?php } ?>

        <?php if (in_array('end_date', $filters)) { ?>
            <div class="col-lg-2 col-sm-12 pull-left">

                <?=
                $form->field($model, 'end_date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'End Date'],
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'todayHighlight' => true,
                        'todayBtn' => true,
                        'format' => 'yyyy-mm-dd',
                        'autoclose' => true,
                    ]
                ]);
                ?>
            </div>
        <?php } ?>

        <div class="col-lg-2 pull-right">
            <label>---</label>
            <div class="form-group pull-right">
                <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
            </div>
        </div>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?= $this->registerJs('
$(document).find("#Print").on("click", function() {
    //Print ele4 with custom options
    $("#booking-tabs-content").print({
            globalStyles : true, 
            prepend : "Customer Inovice",
            stylesheet : "http://fonts.googleapis.com/css?family=Inconsolata",
        });
    });
  ');
?>
