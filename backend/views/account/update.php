<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */

$this->title = 'Update Profile:  ' . $model->first_name . " ". $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="accounts-update">

    <div class="col-md-12 card-box">
        <h3><?= Html::encode($this->title) ?></h3>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
