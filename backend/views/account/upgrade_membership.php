<?php

use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */

$this->title = 'Upgrade Membership';
$this->params['breadcrumbs'][] = ['label' => 'Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-box" style="min-height: 500px;">
            <h3>Upgrade Membership</h3>
            <hr/>
            <?php
            ActiveForm::begin([
                'id' => 'newsletter'
            ]);
            ?>
            <div class="row form-group">
                <div class="col-md-4">
                    <label for="member_group">Select Additional Member</label>
                    <?= Select2::widget([
                        'name' => 'member',
                        'value' => '',
                        'data' => ArrayHelper::map(\common\models\Members::find()->orderBy(['group_id' => 2, 'first_name' => SORT_ASC])->all(), 'id', function ($model) {
                            return $model->first_name . " " . $model->last_name;
                        }),
                        'options' => ['multiple' => false, 'placeholder' => 'Select states ...']
                    ]);
                    ?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    <label for="member_group">Select Membership</label>
                    <?= Html::dropDownList('group_id', '', ['1' => 'Business'],
                        ['class' => 'form-control', 'prompt' => 'Select Membership Category', 'required' => 'required']); ?>
                </div>
            </div>
            <div class="row form-group">
                <div class="col-md-4">
                    <label for="member_group">Select Membership Type</label>
                    <?= Html::dropDownList('accountTypes', '',
                        \Yii::$app->params['accountTypes'],
                        ['class' => 'form-control', 'prompt' => 'Select Membership Type', 'required' => 'required']); ?>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-4">
                    <button type="submit" class="btn btn-primary pull-right">Change</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
