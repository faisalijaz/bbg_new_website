<?php

use common\models\Country;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model common\models\AccountSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="accounts-search">

    <?php $form = ActiveForm::begin([
        'method' => 'get',
    ]); ?>

    <div class="row">

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'from_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'From date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'to_date')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'To date'],
                'type' => DateTimePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-MM-dd'
                ]
            ]);
            ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'group_id')->dropDownList(
                \yii\helpers\ArrayHelper::map(\common\models\Groups::find()->all(), 'id', 'title'),
                ['prompt' => 'Select Type']
            )->label('Membership Type'); ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'account_type')->dropDownList(Yii::$app->params['accountTypes'], ['prompt' => 'Select Type']) ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <?= $form->field($model, 'status')->dropDownList(['Un-Approved', 'Approved'], ['prompt' => 'Select Status ...']) ?>
        </div>

        <div class="col-lg-2 col-sm-12">
            <div class="form-group inline-row" style="margin-top: 25px;">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>
        </div>
    </div>

    <div class="row">
    </div>

    <?php ActiveForm::end(); ?>
</div>
