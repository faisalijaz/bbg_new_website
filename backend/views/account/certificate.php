<style>
    #cetificate_header {
        width: 100%;
        text-align: center;
        margin-top: 0px;
    }

    #cetificate_footer {
        border-right: 0.5px solid #ddd;
        border-bottom: 0.5px solid #ddd;
    }

    @media print {
        #cetificate_header {
            margin-top: 0px;
        }
    }
</style>
<?= Yii::$app->view->render('certificate_pdf', ['model' => $model]); ?>