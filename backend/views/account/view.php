<?php

use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */

$this->title = $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row">
    <div class="card-box col-lg-12">

        <p class="pull-left">
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-sm btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this member?',
                    'method' => 'post',
                ],
            ]); ?>

            <?= Html::button('Reset Password', [
                'class' => 'btn btn-sm btn-warning resetPassword',
                'id' => $model->id
            ]); ?>

        </p>

        <p class="pull-right">

            <span id="memberStatusContainer">

                <?php
                if ($model->status) {

                    echo Html::button('<i class="fa fa-times" /></i> UnApprove Membership', [
                        'class' => 'btn btn-sm btn-info unapprove_member membershipStatus',
                        'target' => '_blank',
                        'title' => 'UnApprove Membership',
                        'id' => $model->id,
                    ]);

                } else {

                    echo Html::button('<i class="fa fa-check-circle"></i> Approve Membership', [
                        'class' => 'btn btn-sm btn-success approve_member membershipStatus',
                        'target' => '_blank',
                        'title' => 'Approve Membership',
                        'id' => $model->id,
                    ]);
                }
                ?>


            </span>
            <?= Html::a('<i class="fa fa-download"></i> Download Certificate', ['member-certificate', 'id' => $model->id, 'mode' => 'download'], [
                'class' => 'btn btn-sm btn-primary',
                'target' => '_blank',
                'title' => 'Membership Certificate'
            ]); ?>

            <?= Html::a('<i class="fa fa-eye"></i> View Certificate', ['member-certificate', 'id' => $model->id, 'mode' => 'view'], [
                'class' => 'btn btn-sm btn-primary',
                'target' => '_blank',
                'title' => 'Membership Certificate'
            ]); ?>

            <?= Html::button('Send Certificate',
                ['id' => $model->id, 'class' => 'sendCertificate btn btn-sm btn-primary']); ?>

        </p>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <ul class="nav nav-tabs tabs">
            <li class="active tab border-right-list ">
                <a href="#accountInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Profile</span>
                </a>
            </li>
            <li class="tab border-right-list ">
                <a href="#CompanyInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Company </span>
                </a>
            </li>
            <li class="tab border-right-list ">
                <a href="#DcoumentsUploaded" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                    <span class="hidden-xs">Documents</span>
                </a>
            </li>
            <li class="tab border-right-list " style="border-right: 1px solid #ddd;">
                <a href="#loyalitiesInfo" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                    <span class="hidden-xs">Contacts</span>
                </a>
            </li>
            <li class="tab border-right-list " style="border-right: 1px solid #ddd;">
                <a href="#PointOfContacts" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
                    <span class="hidden-xs">Point Of Contact</span>
                </a>
            </li>
            <li class="tab border-right-list ">
                <a href="#bookingInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Invoices</span>
                </a>
            </li>
            <li class="tab border-right-list ">
                <a href="#paymentInfo" data-toggle="tab" aria-expanded="false">
                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                    <span class="hidden-xs">Payments</span>
                </a>
            </li>
        </ul>
        <div class="tab-content" style="min-height: 1500px;">
            <hr/>
            <div class="tab-pane active" id="accountInfo">
                <div class="col-md-5">
                    <img src="<?= ($model->picture) ? $model->picture : Yii::$app->params['no_image'];?>"  width="200"/>
                    <hr/>
                    <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'first_name',
                                'last_name',
                                'dob',
                                [
                                    'attribute' => 'gender',
                                    'value' => $model->status == 0 ? 'FeMale' : 'Male'
                                ],
                                [
                                    'attribute' => 'country',
                                    'value' => (isset($model->accountCountry) ? $model->accountCountry->country_name : 'N/A'),
                                ],
                                'email:email',
                                [
                                    'attribute' => 'account_type',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        if ($model->account_type <> null) {
                                            return '<span class="label label-info">' . ($model->account_type == "named_associate") ? "Named Associate" : ucwords($model->account_type) . '</span>';
                                        }
                                        return '-';
                                    },
                                 ],
                                [
                                    'attribute' => 'status',
                                    'format' => 'raw',
                                    'value' => function ($model) {

                                        $status = $model->status;
                                        $class = "label-warning";

                                        if ($status == '1') {
                                            $class = "label-success";
                                        }

                                        if ($status == '2') {
                                            $class = "label-info";
                                        }

                                        if ($status == '3') {
                                            $class = "label-danger";
                                        }

                                        if ($status == '4') {
                                            $class = "label-primary";
                                        }

                                        return '<span id="_status_' . $model->id . '" class="label ' . $class . '">' . ucwords(Yii::$app->params['statusTitle'][$model->status]) . '</span>';
                                    },
                                ],
                                'vat_number',
                                [
                                    'attribute' => 'amazing_offers',
                                    'value' => $model->status == 0 ? 'No' : 'Yes'
                                ],
                                [
                                    'attribute' => 'occasional_updates',
                                    'value' => $model->status == 0 ? 'No' : 'Yes'
                                ],
                                'created_at:date',
                                'updated_at:date'
                            ],
                    ]); ?>

                    <p class="pull-left">
                        <?php

                        $class = "add_true btn-primary ";
                        $icon = '<i class="fa fa-plus"></i>';

                        if ($model->honourary) {
                            $class = "add_false btn-info ";
                            $icon = '<i class="fa fa-minus-circle"></i>';
                        }

                        echo Html::button('<span>' . $icon . '</span> Honorary list', [
                            'class' => $class . ' btn btn-sm special_membership honorary',
                            'target' => '_blank',
                            'title' => 'Honorary list',
                            'id' => $model->id
                        ]);

                        //#################################################################

                        $class = "add_true btn-primary ";
                        $icon = '<i class="fa fa-plus"></i>';

                        if ($model->focus_chair) {
                            $class = "add_false btn-info";
                            $icon = '<i class="fa fa-minus-circle"></i>';
                        }

                        echo Html::button('<span>' . $icon . '</span> Focus Chair', [
                            'class' => $class . ' btn btn-sm special_membership focus_chair',
                            'target' => '_blank',
                            'title' => 'Focus Chair',
                            'id' => $model->id
                        ]);


                        //#################################################################

                        $class = "add_true btn-primary ";
                        $icon = '<i class="fa fa-plus"></i>';

                        if (($model->sponsor)) {
                            $class = "add_false btn-info ";
                            $icon = '<i class="fa fa-minus-circle"></i>';
                        }

                        echo Html::button('<span>' . $icon . '</span> Sponsors', [
                            'class' => $class . 'btn btn-sm special_membership sponsor',
                            'target' => '_blank',
                            'title' => 'Sponsors',
                            'id' => $model->id
                        ]);


                        //#################################################################

                        $class = "add_true btn-primary ";
                        $icon = '<i class="fa fa-plus"></i>';

                        if ($model->committee) {
                            $class = "add_false btn-info ";
                            $icon = '<i class="fa fa-minus-circle"></i>';
                        }

                        echo Html::button('<span>' . $icon . '</span> Committee Members', [
                            'class' => $class . 'btn btn-sm special_membership committee_member',
                            'target' => '_blank',
                            'title' => 'Committee Members',
                            'id' => $model->id
                        ]); ?>


                    </p>
                </div>

                <div class="col-md-7">
                    <div class="col-md-12" id="certificate">
                        <?= Yii::$app->view->render('certificate', ['model' => $model]); ?>
                    </div>
                </div>

            </div>
            <div class="tab-pane" id="CompanyInfo">

                <h3>Company Information</h3>
                <hr/>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'Company',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->name;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Category',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                     if ($model->accountCompany->companyCategory <> null) {
                                         return $model->accountCompany->companyCategory->title;
                                     }
                                }
                            }
                        ],
                        [
                            'attribute' => 'Website',
                            'format' => 'url',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->url;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Phone Number',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->phonenumber;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Fax',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->fax;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Postal Code',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->postal_code;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Emirates',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->emirates_number;
                                }
                            }
                        ],
                        [
                            'attribute' => 'Address',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->address;
                                }
                            }
                        ],
                        [
                            'attribute' => 'About',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->accountCompany <> null) {
                                    return $model->accountCompany->about_company;
                                }
                            }
                        ],
                    ],

                ]); ?>
            </div>
            <div class="tab-pane" id="DcoumentsUploaded">
                <?php
                $docs = \common\models\MembershipDocuments::findOne(['account_id' => $model->id]);

                if ($docs <> null) {
                    ?>
                    <div class="accounts-form col-md-6">
                        <label class="control-label" for="input-image">Trade Licencse</label>
                        <p>
                            <a href="<?= ($docs->trade_licence != "" && $docs->trade_licence <> null) ? $docs->trade_licence : "#"; ?>"
                               target="_blank">
                                <img src="<?= ($docs->trade_licence != "" && $docs->trade_licence <> null) ? $docs->trade_licence : Yii::$app->params['no_image']; ?>"
                                     alt="" width="50%" title=""
                                     data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                            </a>
                        </p>


                    </div>
                    <div class="accounts-form col-md-6">
                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Passport Copy</label>
                            <p>
                                <a href="<?= ($docs->passport_copy != "" && $docs->passport_copy <> null) ? $docs->passport_copy : "#"; ?>"
                                   target="_blank">

                                    <img src="<?= ($docs->passport_copy != "" && $docs->passport_copy <> null) ? $docs->passport_copy : Yii::$app->params['no_image']; ?>"
                                         alt="" width="50%" title=""
                                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                                </a>
                            </p>
                        </div>
                    </div>
                    <hr/>
                    <div class="accounts-form col-md-6">
                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Residence Visa Copy</label>
                            <p>
                                <a href="<?= ($docs->residence_visa != "" && $docs->residence_visa <> null) ? $docs->residence_visa : "#"; ?>"
                                   target="_blank">
                                    <img src="<?= ($docs->residence_visa != "" && $docs->residence_visa <> null) ? $docs->residence_visa : Yii::$app->params['no_image']; ?>"
                                         alt="" width="50%" title=""
                                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                                </a>
                            </p>
                        </div>
                    </div>
                    <div class="accounts-form col-md-6">
                        <div id="images" class="form-group">
                            <label class="control-label" for="input-image">Picture</label>
                            <p>
                                <a href="<?= ($docs->passport_size_pic != "" && $docs->passport_size_pic <> null) ? $docs->passport_size_pic : "#"; ?>"
                                   target="_blank">
                                    <img src="<?= ($docs->passport_size_pic != "" && $docs->passport_size_pic <> null) ? $docs->passport_size_pic : Yii::$app->params['no_image']; ?>"
                                         alt="" width="50%" title=""
                                         data-placeholder="<?= Yii::$app->params['no_image']; ?>"/>
                                </a>
                            </p>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="tab-pane" id="loyalitiesInfo">
                <?= GridView::widget([
                    'dataProvider' => $contacts,
                    'tableOptions' => ['class' => 'table table-striped'],
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'first_name',
                        'last_name',
                        'email:email',
                        'phone_number',
                        'account_type',
                        [
                            'attribute' => 'status',
                            'value' => function($model) {
                                return $model->status == 0 ? 'In-Active' : 'Active';
                            }
                        ],
                        ['class' => 'yii\grid\ActionColumn'],

                    ],
                    'emptyText' => ' - ',
                    'showFooter' => true,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    'toolbar' => [
                        '{export}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => true,
                        GridView::PDF => true,
                    ],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => false,
                    ],
                ]); ?>

            </div>
            <div class="tab-pane" id="bookingInfo">
                <?= GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model->getInvoices(),
                        'pagination' => [
                            'pageSize' => 10,
                        ]
                    ]),
                    'columns' => [
                        [
                            'label' => 'Invoice #',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                                    ['/account/invoice-pdf', 'member' => $model->user_id, 'invoice' => $model->invoice_id],
                                    [
                                        'target' => '_blank',
                                        'data-pjax' => "0"
                                    ]);
                            }
                        ],
                        [
                            'attribute' => 'Related to',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return ucwords($model->invoice_related_to);
                            }
                        ],
                        [
                            'label' => 'User',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->member <> null) {
                                    return Html::a($model->member->first_name . " " . $model->member->last_name,
                                        ['/account/view', 'id' => $model->member->id],
                                        [
                                            'target' => '_blank',
                                            'data-pjax' => "0"
                                        ]);
                                }
                            }
                        ],
                        [
                            'label' => 'Company',
                            'format' => 'raw',
                            'value' => function ($model) {
                                if ($model->member <> null) {
                                    if ($model->member->accountCompany <> null) {
                                        return Html::a($model->member->accountCompany->name,
                                            ['/account/view', 'id' => $model->member->id],
                                            [
                                                'target' => '_blank',
                                                'data-pjax' => "0"
                                            ]);
                                    }
                                }
                            }
                        ],
                        [
                            'attribute' => 'Description',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->invoice_category;
                            }
                        ],
                        [
                            'attribute' => 'Payment Status',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return ($model->payment_status == 'paid') ? '<span class="label label-success">Paid</span>' : '<span class="label label-danger">Unpaid</span>';
                            }
                        ],
                        [
                            'attribute' => 'Amount',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->total;
                            }
                        ],
                        [
                            'attribute' => 'Date',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->invoice_date;
                            }
                        ],
                        [
                            'attribute' => 'Invoice sent',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return ($model->invoice_sent) ? '<span class="label label-success">Sent</span>' : '<span class="label label-danger">Not Sent</span>';
                            }
                        ],
                        [
                            'attribute' => 'Payment Reference',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return (!$model->payment_id) ? "-" : $model->payment_id;
                            }
                        ],

                    ],
                    'emptyText' => ' - ',
                    'showFooter' => true,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    'toolbar' => [
                        '{export}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => true,
                        GridView::PDF => true,
                    ],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => false,
                    ],
                ]); ?>
            </div>
            <div class="tab-pane" id="PointOfContacts">
                <?/*= GridView::widget([
                    'dataProvider' => new ActiveDataProvider([
                        'query' => $model->pointOfContact(),
                        'pagination' => [
                            'pageSize' => 10,
                        ]
                    ]),
                    'columns' => [['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'Name',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return ($model->name);
                            }
                        ],
                        [
                            'attribute' => 'Designation',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->designation;
                            }
                        ],
                        [
                            'attribute' => 'Email',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return ($model->email);
                            }
                        ],
                        [
                            'attribute' => 'Phone Number',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->number;
                            }
                        ],

                        [
                            'attribute' => 'Date',
                            'format' => 'raw',
                            'value' => function ($model) {
                                return $model->create_date;
                            }
                        ],
                    ],
                    'emptyText' => ' - ',
                    'showFooter' => true,
                    'pjax' => true,
                    'pjaxSettings' => [
                        'neverTimeout' => true,
                    ],
                    'toolbar' => [
                        '{export}',
                    ],
                    'export' => [
                        'fontAwesome' => true,
                    ],
                    'exportConfig' => [
                        GridView::EXCEL => true,
                        GridView::PDF => true,
                    ],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => false,
                    ],
                ]); */?>
            </div>
            <div class="tab-pane" id="paymentInfo">
                    <?= GridView::widget([
                        'dataProvider' => new ActiveDataProvider([
                            'query' => $model->getPayments(),
                            'pagination' => [
                                'pageSize' => 10,
                            ]
                        ]),
                        'columns' => [['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'Payment Id',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return str_pad($model->id, 7, "0", STR_PAD_LEFT);
                                }
                            ],
                            [
                                'label' => 'Invoice #',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return Html::a(str_pad($model->invoice_id, 7, "0", STR_PAD_LEFT),
                                        ['/account/invoice-pdf', 'member' => $model->user_id, 'invoice' => $model->invoice_id],
                                        [
                                            'target' => '_blank',
                                            'data-pjax' => "0"
                                        ]);
                                }
                            ],
                            /*[
                                'label' => 'Related to',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->paymentUser <> null) {
                                        return Html::a($model->paymentUser->first_name . " " . $model->paymentUser->last_name,
                                            ['/account/view', 'id' => $model->user_id],
                                            [
                                                'target' => '_blank',
                                                'data-pjax' => "0"
                                            ]);
                                    }
                                }
                            ],*/
                            /*[
                                'label' => 'Company',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    if ($model->paymentUser <> null) {
                                        if ($model->paymentUser->accountCompany <> null) {
                                            return Html::a($model->paymentUser->accountCompany->name,
                                                ['/account/view', 'id' => $model->user_id],
                                                [
                                                    'target' => '_blank',
                                                    'data-pjax' => "0"
                                                ]);
                                        }
                                    }
                                }
                            ],*/
                            [
                                'attribute' => 'Amount',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->amount;
                                }
                            ],
                            [
                                'attribute' => 'transaction_id',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->transaction_id;
                                }
                            ],
                            [
                                'attribute' => 'Payment Method',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return ucwords($model->payment_method);
                                }
                            ],
                            [
                                'attribute' => 'detail',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->detail;
                                }
                            ],
                            [
                                'attribute' => 'Payment Date',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return $model->payment_date;
                                }
                            ],
                            [
                                'attribute' => 'Received By ',
                                'format' => 'raw',
                                'value' => function ($model) {

                                    if ($model->payment_method != "online") {
                                        return $model->received_by;
                                    }

                                    return '-';

                                }
                            ],
                        ],
                        'emptyText' => ' - ',
                        'showFooter' => true,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ],
                        'toolbar' => [
                            '{export}',
                        ],
                        'export' => [
                            'fontAwesome' => true,
                        ],
                        'exportConfig' => [
                            GridView::EXCEL => true,
                            GridView::PDF => true,
                        ],
                        'panel' => [
                            'type' => GridView::TYPE_PRIMARY,
                            'heading' => false,
                        ],
                    ]); ?>
            </div>
    </div>
</div>
    <style>
        .special_membership {
            margin: 5px;
        }
    </style>
<?= $this->registerJs('
    
    $(".sendCertificate").click(function(e){
         
        $(this).attr("disabled",true);
        $(".sendCertificate").attr( "disabled", "disabled" );
        $("body #custom_bbg_loader").css("display", "block");
        $("html").css("opacity", ".5");
        $("html").css("background-color", "#000");
        
        var member = $(this).attr("id"); 
        $(this).attr("disabled",false);
        
         $.ajax({
            url: "' . Yii::$app->request->baseUrl . '/account/send-certificate/",
            type: "POST",
            data: { user: member} , 
            success : function(data){
                console.log(data);
                
                $(".custom_bbg_loader").attr( "disabled", false);
                $("body #custom_bbg_loader").css("display", "none");
                $("html").css("opacity", "1");
                
            },
            error : function(data){
            
                $(".custom_bbg_loader").attr( "disabled", false);
                $("body #custom_bbg_loader").css("display", "none");
                $("html").css("opacity", "1");
                console.log(data);
            }
        }); 
        
    });
    
    $("body").find(".membershipStatus").click(function(){
         
        $(this).attr("disabled",true);
        var member = $(this).attr("id");  
        
        var _status = 0;
        
        if($(this).hasClass("approve_member")) {
            status = 1;  
        }
        
         $.ajax({
            
            url: "' . Yii::$app->request->baseUrl . '/account/approve-member/",
            type: "POST",
            data: { user : member, status : _status }, 
            success : function(data){
                
                if(_status == 1) {
                    
                    $(this).attr("disabled", false);
                    
                    $(this).removeClass("approve_member");  
                    $(this).addClass("unapprove_member"); 
                    
                    $(this).text("UnApprove Membership");
                    $(this).attr("title", "UnApprove Membership");
                
                    
                } else { 
                     
                    $(this).prop("disabled", false);
                    
                    $(this).text("Approve Membership");
                    $(this).attr("title", "Approve Membership");
                    
                    $(this).removeClass("unapprove_member"); 
                    $(this).addClass("approve_member");
                     
                    
                } 
            },
            
            error : function(data){
                console.log(data);
            }
        }); 
    
    });
    
    
    $("body").find(".special_membership").click(function(){
         
        $(this).attr("disabled",true);
        var member = $(this).attr("id");  
        
        var status = "";
        var type = 1;
        
        if($(this).hasClass("add_false")){
            type = 0;
        }
        
        if($(this).hasClass("honorary")) {
            status = "honorary";  
        }
        
        if($(this).hasClass("focus_chair")) {
            status = "focus_chair";  
        }
        
        if($(this).hasClass("sponsor")) {
            status = "sponsor";  
        }
        
        if($(this).hasClass("committee_member")) {
            status = "committee_member";  
        }
        
        
         
         $.ajax({
            
            url: "' . Yii::$app->request->baseUrl . '/account/special-member-status/",
            type: "POST",
            data: { member_status : status, user : member, status_type : type },
             
            success : function(data){
                
                if(type == 1){
                    
                    $("."+status).removeClass("add_true");
                    $("."+status).removeClass("btn-primary");
                     
                    $("."+status).addClass("add_false");
                    $("."+status).addClass("btn-info");
                    
                    $("."+status).find(".fa").removeClass("fa-plus");
                    $("."+status).find(".fa").addClass( "fa-minus-circle" );
                    
                } else{
                
                    $("."+status).removeClass("add_false");
                    $("."+status).removeClass("btn-info");
                    
                    $("."+status).addClass("add_true");
                    $("."+status).addClass("btn-primary");
                    
                    $("."+status).find(".fa").removeClass("fa-minus-circle");
                    $("."+status).find(".fa").addClass( "fa-plus" );
                }
                
                $(this).attr("disabled",false);
            },
            error : function(data){
                
                console.log(data);
                
                $(this).attr("disabled",false);
            }
        }); 
        
        $(this).attr("disabled",false);
    
    });
    
    $("body").find(".resetPassword").click(function(){
         
        $(this).attr("disabled",true);
        var member = $(this).attr("id");  
       
         $.ajax({
            
            url: "' . Yii::$app->request->baseUrl . '/account/reset-password/",
            type: "POST",
            data: { user : member },
             
            success : function(data){
                
                if(data){ 
                   
                   swal({
                        title: "Password Changed",
                        text: "New password is: <b>" + data + "</b>",
                        timer: 60000,
                        type: "success",
                        html: true,
                        showConfirmButton: true
                    });
                   
                } else{
                     
                     swal({
                            title: "Oooppsss!",
                            text: "Passowrd reset failed. Try again!",
                            timer: 10000,
                            type: "error",
                            html: true,
                            showConfirmButton: true
                     });
                }
                
                $(this).attr("disabled",false);
            },
            error : function(data){
                
                console.log(data);
                
                $(this).attr("disabled",false);
            }
        }); 
        
        $(this).attr("disabled",false);
    
    });
    
    
'); ?>