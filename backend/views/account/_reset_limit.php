<?php

use common\models\PaymentReasons;
use common\widgets\Alert;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Accounts */
/* @var $form yii\widgets\ActiveForm */

?>
<?= Alert::widget() ?>
<div class="accounts-form card-box">
    <?php Pjax::begin(); ?>
    <?php $form = ActiveForm::begin(); ?>

    <?= Html::hiddenInput('Payments[user_id]', $user_id); ?>

    <?= $form->field($payments, 'reason')->dropDownList(ArrayHelper::map(PaymentReasons::find()->all(), 'id', 'title'), ['prompt' => 'Select reason'])->label('Reason') ?>

    <?= $form->field($payments, 'payment_note')->textarea(['rows' => 6])->label('Memo') ?>

    <?= $form->field($payments, 'payment_method')->dropDownList(['cash' => 'Cash', 'cheque' => 'Cheque', 'online' => 'Online'], ['prompt' => 'Select payment method']) ?>

    <?= $form->field($payments, 'amount')->textInput(['maxlength' => true, 'type' => 'number']) ?>

    <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">
            Close
        </button>
        <?= Html::submitButton($payments->isNewRecord ? 'Create' : 'Update', ['class' => $payments->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php Pjax::end(); ?>
</div>
