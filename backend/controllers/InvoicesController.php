<?php

namespace backend\controllers;

use backend\models\InvoicesSearch;
use common\models\EventSubscriptions;
use common\models\InvoiceItems;
use common\models\Invoices;
use common\models\Members;
use common\models\Payments;
use common\models\TblInvoice;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * InvoicesController implements the CRUD actions for Invoices model.
 */
class InvoicesController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoices models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvoicesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Invoices model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Invoices model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Invoices();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Invoices model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->invoice_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Invoices model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Invoices model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Invoices the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Invoices::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionMakePayemnt($invoice)
    {

        if ($invoice) {

            $inv_data = Invoices::findOne($invoice);
            $amountPaid = Payments::find()->where(['invoice_id' => $invoice])->sum('amount');

            $payments = new Payments();
            $payments->invoice_id = str_pad($inv_data->invoice_id, '7', '0', STR_PAD_LEFT);
            $payments->amount_payable = ($inv_data->total) - $amountPaid;
            $payments->payment_date = date('Y-m-d');
            $payments->received_by = Yii::$app->user->id;


            if ($payments->load(Yii::$app->request->post())) {

                if ($payments->save()) {

                    Yii::$app->getSession()->setFlash(
                        'success',
                        'Payment record created!'
                    );

                    $this->redirect('/invoices');

                } else {

                    Yii::$app->getSession()->setFlash(
                        'error',
                        'Payment not recorded!'
                    );

                    $this->redirect('/invoices');
                }

            }

            return $this->renderPartial('record_payment', [
                'model' => $payments,
                'invoice' => $inv_data,
            ]);
        }

    }

    /*
     * @param $subscriber
     * @return mixed
     */
    public function actionInvoicePdf($invoice)
    {
        $invoices = Invoices::findOne($invoice);
        $id = ($invoices <> null) ? $invoices->user_id : 0;
        $member = Members::findOne($id);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;
        $mpdf->WriteHtml($this->renderPartial('send_invoice', [
            'member' => $member,
            'invoice' => $invoices,
        ]));

        return $mpdf->Output('Invoice', 'I');
    }

    public function actionImportInvoice()
    {

        $count = 0;
        $query = TblInvoice::find();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 100
        ]);

        $prev_inv = $query->offset($pages->offset)->limit($pages->limit)->all();

        if (count($prev_inv) > 0) {

            foreach ($prev_inv as $reg) {

                $inv_id = ltrim($reg->user_invoice_id, '0');
                $invoice_date = date('Y-m-d',	$reg->invoice_date);

                $subscriptions = EventSubscriptions::find()->where(['gen_invoice' => $inv_id])->all();

                if ($reg->invoice_category == "Event Registration") {

                    if ($subscriptions <> null) {

                        $invoice = Invoices::findOne(['invoice_id' => $inv_id]);

                        if ($invoice == null) {
                            $invoice = new Invoices();
                        }

                        $invoice->invoice_id = ltrim($reg->user_invoice_id, '0');
                        $invoice->user_id = $reg->parent_user_id;
                        $invoice->invoice_related_to = 'event';
                        $invoice->invoice_rel_id = \Yii::$app->request->post('event_id');
                        $invoice->invoice_category = "Event Registration";

                        $invoice->payment_status = 'unpaid';
                        if ($reg->payment_status == "done") {
                            $invoice->payment_status = 'paid';
                        }

                        $invoice->invoice_rel_id = $reg->invoice_id;
                        $invoice->invoice_date = $invoice_date;

                        if ($invoice->save()) {

                            foreach ($subscriptions as $sub) {

                                $invoiceItems = new InvoiceItems();

                                $invoiceItems->invoice_id = $sub->gen_invoice;
                                $invoiceItems->user_id = $sub->user_id;
                                $invoiceItems->invoice_related_to = 'event';
                                $invoiceItems->invoice_rel_id = $sub->id;
                                $invoiceItems->invoice_category = ($sub->eventData <> null) ? $sub->eventData->title : "";
                                $invoiceItems->payment_status = 'unpaid';


                                if ($invoice->payment_status == "paid") {

                                    $invoiceItems->payment_status = 'paid';

                                }

                                $invoiceItems->subtotal = ($sub->fee_paid) ? $sub->fee_paid : 0;
                                $invoiceItems->amount = $invoiceItems->subtotal;

                                $invoiceItems->tax = ($sub->tax) ? $sub->tax : 0;
                                $invoiceItems->invoice_date = $invoice_date;

                                if (!$invoiceItems->save()) {
                                    $this->addError('error', $invoiceItems->getErrors());
                                }

                                if ($invoice->invoice_id) {

                                    $updateInv = Invoices::findOne($invoice->invoice_id);

                                    $updateInv->subtotal = $sub->subtotal;
                                    $updateInv->tax = $sub->tax;
                                    $updateInv->total = $sub->fee_paid;
                                    $updateInv->invoice_rel_id = ($sub->eventData <> null) ? $sub->eventData->id : "0";

                                    if (!$updateInv->save()) {
                                        return ['errors' => \yii\widgets\ActiveForm::validate($updateInv)];
                                    }
                                }
                            }

                        }
                    }
                } else {

                    $invoice = Invoices::findOne(['invoice_id' => $inv_id]);
                    $member = Members::findOne( $reg->parent_user_id);

                    if ($invoice == null) {
                        $invoice = new Invoices();
                    }

                    $invoice->invoice_id = ltrim($reg->user_invoice_id, '0');


                    $invoice->user_id = $reg->parent_user_id;
                    $invoice->invoice_related_to = 'member';
                    $invoice->invoice_rel_id = $reg->parent_user_id;
                    $invoice->invoice_category = $reg->invoice_category;
                    $invoice->invoice_date = $invoice_date;

                    $invoice->payment_status = 'unpaid';
                    if ($reg->payment_status == "done") {
                        $invoice->payment_status = 'paid';
                    }

                    if ($invoice->save()){

                        $group = ($member <> null) ? ($member->group <> null) ? $member->group : null : null;

                        $membership = new InvoiceItems();

                        $membership->tax = 0;
                        $membership->subtotal = ($reg->amount) ? $reg->amount : 0;
                        $membership->amount = $membership->subtotal;


                        $membership->invoice_id = $invoice->invoice_id;
                        $membership->user_id = $invoice->user_id;
                        $membership->invoice_related_to = "membership";
                        $membership->invoice_rel_id = ($member <> null) ? $member->group_id : "0";
                        $membership->invoice_category = $reg->invoice_category;
                        $membership->payment_status = "unpaid";

                        /*if(strtotime($reg->created_date) >= strtotime(date('2017-12-31'))) {

                            $membership->tax = $membership->subtotal - (5/100 * $membership->subtotal);
                            $membership->amount = $membership->tax + $membership->subtotal;

                        }*/


                        if (!$membership->save()) {
                            $errors [] = $membership->getErrors();
                        }
                    }
                }
            }

            $page = Yii::$app->request->get("page") + 1;
            $url = '/invoices/import-invoice?page='. $page .'&per-page=20';

            // echo '<script> window.location = "' . $url .'";</script>';

        } else {
            echo "no record";
        }
    }
}