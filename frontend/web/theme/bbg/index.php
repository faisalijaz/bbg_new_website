<?php include ("header.php");?>
<?php include ("home-banner.php");?>
<section class="MainArea chairmanmessage">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                <div class="CMessage">
                    <h2>CHAIRMAN MESSAGE</h2>
                    <p>
                        Thank you for the ringing endorsement of my bid to lead our group over the 2017/18 term; a real privilege as I enter my own 20th year here in Dubai. I am humbled by the overwhelming congratulatory feedback and positive ideas for improvement and change that endorse my own.
                        I know that we, as members, are hugely grateful to the committee, operations team, focus group chairs and previous BBG Chairs for the legacy we enjoy now. However, we are not afraid to be self-critical when it comes to focus and delivery of real, tangible.
                    </p>
                    <button class="Mybtn pull-right" type="button" > More Detail</button>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <img src="resources/images/chairman.jpg" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>
<section class="EventSwiper">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Upcoming Events</h3>
                </div>
                <!--Carousel Wrapper-->
                <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top">
                        <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->


                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">

                        <!--First slide-->
                        <div class="carousel-item active">

                            <div class="col-md-3">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--/.First slide-->

                        <!--Second slide-->
                        <div class="carousel-item">

                            <div class="col-md-3">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/e1.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>
                        <!--/.Second slide-->

                    </div>
                    <!--/.Slides-->

                </div>
                <!--/.Carousel Wrapper-->
            </div>
        </div>
    </div>
</section>
<section class="cmty">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Committee members</h3>
                </div>
                <!--Carousel Wrapper-->
                <div id="multi-item-example0" class="carousel slide carousel-multi-item" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top">
                        <a class="btn-floating" href="#multi-item-example0" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item-example0" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->


                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">

                        <!--First slide-->
                        <div class="carousel-item active">

                            <div class="col-md-4">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>

                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>

                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>

                        </div>
                        <!--/.First slide-->

                        <!--Second slide-->
                        <div class="carousel-item">

                            <div class="col-md-4">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>

                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>

                            <div class="col-md-4 clearfix d-none d-md-block">
                                <div class="Slidescmty text-center">
                                    <img class="img-fluid" src="resources/images/chairman.jpg" alt="">
                                    <h3>Jonathan MacPherson</h3>
                                    <h4>Deputy Chairman</h4>
                                </div>
                            </div>
                            <!--/.Second slide-->

                        </div>
                        <!--/.Slides-->

                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
</section>
<section class="News">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Latest News</h3>
                </div>
                <!--Carousel Wrapper-->
                <div id="multi-item-example1" class="carousel slide carousel-multi-item" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top">
                        <a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
                        <a class="btn-floating" href="#multi-item-example1" data-slide="next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->


                    <!--Slides-->
                    <div class="carousel-inner" role="listbox">

                        <!--First slide-->
                        <div class="carousel-item active">

                            <div class="col-md-3">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <!--/.First slide-->

                        <!--Second slide-->
                        <div class="carousel-item">

                            <div class="col-md-3">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 clearfix d-none d-md-block">
                                <div class="Slides text-center">
                                    <img class="img-fluid" src="resources/images/news.jpg" alt="">
                                    <div class="Slides-body">
                                        <h4>Networking with the Swedish</h4>
                                        <p>Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                        <button class="Mybtn" type="button" >Read More</button>
                                    </div>
                                </div>
                            </div>
                            <!--/.Second slide-->

                        </div>
                        <!--/.Slides-->

                    </div>
                    <!--/.Carousel Wrapper-->
                </div>
            </div>
        </div>
</section>
<section class="socialfeeds">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="fbplugin">
                    <h2>Facebook</h2>
                    <div class="fb-page" data-href="https://www.facebook.com/BBGOnline/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BBGOnline/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BBGOnline/">British Business Group, Dubai &amp; Northern Emirates</a></blockquote></div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="fbplugin">
                    <h2>Twitter</h2>
                    <div class="fb-page" data-href="https://www.facebook.com/BBGOnline/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BBGOnline/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BBGOnline/">British Business Group, Dubai &amp; Northern Emirates</a></blockquote></div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <div class="fbplugin">
                    <h2>Linked In</h2>
                    <div class="fb-page" data-href="https://www.facebook.com/BBGOnline/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true"><blockquote cite="https://www.facebook.com/BBGOnline/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/BBGOnline/">British Business Group, Dubai &amp; Northern Emirates</a></blockquote></div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
