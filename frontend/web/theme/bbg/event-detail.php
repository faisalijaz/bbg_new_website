<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;


$this->title = 'Event Details';
$this->params['breadcrumbs'][] = $this->title;

?>
<?= TopBannerWidget::widget(); ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Event Details</h3>
                        <h4>Summer Networking with the American Business Council at Vivaldi</h4>
                    </div>
                    <div class="EventDetail">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-64 col-lg-6 col-xl-6">
                                <img src="resources/images/event-pic-large.png" class="img-fluid" alt="">
                            </div>
                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                <div class="EventDetailText">
                                    <p>
                                        <span class="BoldText">Event Date:</span> Wed, 02 August , 2017 <br>
                                        <span class="BoldText">Event Time:</span> 7:00 PM to 9:00 PM <br>
                                        <span class="BoldText">Venue:</span> Vivaldi <br>
                                        <span class="BoldText">Location:</span> Sheraton Dubai Creek, Dubai, <br>
                                    </p>
                                    <span class="ViewMap"><a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> View Map</a></span>
                                    <p>
                                        <br>
                                        <span class="BoldText">Member Registrations:</span> 50 AED <br>
                                        <span class="BoldText">Non Members Fee:</span> 180 AED <br>
                                    </p>
                                </div>
                                <span class="RegisterEventbtn"><a href="steps.php"><i class="fa fa-map-marker" aria-hidden="true"></i> Register Event</a></span>
                            </div>
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="DescriptionHeading">
                                    <h3>Description</h3>
                                    <h4>Summer Networking with the American Business Council at Vivaldi</h4>
                                    <p>Join members and guests of the BBG and the American Business Council for an evening of networking at Vivaldi by Alfredo Russo Restaurant & Lounge. Heralded as one of the best Italian Restaurants in Dubai, Vivaldi offers a creative and delicious menu.
                                        <br> The restaurant has a trendy bar that is accessible throughout the day, and terrace for al fresco dining overlooking the Creek and Skyline of Dubai. Enjoy the elegant Italian atmosphere with complimentary drinks and canapes.
                                        Don't succumb to the summer slowdown! Get an edge on your competition with BBG networking nights!
                                        Confirm your attendance by 12 noon on Monday, 31st July. <br> This is a Pre Pay Event, your place will not be confirmed until payment is received. Cancellations accepted before 12 noon Monday, 31st July, ‘’No show fees’’ applicable after that date.
                                        <br>The BBG Dubai and Northern Emirates is the region’s foremost business-to-business membership networking group which enables like-minded professionals to further their business interests in the UAE and the wider region.</p>
                                </div>
                                <div class="JointEvent">
                                    <h2>Joint event with:</h2>
                                    <img src="resources/images/jointevent.png" alt="">
                                    <h2>Fee includes drinks and canapes <br>
                                        Valet parking available <br>
                                        Dress: Business</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?php include ("RightSide.php");?>
            </div>
         </div>
    </div>
</section>
