<div class="hidden-md-down col-lg-9 col-xl-9">
    <nav class="float-right">
        <ul id='menu'>
            <!-- <li>
                                <?/*= Html::a('Home', ['/'], ['class' => ($url == '/') ? 'active home' : 'home']); */?>
                            </li> -->

                            <li>
                                <?= Html::a('About BBG', ['/pages/about-bbg'], ['class' => 'prett']) ?>
                                <ul class='menus'>
                                    <li class='has-submenu'>
                                    <li><?= Html::a('Sponsors', ['/sponsors'],
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a('Objectives', ['/pages/view-by-seo-url', 'seo_url' => 'objectives'],
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a('BBG Profile', ['/pages/view-by-seo-url', 'seo_url' => 'bbg-profile'],
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a("Chairman's Message", ['/pages/view-by-seo-url', 'seo_url' => 'chairman-message'],
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a("Constitution", ['/pages/view-by-seo-url', 'seo_url' => 'constitution'],
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a('Committee', '/site/committee-info',
                ['class' => 'home']); ?></li>
                                    <li><?= Html::a("Focus Groups", ['/pages/view-by-seo-url', 'seo_url' => 'focus-groups'],
                ['class' => 'home']); ?></li>

                                    <li class='has-submenu'>
                                        <?= Html::a('Information', ['/pages/about-bbg'], ['class' => 'prett']) ?>
                                        <ul class='submenu'>
                                            <?php
            if (!\Yii::$app->user->isGuest) {
                ?>
                                                <li><?= Html::a("AGM", ['/pages/view-by-seo-url', 'seo_url' => 'agm']); ?></li>
                                                <?php
            }
            ?>
                                            <li><?= Html::a("Doing Business in Dubai", ['/pages/view-by-seo-url', 'seo_url' => 'doing-business-in-dubai']); ?></li>
                                            <li><?= Html::a("Trade Visit to Dubai", ['/pages/view-by-seo-url', 'seo_url' => 'trade-visit-to-dubai']); ?></li>
                                            <li><?= Html::a("PROTECTION OF INTELLECTUAL PROPERTY", ['/pages/view-by-seo-url', 'seo_url' => 'poip']); ?></li>
                                            <li><?= Html::a("USEFUL LINKS", ['/pages/view-by-seo-url', 'seo_url' => 'useful-links']); ?></li>

                                        </ul>
                                    </li>




                                </ul>
                            </li>

                            <li>
                                <?= Html::a('Events', ['/events'], ['class' => 'prett']) ?>
                                <ul class='menus'>
                                    <li class='has-submenu'>
                                        <?= Html::a('Upcoming Events', ['/events/'], ['class' => 'home']) ?>
                                        <?= Html::a('Past Events', ['/events/', 'type' => 'past'], ['class' => 'home']) ?>
                                        <!--<ul class='submenu'>
                                            <li><a href="#" title="Sub Menu">Sub Menu</a></li>
                                        </ul>-->
            </li>
        </ul>
        </li>
        <?php
        if (!$userData->isGuest) {
            ?>
            <li>
                <?= Html::a('Directory', ['/account/member-directory'], ['class' => 'prett']) ?>
                <ul class='menus'>
                    <li class='has-submenu'>
                        <?= Html::a('Member Directory', ['/account/member-directory'], ['class' => 'home']) ?>
                        <?= Html::a('Company Directory', ['/account/company-directory'], ['class' => 'home']) ?>

                    </li>
                </ul>
            </li>
            <?php
        }
        ?>
        <li>
            <?= Html::a('Membership', ['/site/apply-membership'], ['class' => 'prett']) ?>
            <ul class='menus'>
                <li class='has-submenu'>
                    <?= Html::a('Join Us', ['/site/apply-membership'], ['class' => 'home']) ?>
                </li>
            </ul>
        </li>
        <li>
            <?= Html::a('News', ['/news'], ['class' => 'prett']) ?>
            <ul class='menus'>
                <li><?= Html::a('Member News', ['/news', 'type' => 'member'], ['class' => 'home']); ?></li>
                <li><?= Html::a('Subscribe', ['/news/subscribe-newsletter/'], ['class' => 'home']); ?></li>
            </ul>
        </li>
        <li><?= Html::a('Contact', ['site/contact'], ['class' => 'home']) ?></li>
        </ul>
    </nav>
</div>