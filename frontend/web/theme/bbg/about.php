<?php include ("header.php");?>
<?php include ("banner.php");?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>About BBG</h3>
                    </div>
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="SiteText">
                                    <p>
                                        Currently one of the largest and most active business groups in the GCC, the British Business Group Dubai & Northern Emirates (BBG) operates under the auspices of the Dubai Chamber of Commerce & Industry and is closely aligned to the UAE office of UKTI (UK Trade and Investment).

                                        With a membership of more than 2000 individuals representing more than 400 companies, one of the group's prime objectives is communication with:

                                        -	Businessmen and women in the UAE and UK;
                                        -	Policy-makers and decision-makers in government and business in UK and UAE;
                                        -	The UAE leadership and influencers;
                                        -	Other British business groups.

                                        Founded in 1987, the BBG is led by a pro-bono committee of 12 annually elected members, each of whom overseas a different aspects of the workings of the group.
                                    </p>
                                    <p>
                                        Currently one of the largest and most active business groups in the GCC, the British Business Group Dubai & Northern Emirates (BBG) operates under the auspices of the Dubai Chamber of Commerce & Industry and is closely aligned to the UAE office of UKTI (UK Trade and Investment).

                                        With a membership of more than 2000 individuals representing more than 400 companies, one of the group's prime objectives is communication with:

                                        -	Businessmen and women in the UAE and UK;
                                        -	Policy-makers and decision-makers in government and business in UK and UAE;
                                        -	The UAE leadership and influencers;
                                        -	Other British business groups.

                                        Founded in 1987, the BBG is led by a pro-bono committee of 12 annually elected members, each of whom overseas a different aspects of the workings of the group.
                                    </p>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?php include ("RightSide.php");?>
            </div>
         </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>
