<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>company directory</h3>
                <br>
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="#">A</a></li>
                            <li class="page-item"><a class="page-link" href="#">B</a></li>
                            <li class="page-item"><a class="page-link" href="#">C</a></li>
                            <li class="page-item"><a class="page-link" href="#">D</a></li>
                            <li class="page-item"><a class="page-link" href="#">E</a></li>
                            <li class="page-item"><a class="page-link" href="#">F</a></li>
                            <li class="page-item"><a class="page-link" href="#">G</a></li>
                            <li class="page-item"><a class="page-link" href="#">H</a></li>
                            <li class="page-item"><a class="page-link" href="#">I</a></li>
                            <li class="page-item"><a class="page-link" href="#">J</a></li>
                            <li class="page-item"><a class="page-link" href="#">K</a></li>
                            <li class="page-item"><a class="page-link" href="#">L</a></li>
                            <li class="page-item"><a class="page-link" href="#">M</a></li>
                            <li class="page-item"><a class="page-link" href="#">N</a></li>
                            <li class="page-item"><a class="page-link" href="#">O</a></li>
                            <li class="page-item"><a class="page-link" href="#">P</a></li>
                            <li class="page-item"><a class="page-link" href="#">Q</a></li>
                            <li class="page-item"><a class="page-link" href="#">R</a></li>
                            <li class="page-item"><a class="page-link" href="#">S</a></li>
                            <li class="page-item"><a class="page-link" href="#">T</a></li>
                            <li class="page-item"><a class="page-link" href="#">U</a></li>
                            <li class="page-item"><a class="page-link" href="#">V</a></li>
                            <li class="page-item"><a class="page-link" href="#">W</a></li>
                            <li class="page-item"><a class="page-link" href="#">X</a></li>
                            <li class="page-item"><a class="page-link" href="#">Y</a></li>
                            <li class="page-item"><a class="page-link" href="#">Z</a></li>

                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3">
                        <div class="TopMostSearch1">
                            <div class="input-group">
                                <input type="search" class="form-control" placeholder="I’m looking for....">
                                <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row PaddingTopBtm30px">
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                        <div class="directorymember">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                    <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                                </div>
                                <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                    <div class="directory">
                                        <h2>Action Impact Events (LLC)</h2>
                                        <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusantium aperiam culpa cum dolor itaque.
                                        </h3>
                                        <span class="EventDetail"><a href="event-detail.php">View Detail</a></span>
                                        <span class="EventDetail"><a href="event-detail.php">View Member</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



