// Handeling of step wizard

$(document).ready(function () {

    $('#step-1-btn').on('click', false);


    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});

/**
 * Function hides or display guest field on the basis of number entered
 *
 */
$(document).ready(function () {

    // Guest section on page load and make it empty

    $("#section_invite_guest").hide();
    $("#guestInformation").html('');

    $("#step-2").prop('disabled', true);
    // $("#registeredUsersTable").dataTable();

    /**
     * Function hides or display guest field on the basis Checkbox checked or unchecked
     */
    $("#inviteGuest").click(function () {

        if ($(document).find('#inviteGuest').is(':checked')) {

            var num_guest = $("#input_guest_number").val();
            var total = parseInt(num_guest) * parseInt($("#nonmember_fee").text());
            $(".nonmember_fee_total").text(total);
            $("#guestsNumbers").text(num_guest);
            $("#guestInformation").html(guestInformation(num_guest));

            $("#section_invite_guest").show();
        } else {
            $("#section_invite_guest").hide();
            $("#guestInformation").html('');
        }
    });

    /**
     * Function hides or display guest field on the basis of number entered
     */
    $("#input_guest_number").focusout(function (e) {

        var num_guest = $(this).val();
        var total = parseInt(num_guest) * parseInt($("#nonmember_fee").text());
        $(".nonmember_fee_total").text(total);
        $("#guestsNumbers").text(num_guest);
        $("#guestInformation").html(guestInformation(num_guest));

    });

    var total = parseInt($("#input_guest_number").val()) * parseInt($("#nonmember_fee").text());

    $(".nonmember_fee_total").text(total);
    $("#guestInformation").html('');
    /**
     * if Option for inform to other members is selected then
     */
    $('body').unbind().on('click', '.memberInfoBtn', function (e) {
        if ($(this).is(':checked')) {
            if ($(this).val() == '1') {
                $("#SelectBoxMemberInfo").removeClass('hidden');
            } else {
                $("#SelectBoxMemberInfo").addClass('hidden');
            }
        }
    });

    $('body').on('click', '#sendInfoBtn', function (e) {

        $(this).prop('disabled', true);

        var event = $("#event_id_infoBtn").val();
        var members = $("#membersList").val();

        if (members != null && members != '') {
            $.ajax({

                url: '/events/inform-about-event',
                type: 'POST',
                data: {'event': event, 'members': members},

                success: function (data) {
                    $("#sendInfoBtn").prop('disabled', false);
                    swal({
                        title: 'Success!',
                        text: 'Event intimation sent successfully!',
                        type: 'success',
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#sendInfoBtn").prop('disabled', false);
                    swal({
                        title: 'Error!',
                        text: + jqXHR.statusText,
                        type: 'error',
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });

                }
            });

        } else {
            $("#sendInfoBtn").prop('disabled', false);
            swal({
                title: 'Error!',
                text: 'Please select members to send',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
        }
    });

});

/**
 * Submit form and validate fields
 *
$('body').unbind().on('click', '#EventRegisterBtn', function (e) {

    e.preventDefault();
    $(this).prop("disabled", true);
    console.log("sadfa");


        // Check if guest checkbox is selected and any of the fields is empty
        if (!isValidGuestInput() && $(document).find('#inviteGuest').is(':checked')) {
            swal({
                title: 'Error!',
                text: 'Please Fill out all fields in guest information',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            return false;
        } else {
            // Check if guest checkbox is not selected and also not any contact selected
            if (!isValidateFormContacts() && !$(document).find('#inviteGuest').is(':checked')) {
                swal({
                    title: 'Error!',
                    text: 'Please select at-least one contact',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
                return false;
            }
        }


        e.preventDefault();


        var form = $(this);
        var formData = form.serialize();

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: formData,
            success: function (data) {

                var response = JSON.parse(data.split('|')[0]);
                // console.log(response.users);
                // console.log(data);

                swal({
                    title: 'Success!',
                    text: 'Event registered!',
                    type: 'success',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });

                // window.location.href = "/events/payment?id=" + $("#event_id").val();

                //$("#EventUsers").html(response.users);
                //$("#totalAmountEventRegistered").text(response.total + " AED");
                //$('#nextInformation1').trigger('click');

            },
            error: function (e) {
                swal({
                    title: 'Error!',
                    text: 'Form submit Error!',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
            }
        });

});

 */

function isValidateFormContacts() {
    valid = true;

    if ($("body").find('.event_member_contacts:checked').length == 0) {
        valid = false;
    }
    return valid;
}

function isValidGuestInput() {

    valid = true;

    $('.input_guest_info').each(function () {
        if ($(this).val() == '') {
            return valid = false;
        }
    });

    return valid;
}

function guestInformation(num_guest) {

    var html = '';

    for (var i = 0; i < num_guest; i++) {
        html += '   <div class="form-group">' +
            '           <table class="table table-bordered table-responsive d-lg-table d-md-table d-sm-table d-table">' +
            '               <tbody>' +
            '                   <tr>' +
            '                       <td class="Redbg">' + parseInt(i + 1) + '</td>' +
            '                       <td class="paddingtop20px">' +
            '                           <div class="row PaddingRL">' +
            '                               <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">' +
            '                                   <input type="text" name="guestInfo[' + i + '][name]" required class="form-control input_guest_info" placeholder="Full Name">' +
            '                               </div>' +
            '                               <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">' +
            '                                   <input type="text" name="guestInfo[' + i + '][phone_number]" required class="form-control input_guest_info"  placeholder="Phone Number">' +
            '                               </div>' +
            '                           </div>' +
            '                           <div class="row PaddingRL">' +
            '                               <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">' +
            '                                   <input type="email" name="guestInfo[' + i + '][email]" required class="form-control input_guest_info"  placeholder="Email Address">' +
            '                               </div>' +
            '                               <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">' +
            '                                   <input type="text" name="guestInfo[' + i + '][company]" required class="form-control input_guest_info" placeholder="Company Name">' +
            '                               </div>' +
            '                           </div>' +
            '                           <div class="row PaddingRL">' +
            '                               <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">' +
            '                                   <div class="checkbox">' +
            '                                       <label>' +
            '                                           <input type="checkbox" class="dietPref" name="guestInfo[' + i + '][diet_option]" value="" id="'+ i +'">' +
            '                                           <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>' +
            '                                           <span>Dietry Preference</span>' +
            '                                       </label>' +
            '                                   </div>' +
            '                               </div>' +
            '                               <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">' +
            '                                   <select class="hidden select_diet_pref form-control " id="select_diet_pref' + i + '" name="guestInfo[' + i + '][other_diet_option_description]">' +
            '                                       <option value="">Select ...</option>' +
            '                                       <option value="Standard">Standard</option>' +
            '                                       <option value="Vegetarian">Vegetarian</option>' +
            '                                       <option value="other">Other Dietary</option>' +
            '                                   </select>' +
            '                               </div>' +
            '                               <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">' +
            '                                   <textarea rows="4" class="hidden" id="extraselect_diet_pref'+ i +'" name="guestInfo[' + i + '][other_diet_option_description]" style = "width: 100%; height: 150px;">' +
            '                                   </textarea>' +
            '                               </div>' +
            '                           </div>' +
            '                       </td>' +
            '                   </tr>' +
            '               </tbody>' +
            '           </table>' +
            '       </div>';

    }

    if (num_guest != 0 && typeof num_guest != 'undefined' && !isNaN(num_guest)) {
        return html;
    }

    return '';
}

function ArrayToObject(arr) {
    var obj = {};
    for (var i = 0; i < arr.length; i++) {
        obj[arr[i]] = arr[i];
    }
    return obj
}


$('body').on('click', '.cancel-request', function (e) {

    e.preventDefault();

    var id = $(this).attr("id");
    var cancel = 0;

    $(this).prop('disabled', true);

    var $html = '<a id="' + id + '" class="cancel-request cancel-true" href="#" style="color: red;" title="Cancel Request"><i class="fa fa-minus-circle" aria-hidden="true"></i></a>'
    var $msg = 'Cancellation request submitted is undone!';

    if ($(this).hasClass("cancel-true")) {
        cancel = 1;
        var $html = '<a id="' + id + '" class="cancel-request cancel-false" style="color: red;" href="#" title="Cancel Request"><i class="fa fa-minus-circle" aria-hidden="true"></i> Undo </a>'
        var $msg = 'Cancellation request submitted!';
    }

    $.ajax({

        url: '/events/subscription-cancel-request',
        type: 'POST',
        data: {'subscription': id, 'cancel': cancel},

        success: function (data) {
            // console.log(data);
            $(this).prop('disabled', false);

            if (data == 1) {
                swal({
                    title: 'Success!',
                    text: $msg,
                    type: 'success',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });
            }

            $("body").find(".cancel-request-" + id).html($html);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(this).prop('disabled', false);
            swal({
                title: 'Error!',
                text: +jqXHR.statusText,
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
        }
    });

});




$(document).on("click", '.dietPref', function(){

    var $id = $(this).attr("id")

    if($(this).is(":checked")){

        $("#select_diet_pref"+$id).removeClass("hidden");

    } else {

        $("#extraselect_diet_pref"+$id).val("");
        $("#select_diet_pref"+$id).val("");
        $("#select_diet_pref"+$id).addClass("hidden");
        $("#extraselect_diet_pref"+$id).addClass("hidden");
    }

});

$(document).on("change", '.select_diet_pref', function(){

    var ids = $(this).attr("id");

    if($(this).val() == "other"){
        $("#extra"+ids).removeClass("hidden");
    }else{
        $("#extra"+ids).val("");
        $("#extra"+$id).addClass("hidden");
    }
});
