<footer>
    <section class="footerarea">
        <div class="container">
            <div class="row footerwidgets">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Phone</h2>
                        <p>Telephone: +971 4 3970303 <br>
                            Fax: +971 4 3970939 <br>
                            Email: info@bbgdxb.com <br>
                            Working Hours: Sun-Thu / 9am to 5pm
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Address</h2>
                        <p>BBG Executive Office, <br>
                            P.O. Box 9333, Dubai,<br>
                            The David May Building, British Embassy Al Seef Road, Dubai, United Arab Emirates.
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Subscribe For Newsletter</h2>
                        <p>Get the latest updates</p>
                        <div class="input-group">
                            <input type="search" class="form-control" placeholder="Enter Your Email Address..">
                            <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        <span class="socialmedia">
                        <a href="#"><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                    </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 CopyR">
                    <p>
                        CopyRights © 2017 - 2018 British Business Group. All Right Reserved. <br>
                        Designed & Developed By: 800Wisdom.ae
                    </p>
                </div>
            </div>
        </div>

    </section>
</footer>
<div id="fb-root"></div>
</body>
<script type="text/javascript" src="resources/js/jquery-2.2.4.js"></script>
<script src="resources/js/bootstrap.js" crossorigin="anonymous"></script>
<script src="resources/calender/tempust.js"></script>
<script type="text/javascript" src="resources/js/jquery.flexisel.js"></script>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "100%";
        document.getElementById("main").style.marginLeft = "100%";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.marginLeft= "0";
    }
</script>
<script>
    $("#tempust").tempust({
        date: new Date("2017/11/12"),
        offset: 1,
        events: {
            "2017/11/12": $("<div>Event Name Here</div>")
        }
    });
    $("#tempust").on("changeDate", function (event) {
        $("#output")
            .append("Date Changed!")
            .append("<br/>");
    });
</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

</script>

<script type="text/javascript">

    $(window).load(function() {
        $("#flexiselDemo1").flexisel();

        $("#flexiselDemo2").flexisel({
            visibleItems: 4,
            itemsToScroll: 3,
            animationSpeed: 200,
            infinite: true,
            navigationTargetSelector: null,
            autoPlay: {
                enable: false,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint:480,
                    visibleItems: 1,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint:640,
                    visibleItems: 2,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint:768,
                    visibleItems: 3,
                    itemsToScroll: 3
                }
            },
            loaded: function(object) {
                console.log('Slider loaded...');
            },
            before: function(object){
                console.log('Before transition...');
            },
            after: function(object) {
                console.log('After transition...');
            },
            resize: function(object){
                console.log('After resize...');
            }
        });

        $("#flexiselDemo3").flexisel({
            visibleItems: 4,
            itemsToScroll: 1,
            navigationTargetSelector: null,
            autoPlay: {
                enable: true,
                interval: 5000,
                pauseOnHover: true
            }
        });

        $("#flexiselDemo4").flexisel({
            infinite: false
        });

    });

</script>
<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus()
    })
</script>

</html>