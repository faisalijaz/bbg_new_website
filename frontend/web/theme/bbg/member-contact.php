<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>member’s contact</h3>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Personal Contact Information </h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                            <div class="profile">
                                <h2>Abhilash Nair</h2>
                                <h3>Regional Manager - Removal Division at inchcape Shipping Services Dubai L.L.C</h3>
                                <p>
                                    <strong>Membership</strong> - Alternate <br>
                                    <strong>Designation</strong> - Regional Manager - Removal Division <br>
                                    <strong>Company Name</strong> - Inchcape Shipping Services L.L.C <br>
                                    <strong>Mobile Number</strong> - 00971501234567 <br>
                                    <strong>Email Address</strong> - asdfggh@domainname.com <br>
                                    <strong>Member Since</strong> - 18 August 2015 <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



