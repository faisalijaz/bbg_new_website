(function() {
    'use strict';

    angular
        .module('tourDubai')
        .controller('CheckoutController', CheckoutController);

    /** @ngInject */
    function CheckoutController($timeout, $filter, $window, CheckoutService) {
        var vm = this;

        vm.pickup = {
            city: null,
            area: null,
            hotel: null,
            room: '',
            phone_number: ''
        };

        vm.areas = [];
        vm.cities = [];
        vm.hotels = [];


        vm.showPickup = function () {
            $('#pickupModal').modal();
        };

        vm.submitPickup = function () {

            var pickup  = vm.pickup;
            var valid = true;

            if(pickup.city == null) {
                swal({
                    title: 'City',
                    text: 'Please select pickup city',
                    type: 'error'
                });

                valid = false;
            }

            if(pickup.area == null) {
                swal({
                    title: 'Area',
                    text: 'Please select pickup Area',
                    type: 'error'
                });

                valid = false;
            }

            if(pickup.hotel == null) {
                swal({
                    title: 'Hotel',
                    text: 'Please select pickup Hotel',
                    type: 'error'
                });

                valid = false;
            }


            if(pickup.room == '') {
                swal({
                    title: 'Room',
                    text: 'Please provide your room address',
                    type: 'error'
                });

                valid = false;
            }

            if(pickup.phone_number == '') {
                swal({
                    title: 'Phone number',
                    text: 'Please provide your phone number',
                    type: 'error'
                });
                valid = false;
            }

            if(valid) {
                $('#pickupModal').modal('hide');
            }
        };


        vm.getAreas = function () {

            CheckoutService.getCityAreas(vm.pickup.city).then(function (response) {
                vm.areas = response;
            });

        };
        vm.getAreas();


        vm.getCities = function () {

            CheckoutService.getCities().then(function (response) {
                vm.cities = response;
            });

        };
        vm.getCities();

        vm.getHotels = function () {

            CheckoutService.getHotels().then(function (response) {
                vm.hotels = response;
            });

        };
        vm.getHotels();

        vm.hasAddress = function() {
            if(vm.pickup.city !== undefined && vm.pickup.city !== null) {
                return true;
            } else {
                return false;
            }
        };

        vm.getPickUpAddress = function( type ) {

            var address;

            if(type === 'city') {
                address = $filter('filter')(vm.cities, { id: vm.pickup.city }) [0];
            } else if(type === 'area') {
                address = $filter('filter')(vm.areas, { id: vm.pickup.area }) [0];
            } else if(type === 'hotel') {
                address = $filter('filter')(vm.hotels, { id: vm.pickup.hotel }) [0];
            }


            return (address !== undefined ? address.name : '' );
        };

        vm.initDomData  = function () {
            $timeout(function () {
                vm.pickup = $window.pickup;
            }, 100);
        };

        vm.initDomData();

    }
})();