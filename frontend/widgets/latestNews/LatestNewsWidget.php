<?php

namespace frontend\widgets\latestNews;

use common\models\News;
use yii\base\Widget;
use common\models\CommitteMembers;
/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class LatestNewsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'carousel';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Committee Members';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        LatestNewsWidgetAsset::register($this->getView());

        $news = News::find()->where(['isPublished' => '1'])->all();

        return $this->render($this->view, [
            'news' => $news
        ]);
    }
}
