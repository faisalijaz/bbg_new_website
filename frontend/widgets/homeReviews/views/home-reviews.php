<?php
?>

<div class="clip">
    <div class="bg bg-bg-chrome"
         style="background-image: url('https://s3-ap-southeast-1.amazonaws.com/tour-booking//reviews-bg595df838aa515.jpg');">
    </div>
</div>
<div class="pages-text-transparent swiper-container testi-3" data-autoplay="0" data-loop="1" data-speed="1000"
     data-center="0" data-slides-per-view="1" id="testi-slider-3">
    <div class="swiper-wrapper">
        <?php foreach ($reviews as $review) { ?>
            <div class="swiper-slide">
                <div class="testi-wrap">
                    <div class="qq">
                        <img src="theme/img/quote.png" alt="">
                    </div>
                    <p><?= $review->description; ?></p>
                    <h4><b>- <?= $review->client_name; ?> -</b></h4>
                </div>
            </div>
        <?php } ?>

    </div>
    <div class="pagination poin-style-1"></div>
</div>