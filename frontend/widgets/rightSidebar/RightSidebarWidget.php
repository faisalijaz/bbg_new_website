<?php

namespace frontend\widgets\rightSidebar;

use common\models\Widgets;
use frontend\models\EventCalendar;
use Yii;
use yii\base\Widget;
use common\models\Events;

/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class RightSidebarWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'sidebar';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Right Sidebar';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        RightSidebarWidgetAsset::register($this->getView());

        $upcoming_events = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])
            ->orderBy(['event_startDate' => 'ASC'])->limit(5)->all();

        $past_events = Events::find()->where(['<=', 'event_startDate', date('Y-m-d')])
            ->orderBy(['event_startDate' => 'ASC'])->limit(5)->all();

        return $this->render($this->view, [
            'upcomingEvents' => $upcoming_events,
            'pastEvents' => $past_events
        ]);
    }
}
