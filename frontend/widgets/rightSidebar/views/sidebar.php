<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-calendar" aria-hidden="true"></i> Events Calendar</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="EventCalender">
                    <div id="tempust"></div>
                    <!--<div id="output"><div>-->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-bullhorn" aria-hidden="true"></i> Upcoming events</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?php $events_data = [];
                if (count($upcomingEvents) > 0) {
                    foreach ($upcomingEvents as $event) {
                        $start_date = \Yii::$app->formatter->asDate($event->event_startDate, 'php:Y/m/d');
                        $events_data[$start_date] = "<a href='/events/event-details?id=".$event->id."' style='color:#fff;' >" . $event->title . "</a>";
                        ?>
                        <a href="/events/event-details?id=<?= $event->id; ?>">
                            <div class="UpcomingEvent1">
                                <h2><?= $event->title; ?></h2>
                                <h3>
                                    <b><?= \Yii::$app->formatter->asDate($event->event_startDate, 'php:D, d mm, Y'); ?></b>
                                </h3>
                                <p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
    <div class="SideHeading">
        <h3><i class="fa fa-history" aria-hidden="true"></i> Past events</h3>
    </div>
    <div class="SideCalender">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?php
                if (count($pastEvents) > 0) {
                    foreach ($pastEvents as $event) {
                        $start_date = \Yii::$app->formatter->asDate($event->event_startDate, 'php:Y/m/d');
                        $events_data[$start_date] = "<a href='/events/event-details?id=".$event->id."' style='color:#fff;' >" . $event->title . "</a>";
                        ?>
                        <a href="/events/event-details?id=<?= $event->id; ?>">
                            <div class="UpcomingEvent1">
                                <h2><?= $event->title; ?> </h2>
                                <h3>
                                    <b><?= \Yii::$app->formatter->asDate($event->event_startDate, 'php:D, d mm, Y'); ?></b>
                                </h3>
                                <p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
                            </div>
                        </a>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php $this->registerJs('
    
    var today = "' . date("Y/m/d") . '";
    var str = ' . \GuzzleHttp\json_encode($events_data) . ' 
        console.log(today);
    $("#tempust").tempust({
        date: new Date(today),
        offset: 1,
        events: str
    });
    
    if($("#tempust .tempust").length > 1){
        $("#tempust .tempust:first").remove()
    }
    
'); ?>