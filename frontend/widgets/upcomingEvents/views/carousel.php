<?php

use yii\helpers\Html;

if ($upcomingEvents <> null) {
    ?>
    <section class="EventSwiper">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="Heading text-center">
                        <h3>Upcoming Events</h3>
                    </div>
                    <!--Carousel Wrapper-->
                    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

                        <!--Controls-->
                        <div class="controls-top">
                            <a class="btn-floating" href="#multi-item-example" data-slide="prev">
                                <i class="fa fa-chevron-left"></i>
                            </a>
                            <a class="btn-floating" href="#multi-item-example" data-slide="next">
                                <i class="fa fa-chevron-right"></i>
                            </a>
                        </div>
                        <!--/.Controls-->

                        <!--Slides-->
                        <div class="carousel-inner" role="listbox">
                            <!--First slide-->
                            <div class="carousel-item active">
                                <?php
                                $visible = 0;
                                foreach ($upcomingEvents as $event) {
                                    if ($visible <= 3) {
                                        ?>
                                        <div class="col-md-3">
                                            <div class="Slides text-center">
                                                <img class="img-fluid" src="/theme/bbg/resources/images/e1.jpg" alt="">
                                                <div class="Slides-body">
                                                    <h4><?= $event->title; ?></h4>
                                                    <p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
                                                    <?= Html::a('<button class="Mybtn" type="button">Read More</button>', ['/events/event-details', 'id' => $event->id], []); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    $visible++;
                                }
                                ?>
                            </div>
                            <!--/.First slide-->

                            <!--Second slide-->
                            <div class="carousel-item">
                                <?php
                                $visible = 0;
                                foreach ($upcomingEvents as $event) {
                                    if ($visible > 3) {
                                        ?>
                                        <div class="col-md-3">
                                            <div class="Slides text-center">
                                                <img class="img-fluid" src="/theme/bbg/resources/images/e1.jpg" alt="">
                                                <div class="Slides-body">
                                                    <h4><?= $event->title; ?></h4>
                                                    <p><?= substr($event->short_description, 0, 90) . '...'; ?></p>
                                                    <?= Html::a('<button class="Mybtn" type="button">Read More</button>', ['/events/event-details', 'id' => $event->id], []); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    $visible++;
                                }
                                ?>
                                <!--/.Second slide-->
                            </div>
                            <!--/.Slides-->
                        </div>
                        <!--/.Carousel Wrapper-->
                    </div>
                </div>
            </div>
    </section>
    <?php
}
?>