<?php

namespace frontend\widgets\upcomingEvents;

use yii\base\Widget;
use common\models\Events;
/**
 * Class OrderWidget
 * @package backend\widgets\order
 */
class UpcomingEventsWidget extends Widget
{
    /**
     * Order report view type
     * @var string
     */
    public $view = 'carousel';

    /**
     * Report View title
     * @var string
     */
    public $title = 'Upcoming Events';

    /**
     * Init
     * @return void
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        // Register AssetBundle
        UpcomingEventsWidgetAsset::register($this->getView());

        $events = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])
                    ->orderBy(['event_startDate' => 'ASC'])->limit(10)->all();

        return $this->render($this->view, [
            'upcomingEvents' => $events
        ]);
    }
}
