<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $associate \common\models\LoginForm */

use frontend\widgets\topBanner\TopBannerWidget;
use kartik\checkbox\CheckboxX;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = 'Membership Application';
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        form div.required label.control-label:after {
            content: " * ";
            color: #a94442;
        }
    </style>
<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                        <div class="col-md-12 ">
                            <div class="Heading text-left">
                                <h3>Associate Member</h3><br/>
                            </div>
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_firstname')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_lastname')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_email')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_mobile')->textInput([])->label('Mobile') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_country')->dropDownList(
                                        \yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'country_name'),
                                        ['prompt' => 'Select ...'])->label('Country') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($associate, 'assosiate_city')->textInput([])->label() ?>
                                </div>


                            </div>



                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                            <?= Html::submitButton('Add Member', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                        </div>
                    </div>
                </div>
            </div>

    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($associate->getErrors()) > 0) {

    foreach ($associate->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}
?>