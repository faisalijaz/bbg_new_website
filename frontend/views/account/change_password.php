<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('header', [
    'breadcrumbs' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    'title' => $this->title
]);
?>

<div class="detail-wrapper">
    <div class="container">
        <div class="row padd-90">
            <?php echo $this->render('_sidebar', [
                'model' => $profile,
            ]); ?>
            <div class="col-xs-12 col-md-9">
                <div class="simple-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="f-login-content">
                                <h3 class="small-title"><?= Html::encode($this->title) ?></h3>

                                <?php $form = ActiveForm::begin(); ?>

                                <div class="col-md-6 col-sm-12 input-style-1 b-50 type-2 color-5">
                                    <?= $form->field($model, 'currentPassword')->passwordInput(['autofocus' => true, 'placeholder' => 'Enter your Current Password'])->label(false) ?>
                                </div>

                                <div class="col-md-6 col-sm-12 input-style-1 b-50 type-2 color-5">
                                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Enter new password'])->label(false) ?>
                                </div>

                                <div class="col-md-6 col-sm-12 input-style-1 b-50 type-2 color-5">
                                    <?= $form->field($model, 'repassword')->passwordInput(['autofocus' => true, 'placeholder' => 'Re-enter new passowrd'])->label(false) ?>
                                </div>

                                <div class="col-md-6 col-sm-12 input-style-1 b-50 type-2 color-5">
                                    <?= Html::submitButton('Update Password', ['class' => 'login-btn c-button full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'Update Email']) ?>
                                </div>

                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
