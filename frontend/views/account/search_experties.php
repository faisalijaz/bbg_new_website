<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;
use common\models\Members;

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;

    foreach ($companies_members as $companies_member){
        //echo $companies_member->id;
        $query_members = "SELECT * FROM `accounts` WHERE company = $companies_member->id";
        $search_members[] = Members::findBySql($query_members)->all();
    }

    foreach ($members as $member){
        $query = "SELECT * FROM `accounts` WHERE id = $member->id";
        $query_accounts[] = Members::findBySql($query)->all();
    }

    $unique_record = array_merge($query_accounts , $search_members);

    foreach($unique_record as $memb ){
        foreach ($memb as $row){
            $temp_array[] = $row->id;
        }
    }
    $temp_array = array_unique($temp_array);

    foreach ($temp_array as $record){
        $query = "SELECT * FROM `accounts` WHERE id = $record";
        $members_data[] = Members::findBySql($query)->all();
    }

?>

<?/*= TopBannerWidget::widget(); */?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Search Results</h3>
                    </div>
                    <hr/>
                    <div class="committee text-center">
                        <div class="row">
                            <?php
                            if ($members_data <> null) {
                            foreach($members_data as $members ){
                                foreach ($members as $member) {  ?>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
                                        <img src="<?= ($member->picture) ? $member->picture : Yii::$app->params['no_image']; ?>"
                                             class="committee_staff_image img-fluid"
                                             alt="<?= $member->first_name . " " . $member->last_name; ?>">
                                        <div class="name_and_postion">
                                            <h5><?= Html::a($member->first_name . " " . $member->last_name,
                                                    [
                                                        '/account/member-details/', 'id' => $member->id, 'type' => Yii::$app->request->get('search_request_experties')
                                                    ],
                                                    [

                                                    ]); ?></h5>
                                            <h6><?= substr($member->designation, 0, 40); ?></h6>
                                        </div>
                                        <h3 class="commitee_staff_email">
                                            <i class="fa fa-envelope"
                                               aria-hidden="true"></i> <?= Html::a($member->email, null, ['href' => 'mailto:' . $member->email]); ?>
                                        </h3>
                                    </div>
                                <?php } }
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>

    <style>
        .searchResult .view a {
            font-weight: bold;
        }
        #events,#news{
            background: #ffffff;
        }
        .searchResult .view {
            padding: 5px 5px 10px;
            margin-bottom: 10px;
            border-bottom: dashed 1px #ccc;
            -webkit-transition: background 0.2s;
            transition: background 0.2s;
        }
    </style>