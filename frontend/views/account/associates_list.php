<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <?= Html::a('<i class="fa fa-plus"></i> Add Associate', '/account/associate-member', ['class' => 'Mybtn pull-right', 'style' => "margin:10px"]); ?>
                <h3>Associates</h3>
                <br>


                <table class="table table-striped table-responsive">
                    <thead>

                    <tr>
                        <th>Title</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Mobile</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if ($associates <> null) {
                        foreach ($associates as $asscociate) {
                            ?>
                            <tr>
                                <td><?= $asscociate->assosiate_title; ?></td>
                                <td><?= $asscociate->assosiate_firstname; ?></td>
                                <td><?= $asscociate->assosiate_lastname; ?></td>
                                <td><?= $asscociate->assosiate_email; ?></td>
                                <td><?= $asscociate->assosiate_mobile; ?></td>
                                <td><?= $asscociate->created_on; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                    </tbody>
                </table>
                <br>
                <div class="col-md-12 text-center pull-right">
                    <?= LinkPager::widget([
                        'pagination' => $pages,
                    ]);
                    ?>
                </div>
            </div>

        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

