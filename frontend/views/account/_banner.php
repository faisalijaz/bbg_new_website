<?php

use yii\helpers\Html;

$this->title = ($model <> null) ? $model->first_name . " " . $model->last_name . " - Profile": "My Profile";
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .topnav1 a {
        padding: 14px 10px;
    }
</style>
<section class="bannerBG">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <img src="<?= (!empty($model->picture)) ? $model->picture : Yii::$app->params['no_image']; ?>" class="img-fluid PaddingTopBtm30px" alt="">
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 Tcenter">
                <div style="position: unset !important; padding-top: 25px;" class="BannerText">
                    <h1><?= ($model <> null) ? $model->first_name . " " . $model->last_name : ""; ?></h1>
                    <h2>
                        <?= ($model <> null) ? $model->designation: ""; ?> <br>
                        <?= ($model <> null) ? $model->address: ""; ?> <br>
                    </h2>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 Tcenter">
                <div class="editprofilebtn">
                    <?= Html::a('<i class="fa fa-user" aria-hidden="true"></i> Edit Profile',['/account/update'],[]) ?>
                </div>
            </div>
        </div>
        <div class="row whiteline text-right">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="topnav1" id="myTopnav">
                    <?php
                    $expiry =  new DateTime($model->expiry_date);
                    $date = new DateTime(date('Y-m-d'));

                    $difference = $expiry->diff($date);
                    $days = $difference->d;

                    if($days <= 7){
                       echo Html::a('Renew Membership', ['/account/renew-membership'], []);
                    }
                    ?>

                    <?php if ($model->group <> null && $model->group->add_associate) {
                        echo Html::a('Associates', ['/account/associates'], []);
                    } ?>
                    <?= Html::a('Company Members', ['/account/my-company-members'], []) ?>
                    <?= Html::a('Member Searches',['/account/expert-requests'],[]) ?>
                    <?php
                    if($model->group_id == 2) {
                        ?>
                        <?= Html::a('Upgrade Membership',['/account/membership-upgrade'],[]) ?>
                        <?php
                    }
                    ?>
                    <?/*= Html::a('Payment History',['/account/payments'],[]) */?>
                    <?= Html::a('Payment History',['/account/invoices'],[]) ?>
                    <?= Html::a('My Contacts',['/account/member-contacts'],[]) ?>
                    <?= Html::a('Member Offers',['/account/member-offers'],[]) ?>
                    <?= Html::a('Events Registered',['/account/events-registered'],[]) ?>
                </div>
            </div>
        </div>
    </div>
</section>