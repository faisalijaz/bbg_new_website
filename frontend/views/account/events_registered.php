<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

?>
<?= $this->render('_banner', [
    'model' => $model
]); ?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="Heading text-center">
                    <h3>Events Registered</h3>
                </div>
                <div class="form-group no-more-tables" style=" overflow-x:auto;">
                    <table id="memberEventsRegTable"
                           class="table display responsive no-wrap dt-responsive nowrap d-lg-table d-md-table d-sm-table d-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Event</th>
                            <th>Event Date</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if ($events <> null) {
                            $count = 1;
                            foreach ($events as $event) {
                                if ($event->eventData <> null) {
                                    $event_detials = $event->eventData;
                                }
                                ?>

                                <tr>
                                    <td class="text-center"><?= $count++; ?></td>
                                    <td class="text-center">
                                        <?= $event->firstname . ' ' . $event->lastname; ?>
                                        <br/><?= Html::a($event->email, null, ['href' => 'mailto:' . $event->email]); ?>
                                    </td>
                                    <td class="text-center"><?= (count($event_detials) > 0) ? $event_detials->title : ''; ?></td>
                                    <td class="text-center"><?= (count($event_detials) > 0) ? $event_detials->event_startDate : ''; ?></td>
                                    <td class="text-center">
                                        <span class="pull-right pricebox"><?= $event->fee_paid; ?></span>
                                    </td>
                                    <td class="text-center">
                                            <span class="Editbtn">
                                                <?= Html::a(
                                                    'Invoice',
                                                    ['/account/view-invoice', 'invoice' => $event->gen_invoice],
                                                    [
                                                        'target' => '_blank',
                                                        'Title' => 'View Invoice',
                                                        'data' => [
                                                            'method' => 'post'
                                                        ],
                                                    ]
                                                );
                                                ?>
                                            </span>
                                    </td>
                                </tr>

                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <br>
                    <div class="col-md-12 text-center pull-right">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

