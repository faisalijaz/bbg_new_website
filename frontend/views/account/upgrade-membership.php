<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="membershipcontent">
                    <div class="Heading">
                        <h3>Upgrade Membership</h3>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                <div class="type-bg">
                    <div class="TypeHeading">
                        <h2>Business</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                    </div>
                    <div class="Type">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Member Type</th>
                                <th>Membership Fee</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nominee</td>
                                <td>AED 2540</td>
                            </tr>
                            <tr>
                                <td>Alternate</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td>Additional Members</td>
                                <td>AED 895</td>
                            </tr>
                            <tr>
                                <td>Named Associates</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td colspan="2">Document Required</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol class="numbering">
                                        <li>Copy of UAE Trade Licence.</li>
                                        <li>Copy of your Lorem ipsum dolor sit amet,</li>
                                        <li>Passport Size picture (Upload as JPEG).</li>
                                    </ol>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="fullbtn"><a href="#">Apply Now</a></div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                <div class="type-bg">
                    <div class="TypeHeading">
                        <h2>individual</h2>
                        <h3>Lorem ipsum dolor sit amet, consectetur</h3>
                    </div>
                    <div class="Type">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Member Type</th>
                                <th>Membership Fee</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Nominee</td>
                                <td>AED 2540</td>
                            </tr>
                            <tr>
                                <td>Alternate</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td>Additional Members</td>
                                <td>AED 895</td>
                            </tr>
                            <tr>
                                <td>Named Associates</td>
                                <td>Free</td>
                            </tr>
                            <tr>
                                <td colspan="2">Document Required</td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <ol class="numbering">
                                        <li>Copy of UAE Trade Licence.</li>
                                        <li>Copy of your Lorem ipsum dolor sit amet,</li>
                                        <li>Passport Size picture (Upload as JPEG).</li>
                                    </ol>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                    <div class="fullbtn"><a href="#">Apply Now</a></div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



