<?php include ("header.php");?>
<?php include ("members-banner.php");?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 Heading">
                <h3>member’s profile</h3>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Personal Contact Information</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                            <div class="profile">
                                <h2>Abhilash Nair</h2>
                                <h3>Regional Manager - Removal Division at inchcape Shipping Services Dubai L.L.C</h3>
                                <p>
                                    <strong>Email</strong> - abhilashnair@domainname.com <br>
                                    <strong>Secondary Email</strong> - abcdefg@domainname.com <br>
                                    <strong>Contact Number</strong> - 00971501234567 <br>
                                    <strong>Address</strong> - Villa 24, Al Tawaash Street, Jumeirah 3, Dubai <br>
                                    <strong>Nationality</strong> - UK <br>
                                    <strong>Linkedin</strong> - http://linkedin.com/profilename <br>
                                    <strong>Twitter URL</strong> - http://twitter.com/profilename <br>
                                    <strong>Company Type</strong> - Construction <br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 RedHeading">
                    <h2>Company Information</h2>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 darkbg">
                    <div class="row PaddingTopBtm30px">
                        <div class="col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2">
                            <img src="resources/images/profilepic.jpg" class="img-fluid" alt="">
                        </div>
                        <div class="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
                            <div class="profile">
                                <h2>Inchcape Shipping Services L.L.C</h2>
                                <h3>Dubai United Arab Emirates</h3>
                                <p>
                                    <strong>Company Type</strong> - Logistics <br>
                                    <strong>URL </strong> - www.domainname.com <br>
                                    <strong>Contact Number</strong> - 00971501234567 <br>
                                    <strong>Address</strong> - Villa 24, Al Tawaash Street, Jumeirah 3, Dubai <br>
                                    <strong>P.O.Box</strong> - 33165 <br>
                                </p>
                                <h2>About Company</h2>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium beatae culpa error harum nostrum. Accusamus amet architecto, debitis distinctio eos fugiat in incidunt ipsa nesciunt quod reiciendis sed sint sunt! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at commodi corporis, doloribus explicabo fuga ipsum iusto laudantium natus odit officia quaerat. Eum hic impedit, iste quod repellendus tenetur voluptas? Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi delectus deserunt dolorum enim, ex excepturi impedit ipsum maxime minima minus nemo nostrum officiis omnis placeat quae quidem reprehenderit saepe tempore.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>

        </div>
    </div>
</section>
<?php include ("Clients-logos.php");?>
<?php include ("footer.php");?>



