<?php
Yii::$app->session->set('event_invoices', []);

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Event Registration';
$this->params['breadcrumbs'][] = $this->title;

$subTotal = 0;
$vat = 0;
$total = 0;
$invoices = [];

$members = \common\models\Members::find()->where(['status' => 1])->all();

?>
<style>
    .not-active {
        pointer-events: none;
        cursor: default;
    }

    ,
    .select2.select2-multiple {
        width: 95% !important;
    }

    span.select2.select2-container.select2-container--default.select2-container--below {
        width: 100% !important;
    }

</style>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row Eventform">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step" id="step-1-btn">
                        <a href="#step-2" type="button"
                           class="mybutn btn btn-default btn-circle not-active"
                           disabled="disabled">Step 1</a>
                    </div>

                    <div class="stepwizard-step">
                        <a href="#step-3" type="button"
                           class="mybutn btn btn-default btn-circle not-active"
                           disabled="disabled">Step 2</a>
                    </div>

                    <div class="stepwizard-step">
                        <a href="#step-1" type="button"
                           class="mybutn btn btn-primary btn-circle <?= (\Yii::$app->request->get('s') == 1) ? "" : " not-active" ?>">
                            Step 3</a>
                    </div>

                </div>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
            <div class="col-8 col-sm-8 col-md-8 col-lg-8 col-xl-8 EventformPadding">
                <div class="row setup-content" id="step-1">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="ThankyouMsg Eventform">
                                <h4>Thank you for your Registration</h4>
                                <?php

                               if(isset($response) && count($response) > 0){
                                   if ($response <> null && $response['response_code'] == "100") {
                                       ?>
                                       <div class="alert alert-success">
                                           <h3>Payment Success</h3>
                                           <p>Thank You your payment has been approve.</p>
                                       </div>
                                       <?php
                                   } else {
                                       ?>
                                       <div class="alert alert-danger">
                                           <h3>Oooopss!! Payment Error</h3>
                                           <p><?= $response['response_code']; ?></p>
                                       </div>
                                       <?php
                                   }
                               }
                                ?>

                                <div class="numberofguests text-center">Do you want to tell others BBG Members that your
                                    are Attending this event?
                                </div>

                                <div class="radiobox paddingtop20px text-center">
                                    <label>
                                        <input type="radio" class="memberInfoBtn" name="sendToMembers" value="1">
                                        <span class="numberofguests">Yes</span>
                                    </label>
                                    <label>
                                        <input type="radio" class="memberInfoBtn" name="sendToMembers" value="0"
                                               checked>
                                        <span class="numberofguests">No</span>
                                    </label>
                                </div>
                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 hidden"
                                     id="SelectBoxMemberInfo">
                                    <div class="col-md-12">
                                        <div class="numberofguests text-left">Select Members</div>
                                        <div class="input-group">
                                            <select class="js-example-basic-multiple" id="membersList"
                                                    name="membersList[]" multiple="multiple">
                                                <option value="">Select Members</option>
                                                <?php
                                                if ($members <> null) {
                                                    foreach ($members as $member) {
                                                        ?>
                                                        <option value="<?= $member->id; ?>"><?= $member->first_name . " " . $member->last_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>

                                            <span class="input-group-btn">
                                            <button id="sendInfoBtn" class="MySearchBtn btn btn-default" type="button">
                                                <!--<i class="fa fa-paper-plane" aria-hidden="true"></i>--> Send
                                            </button>
                                        </span>
                                        </div><!---->
                                        <br/>
                                        <input type="hidden" id="event_id_infoBtn"
                                               value="<?= \Yii::$app->request->get('id'); ?>">
                                    </div>
                                </div>
                                <div class="FinishBtn clear-fix text-center">
                                    <?= Html::a('Back',['/events/event-register/', 'id' => \Yii::$app->request->get('id')], ['class' => "Mybtn nextBtn"]); ?>
                                    <?= Html::a('Finish',['/'], ['class' => "Mybtn nextBtn"]); ?>

                                </div>

                            </div>
                            <h3>Share</h3>
                            <!-- AddToAny BEGIN -->
                            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                                <a class="a2a_button_facebook"></a>
                                <a class="a2a_button_twitter"></a>
                                <a class="a2a_button_google_plus"></a>
                            </div>
                            <script async src="https://static.addtoany.com/menu/page.js"></script>
                            <!-- AddToAny END -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2"></div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>

<?= $this->registerJs('
    $(".js-example-basic-multiple").select2({
        placeholder: "Select Members ...",
        allowClear: true,
       width: "resolve",  
    });
    $.fn.select2.defaults.set( "theme", "bootstrap" );
'); ?>