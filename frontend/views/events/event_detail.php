<?php


use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;

$this->title = 'Event Details';
$this->params['breadcrumbs'][] = $this->title;

?>

<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($event) && $event <> null) {
                            ?>
                            <div class="Heading">
                                <h3><?= $this->title; ?></h3>
                                <h4><?= $event->title; ?></h4>
                            </div>
                            <div class="EventDetail">
                                <div class="row">

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">

                                        <div id="multi-item-example1" class="carousel slide carousel-multi-item"
                                             data-ride="carousel">
                                            <div class="controls-top">
                                                <a class="btn-floating" href="#multi-item-example1" data-slide="prev"><i
                                                            class="fa fa-chevron-left"></i></a>
                                                <a class="btn-floating" href="#multi-item-example1" data-slide="next"><i
                                                            class="fa fa-chevron-right"></i></a>
                                            </div>

                                            <div class="carousel-inner" role="listbox">
                                             <!--   <!--First slide-->

                                                <!--/.First slide-->
                                                <!--Second slide-->
                                                <?php
                                                $i = 0;
                                                foreach ($event->eventImages as $row) {

                                                    if($i == 0){
                                                        ?>
                                                        <div class="carousel-item active">
                                                            <div class="col-md-12">
                                                                <div class="Slides text-center">
                                                                    <img class="img-fluid"
                                                                         src="<?= ($row->image) ? $row->image : \Yii::$app->params['no_image']; ?>"
                                                                         alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php

                                                    }    else{
                                                    ?>
                                                    <div class="carousel-item">
                                                        <div class="col-md-12">
                                                            <div class="Slides text-center">
                                                                <img class="img-fluid" src="<?= $row->image ?>" alt="">
                                                            </div>
                                                        </div>
                                                        <!--/.Second slide-->
                                                    </div>
                                                <?php }$i++;
                                                } ?>
                                                <!--/.Slides-->
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="EventDetailText">
                                            <p>
                                                <span class="BoldText">Event Date:</span> <?= \Yii::$app->formatter->asDate($event->event_startDate, 'php:D, d mm, Y'); ?>
                                                <br>
                                                <span class="BoldText">Event Time:</span> <?= \Yii::$app->formatter->asDate($event->event_startTime, 'php:H:i a'); ?>
                                                to <?= \Yii::$app->formatter->asDate($event->event_endTime, 'php:H:i a'); ?><br>
                                                <span class="BoldText">Venue:</span> <?= $event->venue; ?> <br>
                                                <span class="BoldText">Location:</span> <?= $event->address; ?><br>
                                            </p>
                                            <span class="ViewMap">
                                            <a href="#event_map" class="smooth_scroll">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> View Map
                                            </a>
                                        </span>
                                            <p>
                                                <br>
                                                <?php

                                                if (!$event->members_only) {
                                                    if ($event->specialEventsFee <> null) {
                                                        echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                            ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                                :</span> <?= ucwords($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                        }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span> <?= $event->member_fee; ?>
                                                        AED <br>
                                                        <span class="BoldText">Non Members Fee:</span> <?= $event->nonmember_fee; ?>
                                                        AED <br>
                                                        <?php
                                                    }
                                                } else if ($event->members_only && !Yii::$app->user->isGuest) {
                                                    if ($event->specialEventsFee <> null) {
                                                        echo '<span class="BoldText">Special Events</span><br/>';
                                                        foreach ($event->specialEventsFee as $spEvent) {
                                                            ?>
                                                            <span class="BoldText"><?= ucwords($spEvent->fee_type); ?>
                                                                :</span> <?= ucwords($spEvent->amount); ?>
                                                            AED <br>
                                                            <?php
                                                        }
                                                    } else { ?>
                                                        <span class="BoldText">Member Registrations:</span> <?= $event->member_fee; ?>
                                                        AED <br>
                                                        <span class="BoldText">Non Members Fee:</span> <?= $event->nonmember_fee; ?>
                                                        AED <br>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </p>

                                        </div>
                                        <?php
                                        if ($event->tba != 1 && ($event->group_size - $reg_num > 0) && $event->registration_start <= date('Y-m-d') && $event->registration_end >= date('Y-m-d')) {
                                            if (!$event->members_only) {
                                                ?>
                                                <span class="RegisterEventbtn">
                                                    <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', ['/events/event-register', 'id' => $event->id, 's' => 1], []); ?>
                                                </span>
                                                <?php
                                            } elseif($event->members_only && !Yii::$app->user->isGuest){
                                                ?>
                                                <span class="RegisterEventbtn">
                                                    <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', ['/events/event-register', 'id' => $event->id, 's' => 1], []); ?>
                                                </span>
                                                <?php
                                            }

                                            else {
                                                ?>
                                                <span class="RegisterEventbtn">
                                                    <?= Html::a('<i class="fa fa-map-marker" aria-hidden="true"></i> Register Now ', '#', ['class' => 'members_only event_reg_btn']); ?>
                                                </span>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <span class="label label-warning"><i class="fa fa-lock"></i> Registeration are closed.</span>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <h3>Description</h3>
                                            <h4><?= $event->short_description; ?></h4>
                                            <p><?= $event->description; ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div id="event_map" class="col-md-8">
                                <div style="width: 100%">
                                    <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="20"
                                            marginwidth="20"
                                            src="https://maps.google.com/maps?q=<?= urlencode($event->address) ?>&sensor=false&hl=en;z=14&amp;output=embed"></iframe>
                                </div>
                                <br/>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>

<?= $this->registerJs('
    
    var slider = $("#lightSlider").lightSlider();
    
     slider.item(1);  
    slider.goToSlide(3);
    slider.goToPrevSlide();
    slider.goToNextSlide();
    slider.getCurrentSlideCount();
    slider.refresh();
    slider.play(); 
    slider.pause();   
    
    
');?>

<?= $this->registerJs('
    
    $(".event_reg_btn").click(function(e){
        
        e.preventDefault();
        
        if($(this).hasClass("members_only")){
            swal({
                    title: "Members Event!",
                    text: "This Event is for members only. You need to be a member to register for this event!",
                    timer: 5000,
                    type: "info",
                    html: true,
                    showConfirmButton: false
                });
            );
        }
    });
    
'); ?>
<style>
    .controls-top a.btn-floating {
        background: none;
    }

    .controls-top a.btn-floating:hover {
        background: none;
    }
</style>