<?php

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <?php
            if ($isPast) {
                ?>
                <div class="col-md-8 pull-left"></div>
                <div class="col-md-4 pull-right">
                    <?= Html::dropDownList(
                        'past_event_month', '',
                        ['April', 'May', 'June', 'July'], [
                            'prompt' => 'Select ...',
                            'class' => 'form-control'
                        ]
                    );
                    ?>
                </div>
                <?php
            }
            ?>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title; ?></h3>
                    </div>
                    <?php
                    if (isset($events) && $events <> null && count($events) > 0) {
                        foreach ($events as $event) {
                            ?>
                            <div class="Event1">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <!--Second slide-->
                                        <?php
                                        $i = 0;
                                        foreach ($event->eventImages as $row) {
                                            if ($i == 0) {
                                                ?>
                                                <img src="<?= ($row->image) ?  $row->image  : Yii::$app->params['no_image']; ?>"
                                                     class="img-fluid" alt="">
                                            <?php }
                                            $i++;
                                        } ?>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="EventText">
                                            <h3><i class="fa fa-calendar"
                                                   aria-hidden="true"></i> <?= \Yii::$app->formatter->asDate($event->event_startDate, 'php:D, d mm, Y'); ?>
                                                <h5>
                                                    <i class="fa fa-map-marker"
                                                       aria-hidden="true"></i> <?= $event->address; ?>
                                                </h5>
                                            </h3>

                                            <h4><?= $event->title; ?></h4>
                                            <p><?= $event->short_description; ?></p>
                                            <span class="EventDetail">
                                                <?= Html::a('Event Detail', [
                                                    '/events/event-details',
                                                    'id' => $event->id], [

                                                ]); ?>

                                            </span>
                                            <span class="pull-right SocialLinksEvents">
                                                <a href="#">
                                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                                </a>
                                                <a href="#">
                                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="col-md-12 text-center">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>

            </div>
            <!--<div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <? /*= RightSidebarWidget::widget(); */ ?>
            </div>-->
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>