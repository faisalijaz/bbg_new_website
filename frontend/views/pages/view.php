<?php

/* @var $this yii\web\View */

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;


$this->title = $model->title;
$this->params['breadcrumbs'][] = $this->title;
?>

<?php if($model->banner_image <> null && $model->banner_status == 1){ ?>
    <section class="bbg_ccms_banner_img " style="background-image: url(<?= $model->banner_image; ?>);"></section>
<?php } ?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title;?></h3>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="SiteText">
                                <?= $model->description; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
