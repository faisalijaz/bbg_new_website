<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = $model->title;

$class = 'pages-text-transparent';

if (Yii::$app->request->get('seo_url')) {
    $seo = Yii::$app->request->get('seo_url');
    if ($seo == 'blogger' || $seo == 'gallery' || $seo == 'faq') {
        $class = '';
    }
}
?>
<div class="inner-banner style-6">
    <img class="center-image" src="<?= $model->banner_image ?>" alt="">
    <div class="vertical-align <?= $class; ?>">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">
                    <?= Breadcrumbs::widget([
                        'options' => ['class' => 'banner-breadcrumb color-white clearfix'],
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'itemTemplate' => '<li class=\'link-blue-2\'>{link}</li> / '
                    ]) ?>

                    <h2 class="color-white"><?= Html::encode($model->title) ?></h2>
                    <?php if (!empty($model->short_description)) { ?>
                        <p class="color-white"><?= Html::encode($model->short_description) ?> </p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>