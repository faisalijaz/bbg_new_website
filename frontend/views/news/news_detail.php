<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;
use yii\helpers\Html;


$this->title = 'News Details';
$this->params['breadcrumbs'][] = $this->title;

?>

<?/*= TopBannerWidget::widget(); */?>

    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                        <?php
                        if (isset($news) && $news <> null) {
                            ?>
                            <div class="Heading">
                                <h3><?= $this->title; ?></h3>
                                <h4><?= $news->title; ?></h4>
                            </div>
                            <div class="EventDetail">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-64 col-lg-6 col-xl-6">
                                        <img src="<?= $news->image; ?>" class="img-fluid" alt="<?= $news->title; ?>">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <div class="EventDetailText">
                                            <p>
                                                <span class="BoldText">Created Date:</span> <?= \Yii::$app->formatter->asDate($news->created_at, 'php:D, d mm, Y'); ?>
                                                <br>
                                                <span class="BoldText">Type:</span> <?= $news->news_type; ?> <br><br>
                                            </p>
                                            <span class="ViewMap">
                                                <a href="#event_map" class="smooth_scroll">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i> View Map
                                                </a>
                                            </span>

                                        </div>

                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="DescriptionHeading">
                                            <h3>Short Description</h3>
                                            <h4><?= $news->short_description; ?></h4>
                                        </div>
                                        <div class="DescriptionHeading">
                                            <h3>Description</h3>
                                            <?= $news->description; ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                    <?= RightSidebarWidget::widget(); ?>
                </div>
            </div>
        </div>
    </section>
<?= HomeBottomWidget::widget(); ?>
<?php
if (\Yii::$app->session->getFlash('error')) {

    $errors = Yii::$app->session->getFlash('error');

    $this->registerJs('
    swal({
            title: "Opps!",
            text: "' . $errors . '",
            timer: 5000,
            type: "error",
            html: true,
            showConfirmButton: false
        });
    ');
}
?>