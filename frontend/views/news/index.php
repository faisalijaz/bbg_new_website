<?php

use frontend\widgets\rightSidebar\RightSidebarWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;
use yii\widgets\LinkPager;
use frontend\widgets\homeBottom\HomeBottomWidget;

$this->title = 'UPCOMING News';
$this->params['breadcrumbs'][] = $this->title;

?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3><?= $this->title; ?></h3>
                    </div>
                    <?php
                    if (isset($news) && $news <> null && count($news) > 0) {
                        foreach ($news as $news) {
                            ?>
                            <div class="Event1">
                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <img src="<?= $news->image; ?>" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                                        <div class="EventText">
                                            <h3><i class="fa fa-calendar"
                                                   aria-hidden="true"></i> <?= \Yii::$app->formatter->asDate($news->created_at, 'php:d M Y'); ?>
                                            </h3>
                                            <h4><?= $news->title; ?></h4>
                                            <p><?= $news->short_description; ?></p>
                                            <span class="EventDetail">
                                                <?= Html::a('News Detail', ['/news/news-details', 'id' => $news->id], []); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                    <div class="col-md-12 text-center">
                        <?= LinkPager::widget([
                            'pagination' => $pages,
                        ]);
                        ?>
                    </div>
                </div>

            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <?= RightSidebarWidget::widget(); ?>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>