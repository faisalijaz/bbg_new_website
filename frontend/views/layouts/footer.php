<?php
if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
$year = date('Y');
?>
<footer>
    <section class="footerarea">
        <div class="container">
            <div class="row footerwidgets">
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Phone</h2>
                        <p>Telephone: <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?> <br>
                            Email: <?= (isset($siteSetting['admin_email'])) ? $siteSetting['admin_email'] : ''; ?> <br>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Address</h2>
                        <p><?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?></p>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4">
                    <div class="column1">
                        <h2>Subscribe For Newsletter</h2>
                        <p>Get the latest updates</p>
                        <div class="input-group">
                            <input type="email" name="email" class="form-control subscribeEmail"
                                   placeholder="Enter Your Email Address..">
                            <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" id="subscribeNewsletter" type="button">
                                <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                        <span class="socialmedia">
                            <a target="_blank" href="<?= (isset($siteSetting['facebook'])) ? $siteSetting['facebook'] : ''; ?> "><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                            <a target="_blank" href="<?= (isset($siteSetting['twitter'])) ? $siteSetting['twitter'] : ''; ?> "><i class="fa fa-twitter-square" aria-hidden="true"></i></a>
                            <a target="_blank" href="<?= (isset($siteSetting['linkedin'])) ? $siteSetting['linkedin'] : ''; ?> "><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="copyrights">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 CopyR">
                    <p>
                        CopyRights © <?= $year;?> - <?= $year+1;?>  <?= (isset($siteSetting['app_name'])) ? $siteSetting['app_name'] : ''; ?>. All Right Reserved. <br>
                        Designed & Developed By: 800Wisdom.ae
                    </p>
                </div>
            </div>
        </div>

    </section>
</footer>
<div id="fb-root"></div>
</body>

<?= $this->registerJs("

    $('body').find('#subscribeNewsletter').click(function (e) { 
     
        $(this).prop('disabled', true);
        var email_data = $(document).find('.subscribeEmail').val()
        
        if ( email_data == '') {
            
            swal({
                title: 'Error!',
                text: 'Please enter email address!',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#subscribeNewsletter').prop('disabled', false);
            return false; 
        }
        
        if(!isEmail(email_data)){
            swal({
                title: 'Error!',
                text: 'Please enter correct email address!',
                type: 'error',
                timer: 5000,
                confirmButtonText: 'OK'
            });
            $('#subscribeNewsletter').prop('disabled', false);
            return false; 
        }
        
        $.ajax({
        
            url: '/site/subscribe-newsletter',
            type: 'POST',
            data : {email : email_data},
            success: function (data) {
                
                var data = JSON.parse(data);
                console.log(data);
                
                if(data.type == 'error'){
                    swal({
                        title: 'Error!', 
                         text: data.msg, 
                        type: 'error',
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });
                } else{
                
                    swal({
                        title: 'Success!',
                        text: data.msg,
                        type: 'success', 
                        timer: 5000,
                        confirmButtonText: 'OK'
                    });
                
                }
                $('#subscribeNewsletter').prop('disabled', false)
            },
            error: function (e) {

                swal({
                    title: 'Error!',
                    text: 'Registration could not be completed!',
                    type: 'error',
                    timer: 5000,
                    confirmButtonText: 'OK'
                });

                $('#subscribeNewsletter').prop('disabled', false);
            }
        });

        $('#subscribeNewsletter').prop('disabled', false);
        e.stopImmediatePropagation();
        return false;
    });
    
    
    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }
    
");