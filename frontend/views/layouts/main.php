<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="apple-mobile-web-app-capable" content="yes"/>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no"/>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="style-2" data-color="theme-1">

<?php $this->beginBody() ?>

<!-- Theme loading -->
<?= $this->render('theme_loading', []); ?>

<!--Applicaiton Header-->
<?= $this->render('header', []); ?>

<?= \yii2mod\alert\Alert::widget() ?>

<?= $content ?>

<!--Applicaiton Footer-->
<?= $this->render('footer', []); ?>


<?php
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => true, 'tabindex' => false],
    'options' => ['tabindex' => false]
]);
echo "<div id='modalContent'><div style='text-align:center'></div>";
yii\bootstrap\Modal::end();
?>

<?php $this->endBody() ?>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


