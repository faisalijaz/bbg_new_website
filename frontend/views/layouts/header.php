<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$url = Url::current();

$userData = \Yii::$app->user;
$logo = Yii::$app->appSettings->getByAttribute("app_logo");

$menu_list = Yii::$app->appSettings->getMenuItems();

?>
<header style="height: 174px;">
    <div id="fixedTopNav">
        <div class="TopMost">
            <div class="container">
                <div class="row">
                    <?php
                    if ($userData->isGuest) {
                        ?>
                        <div class="col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3">
                            <div class="MembershipBtn PaddingTopBtm">
                                <a href="/site/apply-membership">apply for membership</a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                        <div class="MemberLogin PaddingTopBtm">
                            <?php
                            if (!$userData->isGuest) {
                                ?>
                                <a href="/account/profile">
                                    <i class="fa fa-user-o usericonsize"
                                       aria-hidden="true"></i> Welcome, <?= $userData->identity->first_name; ?>
                                </a>
                                <?php
                            } else {
                                ?>
                                <a href="#" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-user usericonsize" aria-hidden="true"></i> Member Login
                                </a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                    if (!$userData->isGuest) {
                        ?>
                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                            <div class="MemberLogin PaddingTopBtm">
                                <?= Html::a('<i class="fa  fa-sign-out usericonsize" aria-hidden="true"></i> Logout',
                                    ['/site/logout'], ['data' => ['method' => 'post']]); ?>
                            </div>
                        </div>

                        <div class="col-12 col-sm-6 col-md-4 col-lg-2 col-xl-2">
                            <div class="MemberLogin PaddingTopBtm">
                                <a id="request_experties" class="request_experties" href="#" data-toggle="modal"
                                   data-target="#myModalRequestExpertise">
                                    <i class="fa fa-search usericonsize" aria-hidden="true"></i> Reqeuset Experties
                                </a>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="hidden-md-down col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3"></div>

                    <div class="hidden-md-down col-12 col-sm-6 col-md-3 col-lg-3 col-xl-3">
                        <?php $form = ActiveForm::begin(['id' => 'searcher', 'method' => 'get', 'action' => '/', 'options' => ['class' => '']]); ?>
                        <div class="TopMostSearch">
                            <div class="input-group">
                                <input type="search" name="search" class="form-control"
                                       placeholder="I’m looking for....">
                                <span class="input-group-btn">
                                <button class="MySearchBtn btn btn-default" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>
                                </button>
                            </span>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="SiteHeader">
            <div class="container">
                <div class="row">
                    <div class="col-3 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                        <?= Html::a('<img src="' . $logo . '" alt="BBG-Dubai">', ['/site/index'], []) ?>
                    </div>
                    <div class="hidden-md-down col-lg-9 col-xl-9">
                        <nav class="float-left">
                            <ul id='menu'>

                                <?php
                                foreach ($menu_list as $key => $value){ ?>
                                <li>
                                    <?= Html::a($key, [$value], ['class' => Yii::$app->appSettings->getSubMenuItems($key) ? 'prett' : '']) ?>
                                    <?php
                                    $submenu = Yii::$app->appSettings->getSubMenuItems($key);
                                    echo '<ul class="menus">';
                                    foreach ($submenu

                                    as $key1 => $value1){ ?>
                            <li class="<?= Yii::$app->appSettings->getSubMenuItems($key1) ? 'has-submenu' : '' ?>">
                            <?= Html::a(strtoupper(str_replace('-', ' ', $key1)), [$value1], ['class' => Yii::$app->appSettings->getSubMenuItems($key1) ? 'prett' : 'home']) ?>
                            <?= Yii::$app->appSettings->getSubMenuItems($key1) ? '' : '</li>' ?>

                            <?php
                            $submenu_child = Yii::$app->appSettings->getSubMenuItems($key1);
                            if (isset($submenu_child)){
                            if ($submenu_child <> null && !empty($submenu_child)) {
                            echo '<ul class="submenu">';
                            foreach ($submenu_child

                                     as $key2 => $value2) { ?>
                                <li class=""><?= Html::a(strtoupper(str_replace('-', ' ', $key2)), [$value2], ['class' => 'home']) ?></li>
                            <?php
                            }
                            echo '</ul>';
                            Yii::$app->appSettings->getSubMenuItems($key1) ? '</li>' : '';
                            }
                            }

                            }
                            ?>
                                </li>

                            </ul>

                            <?php } ?>

                        </nav>
                    </div>
                    <div class="col-8 col-sm-8 col-md-8 hidden-lg-up">
                        <div id="mySidenav" class="sidenav">
                            <?= \yii\helpers\Html::a('About BBG<span class=""></span>', ['/pages/view-by-seo-url', 'seo_url' => 'about-us'], ['class' => 'active']); ?>
                            <?= Html::a('Events', ['/events'], []) ?>
                            <?php
                            if (!$userData->isGuest) {
                                ?>
                                <?= Html::a('Member Directory', ['#'], []) ?>
                                <?= Html::a('Company Directory', ['/account/company-directory'], []) ?>
                                <?php
                            }
                            ?>
                            <?= Html::a('Member Directory', ['/account/member-directory'], []) ?>
                            <?= Html::a('News', ['/news'], []) ?>
                            <?= Html::a('Contact', ['site/contact'], []) ?>
                        </div>
                        <div class="text-right">
                            <span style="font-size:30px;cursor:pointer;" onclick="openNav()">&#9776;</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Members Login</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(['id' => 'model_login_form', 'action' => '/site/ajax-login', 'options' => ['class' => 'f-login-form']]); ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">

                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <input type="email" required id="accountloginform_user" class="form-control"
                                           name="AccountLoginForm[username]" autofocus=""
                                           placeholder="Please Enter Username"
                                           aria-required="true">
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <div class="form-group">
                                    <input type="password" required id="accountloginform_password" class="form-control"
                                           name="AccountLoginForm[password]" placeholder="Enter your password"
                                           aria-required="true">
                                </div>
                                <div id="ajax_login_errors" class="alert hidden row"></div>
                                <button type="submit" id="submitModelLogin"
                                        class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o"
                                        name="LOGIN TO YOUR ACCOUNT">Sign In
                                </button>
                                <a href="/site/request-password-reset">
                                    <button class="Mybtn " type="button">Forgot Password</button>
                                </a>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>

        <!-- The Modal for request expertise-->
        <div class="modal fade" id="myModalRequestExpertise" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Reqeuset Experties</h4>
                        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin(['id' => 'model_request_experties', 'method' => 'get', 'action' => '/account/search-experties', 'options' => ['class' => 'c_model_request_experties']]); ?>
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                <div class="form-group">
                                    <input type="text" required id="search_request_experties" class="form-control"
                                           name="search_request_experties" autofocus="" placeholder="Please Enter value"
                                           aria-required="true">
                                    <p class="help-block help-block-error"></p>
                                </div>
                                <button type="submit" id="submitModelSearchExperties"
                                        class="Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o">Search Experties
                                </button>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
</header>
<div style="clear: both"></div>