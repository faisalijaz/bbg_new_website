<?php

use yii\helpers\Html;

$items = Yii::$app->session->has('cartItems') ? Yii::$app->session->get('cartItems') : [];

if (!$items) {
    $items = [];
}

?>


<div class="cart-popup">
    <div class="cart_close"></div>
    <h3 class="cart-title color-dark-2">Cart</h3>

        <?php if (isset($items) && count($items) > 0 && !empty($items)) { ?>
            <div class="max-height-300">
                <?php foreach ($items as $key => $item) { ?>
            <div id="<?= $key ?>" class="hotel-small style-2 clearfix">
                <a class="hotel-img black-hover" href="#">
                    <img style="width: 120px; height: 99px;" class="img-responsiv"
                         src="<?= $item['image']; ?>" alt="">
                    <div class="tour-layer delay-1"></div>
                    <div class="hotel-loc tt text-center"><?= $item['checkin']; ?></div>
                </a>
                <div class="hotel-desc cus_hotel_desc">
                    <h5 class="color-dark-2"><strong>AED <?= $item['total_price'] ?></strong> </h5>
                    <h4 class="color-dark-2"><?=  substr($item['title'], 0, 71); ?></h4>
                    <?php echo \yii\helpers\Html::a('x', 'javascript:void(0)', [
                        'class' => 'close pull-right delete-item',
                        'id' => $key,
                        'style' => 'padding: 0px 10px'
                    ]) ?>

                    <?php echo \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['/tour/view-slug/', 'slug' => $item['seo_url'], 'sessionId' => $key], [
                        'class' => 'close pull-right edit-item',
                        'id' => $key,
                        'style' => 'padding: 0px 10px'
                    ]) ?>


                    <?php foreach ($item['addons'] as $addon) { ?>
                        <img width="20" src="<?= $addon['image']; ?>"
                             alt="<?= $addon['title']; ?>">
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
            </div>

    <div class="cart-buttons" style="border: none;">

        <?php if(Yii::$app->user->isGuest) { ?>
            <?= Html::a('Checkout', ['/booking/guest-checkout'], ['class' => 'c-button b-40 full bg-blue-2 hv-bg-blue-2-o']) ?>
        <?php } else { ?>
            <?= Html::a('Checkout', ['/booking/customer-checkout'], ['class' => 'c-button b-40 full bg-blue-2 hv-bg-blue-2-o']) ?>
        <?php } ?>

    </div>

    <?php } else { ?>

        <p>Your Cart is empty!</p>

        <a href="/tours" class="c-button b-40 full bg-blue-2 hv-bg-blue-2-o"><span>Explore Tours</span></a>
    <?php } ?>
</div>

