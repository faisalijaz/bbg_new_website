<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Search Results';
$this->params['breadcrumbs'][] = $this->title;
?>

<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8">
                    <a href="/"><img src="https://d1l2trc10ppjt4.cloudfront.net/logo/bbg_new_logo5a685a0071f2d.png" alt="BBG-Dubai"></a>
                </div>
                <div class="col-3 col-sm-3 col-md-4 col-lg-4 col-xl-4">
                    <h1><b><?= Yii::$app->params['invoice_title']; ?></b></h1>
                    <span>Invoice date: 01 january 2020</span><br/>
                    <span>Invoice number: 002540</span>
                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8">
                    <br/><strong>Al,Seef Road, Bur Dubai</strong><br/>
                    <strong>Dubai</strong><br/>
                    <strong>P.O BOX 9933</strong><br/>
                    <strong>Telehephone: +97 2654 5158</strong><br/>
                    <strong>Email: bbgaccount@gmail.com</strong><br/>
                    <strong>VAT Registration no: _______________</strong><br/>
                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-8 col-xl-8">
                    <br/><strong>Bill To: XXXX</strong><br/>
                    <strong>Name: XXXX</strong><br/>
                    <strong>Company: XXXX</strong><br/>
                    <strong>Address: XXXX</strong><br/>
                    <strong>P.O Box: XXXX</strong><br/>
                    <strong>Telephone: XXXX</strong><br/>
                    <strong>VAT Registration no: _______________</strong><br/><br/>
                </div>

                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                <table class="table ">
                    <thead>
                    <tr>
                        <th class="col-md-8">Item</th>
                        <th class="col-md-4">Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Description</td>
                        <td>2,4585</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>2,4585</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>2,4585</td>
                    </tr>
                    <tr>
                        <td><strong>Subtotal</strong></td>
                        <td><strong>2054,4585</strong></td>
                    </tr>
                    <tr>
                        <td><strong>VAT(5%)</strong></td>
                        <td><strong>585</strong></td>
                    </tr>
                    <tr>
                        <td><strong>Total</strong></td>
                        <td><strong>585852</strong></td>
                    </tr>
                    </tbody>
                </table>




                </div>


                <div class="col-3 col-sm-3 col-md-3 col-lg-12 col-xl-12">
                    <br/><strong>Below are the payment methods accepted by BBG</strong><br/><br/>
                    <strong>Cheque: XXXX</strong><br/>
                    <strong>Cheque: XXXX</strong><br/>
                    <strong>Cheque: XXXX</strong><br/>
                    <strong>Cheque: XXXX</strong><br/>
                    <strong>Cheque: XXXX</strong><br/>
                </div>




                </div>


        </div>
    </div>
</section>


<style>
    .searchResult .view a {
        font-weight: bold;
    }

    #events, #news {
        background: #ffffff;
    }

    .searchResult .view {
        padding: 5px 5px 10px;
        margin-bottom: 10px;
        border-bottom: dashed 1px #ccc;
        -webkit-transition: background 0.2s;
        transition: background 0.2s;
    }
</style>