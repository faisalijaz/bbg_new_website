<?php

use frontend\widgets\committeMembers\CommitteeMembersWidget;
use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\homeSlider\HomeSliderWidget;
use frontend\widgets\latestNews\LatestNewsWidget;
use frontend\widgets\upcomingEvents\UpcomingEventsWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Welcome to BBG';
if (count((array)\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
?>

<!-- TOP BANNER -->
<?= HomeSliderWidget::widget(); ?>

<section class="MainArea chairmanmessage">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
                <div class="CMessage">
                    <h2><?= ($chairmna_meassage <> null) ? $chairmna_meassage->title : ''; ?></h2>
                    <p><?= ($chairmna_meassage <> null) ? $chairmna_meassage->short_description : ''; ?></p>
                    <?= Html::a('<button class="Mybtn pull-right" type="button" > More Detail</button>', ['/pages/view-by-seo-url', 'seo_url' => 'chairman-message'], []); ?>

                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <img src="/theme/bbg/resources/images/chairman.jpg" class="img-fluid" alt="">
            </div>
        </div>
    </div>
</section>

    <!-- ############# Calling Events Carousel ####### -->
<?= UpcomingEventsWidget::widget(); ?>

    <!-- ############# Calling Events Carousel ####### -->
<?= CommitteeMembersWidget::widget(); ?>

    <!-- ############# Calling Events Carousel ####### -->
<?= LatestNewsWidget::widget(); ?>

<section class="socialfeeds">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6" style="padding-top: 25px">
                <div class="fbplugin">
                    <h2>Facebook</h2>
                </div>

                <div class="fb-page" height=475 width=500 data-href="<?= (isset($siteSetting['facebook'])) ? $siteSetting['facebook'] : ''; ?>" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/BBGOnline/" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/BBGOnline/">British Business Group, Dubai &amp; Northern Emirates</a>
                    </blockquote>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 ccol-xl-6 social_feeds">
                <div class="fbplugin">
                    <h2>Twitter</h2>
                </div>
                <div class="scrollable">
                    <a class="twitter-timeline" href="https://twitter.com/BBGOnline">Tweets by BBG Dubai</a>
                </div>
            </div>
        </div>
        <br/><br/><br/>
        <div class="row">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 social_feeds">
                <div class="fbplugin">
                    <h2>Instagram</h2>
                </div>
                <div class="scrollable">
                    <blockquote class="instagram-media" data-instgrm-captioned data-instgrm-permalink="https://www.instagram.com/p/BfsV8KXhPk0/" data-instgrm-version="8" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50.0% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURczMzPf399fX1+bm5mzY9AMAAADiSURBVDjLvZXbEsMgCES5/P8/t9FuRVCRmU73JWlzosgSIIZURCjo/ad+EQJJB4Hv8BFt+IDpQoCx1wjOSBFhh2XssxEIYn3ulI/6MNReE07UIWJEv8UEOWDS88LY97kqyTliJKKtuYBbruAyVh5wOHiXmpi5we58Ek028czwyuQdLKPG1Bkb4NnM+VeAnfHqn1k4+GPT6uGQcvu2h2OVuIf/gWUFyy8OWEpdyZSa3aVCqpVoVvzZZ2VTnn2wU8qzVjDDetO90GSy9mVLqtgYSy231MxrY6I2gGqjrTY0L8fxCxfCBbhWrsYYAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/BfsV8KXhPk0/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">It&#39;s that time of the year again. Join our Annual Golf Day &amp; Dinner on Thursday 8 March at Trump International Golf Club. Sign up now for this or any of our other events at bbgdubai.org</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">A post shared by <a href="https://www.instagram.com/bbgonline/" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px;" target="_blank"> British Business Group</a> (@bbgonline) on <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2018-02-27T08:07:46+00:00">Feb 27, 2018 at 12:07am PST</time></p></div></blockquote> <script async defer src="//www.instagram.com/embed.js"></script>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 social_feeds">
                <div class="fbplugin">
                    <h2>Linked In</h2>
                </div>
                <div class="scrollable">
                    <script type="IN/CompanyProfile" data-id="1043402" data-format="inline" data-related="false"></script>
                    <script type="IN/CompanyInsider" data-id="1043402" data-format="inline" data-width="400"></script>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>