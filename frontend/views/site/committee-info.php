<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \frontend\models\ContactForm */

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\topBanner\TopBannerWidget;
use yii\helpers\Html;

$this->title = 'Committee Info';
$this->params['breadcrumbs'][] = $this->title;

if (count(\Yii::$app->appSettings->getSettings()) > 0) {
    $siteSetting = \Yii::$app->appSettings->getSettings();
}
?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-8 col-xl-8 LeftArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="Heading">
                        <h3>Committee info</h3>
                    </div>
                    <hr/>
                    <div class="committee text-center">
                        <div class="row">
                            <?php
                            if (isset($committee) && $committee <> null) {
                                foreach ($committee as $member) {
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 text-center">
                                        <img src="<?= $member->image; ?>" class="committee_staff_image img-fluid"
                                             alt="<?= $member->full_name; ?>">
                                        <div class="name_and_postion">
                                            <h5><?= $member->full_name; ?></h5>
                                            <h6><?= $member->designation; ?></h6>
                                        </div>
                                        <h3 class="commitee_staff_email"><i class="fa fa-envelope"
                                                                            aria-hidden="true"></i> <?= Html::a($member->email, null, ['href' => 'mailto:' . $member->email]); ?>
                                        </h3>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="Heading">
                        <h3>Office Staff</h3>
                    </div>
                    <hr/>
                    <div class="committee text-center">
                        <div class="row">
                            <?php
                            if (isset($staff) && $staff <> null) {
                                foreach ($staff as $data) {
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                                        <img src="<?= $data->image; ?>" class="committee_staff_image img-fluid"
                                             alt="<?= $data->full_name; ?>">
                                        <div class="name_and_postion">
                                            <h5><?= $data->full_name; ?></h5>
                                            <h6><?= $data->designation; ?></h6>
                                        </div>
                                        <h3 class="commitee_staff_email"><i class="fa fa-envelope"
                                                                            aria-hidden="true"></i> <?= Html::a($data->email, null, ['href' => 'mailto:' . $data->email]); ?>
                                        </h3>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 RightArea">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 paddingRightLeft">
                    <div class="SideHeading">
                        <h3>Get In Touch</h3>
                    </div>
                    <div class="SideCalender">
                        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 PaddingTopBtm">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                    <i class="fa fa-envelope blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <strong>British Business Group </strong><br>
                                        P.O. Box 9333, Dubai, <br>
                                        United Arab Emirates
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                    <i class="fa fa-map-marker blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        <strong>BBG Executive Office, </strong><br>
                                        <?= (isset($siteSetting['address'])) ? $siteSetting['address'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-3 col-lg-3 col-xl-3 contentcenter">
                                    <i class="fa fa-phone blueicon" aria-hidden="true"></i>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9 col-lg-9 col-xl-9 contactinfo-side contentcenter">
                                    <p>
                                        Telephone: <?= (isset($siteSetting['telephone'])) ? $siteSetting['telephone'] : ''; ?>
                                        <br>
                                        Fax: <?= (isset($siteSetting['fax'])) ? $siteSetting['fax'] : ''; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>
