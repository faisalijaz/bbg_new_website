<?php

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<!--Ajax form submit -->
<script src="<?= Yii::getAlias('@web') . '/themes/js/ajax.submit.js' ?>"></script>

<?php

Pjax::begin(['id' => 'login-form']);

$form = ActiveForm::begin([
    'id' => 'form-login',
    'options' => ['class' => 'simple-form'],
    'method' => 'post',
    'enableAjaxValidation' => true,
    'action' => ['/site/ajax-login']
]);

?>


<div class="row">

    <div class="col-lg-12">
        <div class="form-block type-2 clearfix">
            <div class="form-label color-dark-2">First Name</div>
            <div class="input-style-1 b-50 brd-0 type-2 color-3">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Please Enter Email'])->label(false) ?>
            </div>
        </div>
    </div>


</div>

 <div class="row">


    <div class="col-lg-12">
        <div class="form-block type-2 clearfix">
            <div class="form-label color-dark-2">Password</div>
            <div class="input-style-1 b-50 brd-0 type-2 color-3">
                <?= $form->field($model, 'password')->passwordInput(['autofocus' => true, 'placeholder' => 'Your password'])->label(false) ?>
            </div>
        </div>
    </div>


</div>


<div class="modal-footer">
    <button type="button" class="btn btn-primary btn-custom waves-effect waves-light" data-dismiss="modal">Close</button>
    <?= Html::submitButton('Login', ['id' => 'ajaxSubmitBtn', 'class' => 'btn btn-info btn-custom waves-effect waves-light']) ?>
</div>


<?php ActiveForm::end(); ?>

<?php Pjax::end(); ?>
