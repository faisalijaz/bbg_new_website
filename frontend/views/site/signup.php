<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \common\models\LoginForm */

use frontend\widgets\topBanner\TopBannerWidget;
use kartik\checkbox\CheckboxX;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\helpers\Html;

$this->title = 'Membership Application';
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        form div.required label.control-label:after {
            content: " * ";
            color: #a94442;
        }
    </style>
<?php $form = ActiveForm::begin(['id' => 'login-form', 'options' => ['enctype' => 'multipart/form-data', 'class' => 'f-login-form']]); ?>
<?/*= TopBannerWidget::widget(); */?>
    <section class="MainArea">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 LeftArea" style="padding-bottom: 50px;">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12  paddingRightLeft">
                        <div class="col-md-12 ">
                            <div class="Heading text-left">
                                <h3>Personal Information</h3><br/>
                            </div>
                            <div class="row">

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'title')->dropDownList(Yii::$app->params['userTitle'], ['prompt' => 'Select ...'])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'first_name')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'last_name')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'email')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'secondry_email')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'country_code')->dropDownList(
                                        \yii\helpers\ArrayHelper::map(\common\models\Country::find()->all(), 'id', 'country_name'),
                                        ['prompt' => 'Select ...'])->label('Country') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'city')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'address')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'phone_number')->textInput([])->label('Mobile') ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'designation')->textInput([])->label() ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'nationality')->textInput([])->label() ?>
                                </div>
                                <?php
                                if ($model->group_id == 2) {
                                    ?>
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'company_type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'),
                                            ["class" => "form-control"])->label('Category') ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                            if ($model->group_id <> 2) {
                                ?>
                                <div class="Heading text-left">
                                    <h3>Company Info</h3><br/>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'compnay_name')->textInput([])->label() ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'company_type')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Categories::find()->all(), 'id', 'title'),
                                            ["class" => "form-control"])->label('Category') ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                                <?= $form->field($model, 'compnay_url')->textInput([])->label() ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'company_phone')->textInput([])->label("Telephone") ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'fax')->textInput([])->label() ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'postal_zip')->textInput([])->label("P.O. Box/Zip Code/Post Code") ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                        <?= $form->field($model, 'emirates')->textInput([])->label() ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <?= $form->field($model, 'comapny_address')->textInput([])->label("Address") ?>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                        <?= $form->field($model, 'about_company')->textarea([
                                            "row" => 5,
                                            "class" => "form-control",
                                            'style' => 'height:100px'
                                        ])->label("About Company (Be specific and use keywords; this function is part of the member search function)") ?>

                                    </div>
                                </div>
                                <?php
                            } ?>
                            <div class="Heading text-left">
                                <h3>Upload Required Documents</h3><br/>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'trade_licence')->fileInput(['style' => 'background:#fff;'])->label("Copy of UAE Trade Licence.") ?>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'passport_copy')->fileInput(['style' => 'background:#fff;'])->label("Copy of passport") ?>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'residence_visa')->fileInput(['style' => 'background:#fff;'])->label("Copy of residence visa") ?>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'passport_size_pic')->fileInput(['style' => 'background:#fff;'])->label("Passport size photograph (upload as JPEG)") ?>
                                </div>
                            </div>

                            <div class="Heading text-left">
                                <h3>BBG Profile Info</h3><br/>
                            </div>
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'user_name')->textInput([])->label() ?>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'password')->passwordInput([])->label("Password") ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
                                    <?= $form->field($model, 'repassword')->passwordInput([])->label("Retype password") ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                                        'template' => '<div class="row"><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">{input}</div><div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">{image}</div></div>',
                                    ]) ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'newsletter')->widget(CheckboxX::classname(), [
                                        'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                        'pluginOptions' => [
                                            'theme' => 'krajee-flatblue',
                                            'enclosedLabel' => true,
                                            'threeState' => false,
                                        ]
                                    ])->label(false); ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'sync_events_calander')->widget(CheckboxX::classname(), [
                                        'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                        'pluginOptions' => [
                                            'theme' => 'krajee-flatblue',
                                            'enclosedLabel' => true,
                                            'threeState' => false,
                                        ]
                                    ])->label(false); ?>
                                </div>

                                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                                    <?= $form->field($model, 'terms')->widget(CheckboxX::classname(), [
                                        'initInputType' => CheckboxX::INPUT_CHECKBOX,
                                        'pluginOptions' => [
                                            'theme' => 'krajee-flatblue',
                                            'enclosedLabel' => true,
                                            'threeState' => false,
                                        ]
                                    ])->label(false); ?>
                                    <span>
                                        <a href="#" style="font-size: 10px;font-weight: 800;font-style: italic;">Read Here</a>
                                    </span>
                                </div>

                            </div>
                        </div>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12 text-right">
                            <?= Html::submitButton('Sign Up', ['class' => 'Mybtn full b-60 bg-dr-blue-2 hv-dr-blue-2-o', 'name' => 'LOGIN TO YOUR ACCOUNT']) ?>
                        </div>
                    </div>
                </div>
            </div>

    </section>
<?php ActiveForm::end(); ?>

<?php
if (count($model->getErrors()) > 0) {

    foreach ($model->getErrors() as $error) {
        if (is_array($error)) {
            foreach ($error as $err) {
                echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $err . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
            }
        } else {
            echo $this->registerJs('
                swal({
                        title: "Opps!",
                        text: "' . $error . '",
                        timer: 5000,
                        type: "error",
                        html: true,
                        showConfirmButton: true
                    });
                ');
        }

    }
}
?>