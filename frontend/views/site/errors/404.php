<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$this->title = '404 | Page not found';
?>

<div class="not-found">
    <img class="center-image" src="/theme/img/special/bg-2.jpg" alt="">
    <div class="not-found-box">
        <div class="not-found-title">ouch!</div>
        <div class="not-found-message">sorry the page you are looking for does not exist</div>
        <a href="/" class="c-button b-60 bg-white hv-white-o"><span>view more</span></a>
    </div>
</div>

<div class="clearfix"></div>