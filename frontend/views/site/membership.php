<?php

/* @var $this yii\web\View */

use frontend\widgets\homeBottom\HomeBottomWidget;
use frontend\widgets\topBanner\TopBannerWidget;

$this->title = 'My Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<?/*= TopBannerWidget::widget(); */?>
<section class="MainArea">
    <div class="container LeftArea">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <?= ($model <> null) ? $model->description : ""; ?>
            </div>

            <?php
            if ($membership_types <> null) {
                foreach ($membership_types as $type) {
                    ?>
                    <div class="col-12 col-sm-12 col-md-12 col-lg-4 col-xl-4 PaddingTopBtm30px">
                        <div class="type-bg">
                            <div class="TypeHeading">
                                <h2><?= $type->title; ?></h2>
                                <h3>(One-time Joining Fee AED <?= ($type <> null) ? $type->joining_fee : ""; ?>)</h3>
                            </div>
                            <div class="Type" style="min-height: 360px !important;">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Member Type</th>
                                        <th>Membership Fee</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if ($type->add_nominee) {
                                        ?>
                                        <tr>
                                            <td>Nominee</td>
                                            <td><?= ($type->fee_nominee > 0) ? "AED  " . $type->fee_nominee : "Free"; ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php
                                    if ($type->add_alternate) {
                                        ?>
                                        <tr>
                                            <td>Alternate</td>
                                            <td><?= ($type->fee_alternate > 0) ? "AED  " . $type->fee_alternate : "Free"; ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php
                                    if ($type->add_additional) {
                                        ?>
                                        <tr>
                                            <td>Additional Members</td>
                                            <td><?= ($type->fee_additional > 0) ? "AED  " . $type->fee_additional : "Free"; ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <?php
                                    if ($type->add_associate) {
                                        ?>
                                        <tr>
                                            <td>Named Associates</td>
                                            <td><?= ($type->fee_associate > 0) ? "AED  " . $type->fee_associate : "Free"; ?></td>
                                        </tr>
                                        <?php
                                    } ?>
                                    <tr>
                                        <td colspan="2">Document Required</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <ol class="numbering">
                                                <?= $type->description; ?>
                                            </ol>
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="fullbtn"><a href="/site/signup/?membership=<?= $type->id;?>">Apply Now</a></div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</section>
<?= HomeBottomWidget::widget(); ?>


