<?php

namespace app\models;

use common\models\Members;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AccountSearch represents the model behind the search form about `common\models\Accounts`.
 */
class MemberSearch extends Members
{

    public $alphaSearch;
    public $searchExpert;
    public $find_exp_membership = 0;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'occasional_updates', 'created_at', 'updated_at'], 'integer'],
            [['email', 'searchExpert', 'alphaSearch', 'account_type', 'first_name', 'last_name', 'gender', 'country', 'status', 'findExpiredLimit','find_exp_membership'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Members::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $role = \Yii::$app->authManager->getRolesByUser(\Yii::$app->user->getId());

        if (!isset($role['admin'])) {
            // $this->parent_id = \Yii::$app->user->getId();
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'occasional_updates' => $this->occasional_updates,
            'parent_id' => $this->parent_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'account_type' => $this->account_type,
            'group_id' => $this->group_id,
        ]);

        $query->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'account_type', $this->account_type])
            ->andFilterWhere(['like', 'parent_id', $this->parent_id]);

        if($this->find_exp_membership){
            $date = date('Y-m-d', strtotime('+1 month'));
            $query->andFilterWhere(['between', 'date', date('Y-m-d'), $date]);
        }

        return $dataProvider;
    }

    public function searchExpert($params)
    {

        $query = Members::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->searchExpert) {

            $query->andFilterWhere(['like', 'first_name', $this->searchExpert])
                ->andFilterWhere(['like', 'last_name', $this->searchExpert])
                ->andFilterWhere(['like', 'designation', $this->searchExpert])
                ->andFilterWhere(['like', 'password_reset_token', $this->searchExpert]);

            /* $query->join('LEFT JOIN', 'tour_booking_details bk_detail', 'tour_booking_pickup.pickup_booking_details = bk_detail.id');
             $query->join('LEFT JOIN', 'tour_price_mapping tp', 'bk_detail.tour_id = tp.tour_id');
             $query->andWhere(['tp.experience_id' => $this->searchExpert]);*/

        }

        return $dataProvider;
    }
}
