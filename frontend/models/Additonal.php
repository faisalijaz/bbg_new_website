<?php
namespace frontend\models;

use common\helpers\EmailHelper;
use common\models\Members;
use yii\base\Model;


/**
 * Signup form
 */
class Additonal extends Model
{
    public $email;
    public $password;
    public $group_id;
    public $account_type;
    public $repassword;
    public $user_name;
    public $first_name;
    public $last_name;
    public $phone;
    public $title;
    public $designation;
    public $secondry_email;
    public $country_code;
    public $city;
    public $address;
    public $nationality;
    public $inforamtion_source;
    public $companyInfo = true;
    public $companyData = [];
    public $documents;
    public $phone_number;
    public $verifyCode;
    public $terms;
    public $newsletter = 0;
    public $sync_events_calander;
    public $user_docs;


    public $trade_licence;
    public $passport_copy;
    public $residence_visa;
    public $passport_size_pic;
    public $user_industry;

    public $upgrade;



    /**
     * @inheritdoc
     * @return string
     */
    public function rules()
    {
        return [

            [['title', 'designation', 'first_name', 'last_name'], 'required'],

            [['title', 'designation', 'first_name', 'last_name',], 'string', 'max' => 255],

            ['user_name', 'filter', 'filter' => 'trim'],
            ['user_name', 'required'],
            ['user_name', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This username has already been taken.'],
            ['user_name', 'string', 'min' => 4, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\Members', 'message' => 'This email address has already been taken.'],

            [['documents', 'user_industry', 'trade_licence', 'passport_copy', 'residence_visa', 'passport_size_pic'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg', 'mimeTypes' => 'image/jpeg, image/png'],

        ];
    }

    /**
     * @inheritdoc
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'terms' => 'I agree to the BBG Terms and Conditions .',
            'newsletter' => 'I want to subscribe to the BBG Newsletter.',
            'sync_events_calander' => 'I want to synchronise BBG events with my calendar. (Available only for MS Outlook.)',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        $transaction = \Yii::$app->db->beginTransaction();

        try {

            if (!$this->validate()) {
                return null;
            }

            $user = new Members();

            $user->user_docs = $this->documents;
            $user->group_id = $this->group_id;
            $user->account_type = $this->account_type;

            if (isset($this->documents['passport_size_pic'])) {
                $user->picture = $this->documents['passport_size_pic'];
            }

            $user->parent_id = \Yii::$app->user->id;
            $user->title = $this->title;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->email = $this->email;
            $user->country = $this->country_code;
            $user->city = $this->city;
            $user->address = $this->address;
            $user->phone_number = $this->phone_number;
            $user->designation = $this->designation;
            $user->nationality = $this->nationality;
            $user->user_name = $this->user_name;
            $user->invoiced = 0;
            $user->registeration_date = date('Y-m-d');
            $user->expiry_date = date('Y-m-d', strtotime('+1 year'));
            $user->status = 0;

            $user->setPassword($this->password);
            $user->setVerificationCode();
            $user->generateAuthKey();

            if (!$user->save()) {
                return false;
            }

            $transaction->commit();

            // Send email to customer
            $this->sendEmail($user);

            return true;

        } catch (\Exception $exception) {
            $transaction->rollback();
            return false;
        }
    }


    /**
     * Send an email to user
     * */
    public function sendEmail($user) {

        (new EmailHelper())->sendEmail($user->email, [], 'Application Received', 'account/signup', ['user' => $user]);
    }
}
