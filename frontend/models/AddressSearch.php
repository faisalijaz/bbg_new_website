<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Address;

/**
 * AddonSearch represents the model behind the search form about `app\models\Addons`.
 */
class AddressSearch extends Address
{
    /**
     * @inheritdoc
     * @return array
     */
    public function rules()
    {
        return [
            [['address', 'area', 'city'], 'required'],
            [['account_id'], 'integer'],
            [['address', 'street', 'area', 'city', 'country'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params param
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
                 'pageSize' => 6,
             ],
        ]);

        $this->load($params);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'account_id' => $this->account_id,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'area', $this->area])
            ->andFilterWhere(['like', 'city', $this->city]);

        return $dataProvider;
    }
}
