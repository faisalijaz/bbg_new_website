<?php

namespace frontend\models;

use yii;
use yii\base\Model;
use common\models\Accounts;
use yii\db\Exception;

/**
 * Update
 */
class ChangePassword extends Model
{
    public $currentPassword;
    public $password;
    public $repassword;

    /**
     * @inheritdoc
     * @return string
     */
    public function rules()
    {
        return [

            ['currentPassword', 'required'],
            ['currentPassword', 'string'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['repassword', 'required'],
            ['repassword', 'string', 'min' => 6],
            ['repassword', 'compare', 'compareAttribute' => 'password', 'message' => "Passwords don't match"],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'currentPassword' => 'Old Password',
            'password' => 'New Password',
            'repassword' => 'Repeat New Password',
        ];
    }

    /**
     * @param array $attribute attributes
     * @param array $params    params
     * @return mixed
     */
    public function findPasswords($attribute, $params)
    {
        $user = Accounts::find()->where([
            'email' => Yii::$app->user->identity->email
        ])->one();
        $password = $user->password_hash;
        if (!Yii::$app->security->validatePassword($this->currentPassword, $password)) {
            $this->addError($attribute, 'Old password is incorrect');
        }
    }

    /**
     * Change Password fucntion
     * @return User|null the saved model or null if saving fails
     */
    public function changeUserPassword()
    {
        $modeluser = Accounts::find()->where([
            'email' => Yii::$app->user->identity->email
        ])->one();

        try {
            $modeluser->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            if ($modeluser->save()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            Yii::$app->getSession()->setFlash(
                'error',
                "{$e->getMessage()}"
            );
            return false;
        }
    }
}
