<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        // 'theme/bbg/resources/css/evetsform.css',
        'theme/bbg/resources/css/mystyles.css',
        'theme/bbg/resources/css/reset.css',
        'theme/bbg/resources/css/bootstrap.min.css',
        'theme/bbg/resources/font-awesome/css/font-awesome.min.css',
        'theme/bbg/resources/calender/style.css',
        'http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css',
        'theme/bbg/resources/css/swiper.min.css',
        'theme/bbg/resources/DataTables/datatables.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css',

    ];
    public $js = [
        'theme/bbg/resources/js/jquery-1.12.4.js',
        'theme/bbg/resources/js/bootstrap.js',
        'theme/bbg/resources/calender/tempust.js',
        'theme/bbg/resources/js/jquery.flexisel.js',
        'theme/bbg/resources/js/swal.js',
        'theme/bbg/resources/js/waiting.js',
        'theme/bbg/resources/DataTables/datatables.min.js',
        'theme/bbg/resources/js/header_form_submit.js',
        'theme/bbg/resources/js/footer.js',
        'theme/bbg/resources/js/events_registeration.js',
        '//platform.linkedin.com/in.js',
        'https://platform.twitter.com/widgets.js',
        'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        // 'Zelenin\yii\SemanticUI\assets\SemanticUICSSAsset'
    ];
}
