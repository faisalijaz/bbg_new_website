<?php

namespace frontend\controllers;

use app\models\TourSearch;
use common\models\Banners;
use common\models\BlockBookingsDates;
use common\models\CartModel;
use common\models\Destinations;
use common\models\Reviews;
use common\models\TourPriceMapping;
use common\models\Tours;
use Yii;
use yii\web\NotFoundHttpException;

class TourController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     * @return mixed
     *
     */
    public function actionIndex($destination = '', $slug = '', $category = '')
    {
        $searchModel = new TourSearch();

        if ($category) {
            $searchModel->category_id = $category;
        }
        if ($destination) {
            $searchModel->tour_destination = $destination;
        }
        if ($slug) {
            $searchModel->tour_destination = $slug;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param      $slug
     * @param null $sessionId
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewSlug($slug, $sessionId = null)
    {
        // Find item by seo URL
        $model = $this->findModel('seo_url', $slug);

        // Reviews model to add a new review for tour.
        $review = new Reviews(['tour_id' => $model->id, 'status' => 'In-Active']);
        if (!Yii::$app->user->isGuest) {
            $review->account_id = Yii::$app->user->id;
        }

        $autoFillAttributes = [];

        // Check if user requested to update cart item
        if ($sessionId !== null) {
            $cartItem = (new CartModel())->getItemByKey($sessionId);
            $autoFillAttributes = $cartItem;
            $autoFillAttributes['cartId'] = $sessionId;
        } else {
            $autoFillAttributes = ['tour' => $model->id, 'package_id' => 1];
        }

        // Create a cart model and set some default values like tour & package.
        $cartItemModel = new CartModel();
        $cartItemModel->load($autoFillAttributes, '');

        return $this->render('view', [
            'model' => $model,
            'cartItemModel' => $cartItemModel,
            'reviewModel' => $review
        ]);
    }

    /**
     * @return string
     */
    public function actionDestinations($category)
    {
        $model = Destinations::find()->where(['category' => $category])->all();
        return $this->render('_destinations', [
            'model' => $model
        ]);
    }


    /**
     * @param string $slug tour seo_url to find an item
     * @return string
     */
    public function actionAddOns($slug)
    {
        // Make sure that this function can only be run on post request.
        if (Yii::$app->request->isGet) {
            return $this->redirect(['view-slug', 'slug' => $slug]);
        }

        // If user is not guest then find is group id and find price mapping.
        $group_id = 1;
        if (!Yii::$app->user->isGuest) {
            $group_id = Yii::$app->user->getIdentity()->group_id;
        }

        $cartItem = new CartModel();
        $cartItem->load(Yii::$app->request->post());
        $cartItem->group_id = $group_id;

        // Check if cart id is greater then ze
        if ($cartItem->cartId !== null && !empty($cartItem->cartId)) {
            $sessionItem = (new CartModel())->getItemByKey($cartItem->cartId);
            $cartItem->addons = $sessionItem['addons'];
            $cartItem->pickup = $sessionItem['pickup'];
        }

        // Find item by Seo Url
        $model = $this->findModel('seo_url', $slug);
        $experiences = TourPriceMapping::find()->where([
            'tour_id' => $model->id,
            'group_id' => $group_id])->all();

        return $this->render('select-addons', [
            'model' => $model,
            'cartItem' => $cartItem,
            'experiences' => $experiences,
        ]);
    }

    /**
     * Creates a new Reviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateReview()
    {
        $model = new Reviews();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->SendAdminAlert($model);
            Yii::$app->session->setFlash('success', 'Thank you very much for giving us review.');
            return $this->goBack((!empty(Yii::$app->request->referrer) ? Yii::$app->request->referrer : null));
        } else {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }


    /**
     * Creates a new Reviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reviews();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Reviews model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionFindAvailableDates()
    {
        $availableBookingDates = [];
        $blockedDates = [];

        if (Yii::$app->request->post()) {
            $data = Yii::$app->request->post();
            $model = new Tours();
            if ($data['id']) {
                $bookingDates = $model->tourBookingDates($data['id']);
                if (isset($bookingDates) && $bookingDates <> null) {
                    $BlockedDays = BlockBookingsDates::find()->where(['tour_id' => $data['id']])->all();

                    if ($BlockedDays <> null) {
                        foreach ($BlockedDays as $dates) {
                            $blockedDates[] = \Yii::$app->formatter->asDate($dates->date, 'php:m-d-Y');;
                        }
                    }

                    foreach ($bookingDates as $dates) {
                        $days = Yii::$app->dateTime->createDaysInDateRange($dates->start_date, $dates->end_date, 'm-d-Y');
                        foreach ($days as $day) {
                            if (!in_array($day, $availableBookingDates) && !in_array($day, $blockedDates) &&
                                $day >= Yii::$app->formatter->asDate(Yii::$app->dateTime->getDate(), 'php:m-d-Y')
                            ) {
                                $availableBookingDates[] = $day;
                            }
                        }
                    }
                }
            }
        }
        print_r(\GuzzleHttp\json_encode($availableBookingDates));
        die();
    }

    /**
     * @return array
     */
    public function actionBooking()
    {
        $banner = Banners::find()->where(['slug' => 'checkout-page-banner'])->one();
        return $this->render('booking', [
            'banner' => $banner
        ]);
    }

    /**
     * Finds the Tours model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $attribute column name
     * @param integer $value column name value
     * @return Tours the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($attribute, $value)
    {
        $model = Tours::find()->where([$attribute => $value])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Finds the Tours model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $attribute column name
     * @param integer $value column name value
     * @return Tours the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModelOne($value)
    {
        $model = Tours::find()->where(['id' => $value])->one();
        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
