<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\helpers\PaytabsHelper;
use common\helpers\Utf8Helper;
use common\models\AccountLoginForm;
use common\models\CmsPages;
use common\models\CommitteMembers;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\Groups;
use common\models\Invoices;
use common\models\MediaManager;
use common\models\News;
use common\models\NewsletterSubscriptions;
use common\models\Payments;
use common\models\VerifyAccount;
use frontend\models\ContactForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use Yii;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UnauthorizedHttpException;
use yii\web\UploadedFile;
use common\models\Members;
use kartik\mpdf\Pdf;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return mixed
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $search = Yii::$app->request->get('search');

        if(isset($search)){

            $query_events = "SELECT id,title,short_description FROM `events` where `title` LIKE '%$search%' ";
            $search_events = Events::findBySql($query_events)->all();

            $query_news = "SELECT id,title,short_description FROM `news` where `title` LIKE '%$search%' ";
            $search_news = News::findBySql($query_news)->all();

            return $this->render('search', [
                'search_events' => $search_events,
                'search_news' => $search_news,
            ]);

        }else{

            $chairmna_meassage = CmsPages::findOne(['seo_url' => 'chairman-message']);

            return $this->render('index', [
                'events',
                'chairmna_meassage' => $chairmna_meassage,
            ]);
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new AccountLoginForm();
        $guest = false;

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            if (\Yii::$app->session->get('backToURL') <> null) {

                $url = \Yii::$app->session->get('backToURL');
                \Yii::$app->session->set("backToURL", null);

                return $this->goBack($url);
            }

            if (\Yii::$app->session->get('backToEventRegisterURL') <> null) {

                $url = \Yii::$app->session->get('backToEventRegisterURL');
                \Yii::$app->session->set("backToEventRegisterURL", null);

                return $this->goBack($url);
            }
            echo 1;
            return $this->redirect(['/account/profile']);

        } else {

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs in a user.
     * @return mixed
     */
    public function actionAjaxLogin()
    {
        $model = new AccountLoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->login()) {

            $url = Url::to('/account/profile', true);
            // return $this->redirect($url);

            echo Json::encode((['msg' => "success", 'url' => $url]));

        } else {

            $login_error = "";
            $errors = $model->getErrors();

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    if (is_array($error) && count($error) > 0) {
                        foreach ($error as $err) {
                            $login_error .= "<span class='alert alert-danger'>" . $err . "</span>";
                        }
                    } else {
                        $login_error .= "<span class='alert alert-danger'>" . $error . "</span>";
                    }
                }
            }

            echo Json::encode(['msg' => "error", 'text' => $login_error]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        $banner = CmsPages::find()->where(['seo_url'=>'contact-us'])->one();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'banner' => $banner,
            ]);
        }
    }

    /**
     * @return string
     */
    public function actionCommitteeInfo()
    {
        $committee = CommitteMembers::find()->where(['type' => 'committe_member','status' => '1'])->orderBy('sort_order', 'ASC')->all();
        $staff = CommitteMembers::find()->where(['type' => 'office_staff','status' => '1'])->orderBy('sort_order', 'ASC')->all();
        return $this->render('committee-info', [
            'committee' => $committee,
            'staff' => $staff
        ]);

    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        $model = $this->findModel('seo_url', 'faq');
        return $this->render('about', [
            'model' => $model
        ]);
    }


    public function actionApplyMembership()
    {

        $membership_types = Groups::find()->all();
        $model = CmsPages::findOne(['seo_url' => 'bbg-membership']);
        return $this->render('membership', [
            'model' => $model,
            'membership_types' => $membership_types
        ]);
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup($membership = 0)
    {
        $model = new SignupForm();
        $errors = [];

        if (Yii::$app->request->get()) {
            $model->group_id = Yii::$app->request->get('membership');
            $model->account_type = 'member';
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $licence = UploadedFile::getInstance($model, 'trade_licence');
            $visa = UploadedFile::getInstance($model, 'residence_visa');
            $passport = UploadedFile::getInstance($model, 'passport_copy');
            $pic = UploadedFile::getInstance($model, 'passport_size_pic');

            if ($licence <> null) {
                $trade_licence = $this->actionUpload($licence);
                $model->documents['trade_licence'] = $trade_licence['url'];
            }

            if ($visa<> null) {
                $residence_visa = $this->actionUpload($visa);
                $model->documents['residence_visa'] = $residence_visa['url'];
            }

            if ($passport<> null) {
                $passport_copy = $this->actionUpload($passport);
                $model->documents['passport_copy'] = $passport_copy['url'];
            }

            if ($pic<> null) {
                $passport_size_pic = $this->actionUpload($pic);
                $model->documents['passport_size_pic'] = $passport_size_pic['url'];
            }


            if ($user = $model->signup()) {
                \Yii::$app->session->setFlash('success', 'Your application for membership to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else{
            $errors[] = $model->getErrors();
        }


        return $this->render('signup', [
            'model' => $model,
            'errors' => $errors
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify your account.
     *
     * @param string $token user Auth Token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionVerify($token) {

        try {
            $model = new VerifyAccount($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->verify()) {
            Yii::$app->session->setFlash('success', 'Your Account is verified now.');
            $this->redirect('/login');
        } else {
            throw new UnauthorizedHttpException('Verification code has been expired or invalid.');
        }
    }

    /**
     * @param $file
     * @return array
     */
    public function actionUpload($file)
    {
        $json = [];

        // Make sure we have the correct directory

        $currentDir = MediaManager::findOne(193);
        $parent_id = $currentDir->id;
        $path = $currentDir->path . '/';

        if (!$json) {

            if (!empty($file->name)) {
                // Sanitize the filename
                $filename = basename(html_entity_decode($file->name, ENT_QUOTES, 'UTF-8'));
                $extension = $file->extension;
                $name = $file->baseName;

                // Validate the filename length
                if ((Utf8Helper::utf8_strlen($filename) < 3) || (Utf8Helper::utf8_strlen($filename) > 255)) {
                    $json['error'] = Yii::$app->params['error_filename'];
                }

                // Allowed file extension types
                $allowed = [
                    'jpg',
                    'jpeg',
                    'gif',
                    'png'
                ];

                if (!in_array(Utf8Helper::utf8_strtolower(Utf8Helper::utf8_substr(strrchr($filename, '.'), 1)), $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Allowed file mime types
                $allowed = [
                    'image/jpeg',
                    'image/pjpeg',
                    'image/png',
                    'image/x-png',
                    'image/gif'
                ];

                if (!in_array($file->type, $allowed)) {
                    $json['error'] = Yii::$app->params['error_filetype'];
                }

                // Return any upload error
                if ($file->error != UPLOAD_ERR_OK) {
                    $json['error'] = Yii::$app->params['error_upload' . $file->error];
                }
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }
        }

        if (!$json) {

            $uniqueName = trim((str_replace(' ', '', $name) . uniqid()));
            $randomName = $string = preg_replace('/\s+/', '', $uniqueName) . '.' . $extension;
            $uploadObject = Yii::$app->get('s3bucket')->upload($path . $randomName, $file->tempName);

            if ($uploadObject) {

                // check if CDN host is available then upload and get cdn URL.

                if (Yii::$app->get('s3bucket')->cdnHostname) {
                    $url = Yii::$app->get('s3bucket')->getCdnUrl($path . $randomName);
                } else {
                    $url = Yii::$app->get('s3bucket')->getUrl();
                }

                // Save Data into Database
                $data = ['name' => $file->name, 'parent_id' => $parent_id, 'type' => 'file', 'href' => $url, 'path' => $path . $randomName];
                $this->saveObject($data);
                $json['success'] = Yii::$app->params['text_uploaded'];
                $json['url'] = $url;
            } else {
                $json['error'] = Yii::$app->params['error_upload'];
            }
        }

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $json;
    }

    /**
     * @param $data
     * @return bool
     */
    public function saveObject($data)
    {
        $folderObj = new MediaManager();
        $folderObj->attributes = $data;
        $folderObj->created_at = Yii::$app->dateTime->getTime();

        return $folderObj->save();
    }

    /**
     * @return string
     */
    public function actionInvoice(){
        return $this->render('invoice');
    }

    /**
     * @return string
     */
    public function actionSubscribeNewsletter()
    {
        if (Yii::$app->request->post()) {

            $model = new NewsletterSubscriptions();
            $model->email = Yii::$app->request->post("email");
            $text = "";

            if (!$model->save()) {

                foreach ($model->getErrors() as $error) {

                    if (is_array($error)) {
                        foreach ($error as $err) {
                            $text .= $err . "<br/>";
                        }
                    } else {
                        $text .= $error . "<br/>";
                    }
                }

                return \GuzzleHttp\json_encode(['type' => 'error', 'msg' => $error]);
            }

            return \GuzzleHttp\json_encode(['type' => 'success', 'msg' => "You have subscribed successfully!"]);
        }
    }

    /**
     * @param $invoice_id
     */
    public function actionInvoicePayment($invoice_id)
    {
        if (isset($invoice_id) && !empty($invoice_id)) {

            $errors = [];
            $invoice = Invoices::findOne($invoice_id);
            $result = "";

            if ($invoice <> null) {

                // If invoice is already paid
                if ($invoice->payment_status == 'paid') {

                    \Yii::$app->session->setFlash('error', "This invoice is already paid");
                    $this->redirect('/site/invoice-payment');

                } /*else {

                    // Check if pay page is already created
                    if (!empty($invoice->paytabs_p_id) && !empty($invoice->paytabs_payment_url)) {
                        $this->redirect($invoice->paytabs_payment_url);
                    }

                    // Initialize pay-tabs payments
                    $paytabs = new PaytabsHelper();

                    // Create Pay page
                    $result = $paytabs->create_pay_page($invoice, $name);

                    if ($result <> null) {

                        if ($result->response_code == "4012") {

                            $invoice->paytabs_p_id = $result->p_id;
                            $invoice->paytabs_payment_url = $result->payment_url;
                            $invoice->save();

                        } else {

                            if (count($errors) > 0) {
                                Yii::$app->session->setFlash('error', $result->result);
                                $this->redirect('/events/payment?id=$id');
                            }

                        }
                    }
                }*/
            }

            return $this->render('payment', [
                'invoice' => $invoice,
            ]);
        }
    }

    /**
     *
     */
    public function actionVerifyPayment()
    {
        if (\Yii::$app->request->post()) {

            $msg = '';
            $response = \Yii::$app->request->post();
            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = null;

            if ($response['response_code'] == "100") {
                $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);
            }

            $payment = new Payments();
            $payment->paytabs_make_payment($response);

            return $this->render('payment_result', [
                'response' => $response,
                'invoice' => $invoice
            ]);
        }

        $this->goHome();
    }

    /**
     * @param $invoice
     */
    public function actionPaymentNotifications()
    {
        if (Yii::$app->request->post()) {

            $ipn = Yii::$app->request->post();

            $payment = Payments::findOne(['response_code' => $ipn->invoice_id]);

            if ($payment == null) {
                $payment = new Payments();
            }

            $payment->invoice_id = $ipn->reference_no;
            $payment->transaction_id = $ipn->transaction_id;
            $payment->response_code = $ipn->response_code;
            $payment->currency = $ipn->currency;
            $payment->amount = $ipn->amount;
            $payment->detail = $ipn->result;
            $payment->status = 'done';
            $payment->payment_date = date('Y-m-d');
            $payment->user_id = Yii::$app->user->id;
            $payment->save();

            $invoice = Invoices::findOne(['invoice_id' => $ipn->reference_no]);
            if ($invoice <> null) {

                $invoice->payment_status = 'paid';
                $invoice->save();

                if($invoice->invoice_related_to == "event"){
                    $subscriptions = EventSubscriptions::find()->where(['gen_invoice' => $invoice->invoice_id])->all();
                    foreach ($subscriptions as $subscription){
                        $subscription->payment_status == 'paid';
                        $subscription->save();
                    }
                }
            }

            // Send Receipt to Email

            (new EmailHelper())->sendNewsLetterEmail('se.faisalijaz@gmail.com', [], 'Payment Notification', Yii::$app->request->post());
        }
    }

    public function actionViewInvoice($invoice){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member = Members::findOne(Yii::$app->user->id);
        $invoices = Invoices::findOne($invoice);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;

        if ($invoices->invoice_related_to == "event") {

            $content = $this->renderPartial('/account/send_event_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);

        } else {

            $content = $this->renderPartial('/account/send_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);
        }

        $mpdf->WriteHtml($content);
        return $mpdf->Output('Invoice', 'I');
    }

}
