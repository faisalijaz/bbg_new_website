<?php

namespace frontend\controllers;

use app\models\AccountSearch;
use common\models\AccountCompany;
use common\models\Accounts;
use common\models\Categories;
use common\models\Country;
use common\models\EventSubscriptions;
use common\models\Groups;
use common\models\InterestedCategories;
use common\models\Invoices;
use common\models\MemberCompanies;
use common\models\MemberContacts;
use common\models\MemberOffers;
use common\models\Members;
use common\models\NamedAssosiate;
use common\models\Payments;
use common\models\SearchExpertise;
use frontend\models\Additonal;
use frontend\models\ChangePassword;
use frontend\models\UpgradeForm;
use kartik\mpdf\Pdf;
use Yii;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AccountController implements the CRUD actions for Accounts model.
 */
class AccountController extends Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    /**
     * @inheritgitdoc
     * @return mixed
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'view-invoice' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Accounts models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AccountSearch();

        if (Yii::$app->request->get('account_type')) {
            $searchModel->account_type = Yii::$app->request->get('account_type');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     *  Search the Experties
     * Search from the members / Members Company
     *
     */
    public function actionSearchExperties(){

        $search = Yii::$app->request->get('search_request_experties');

        if(isset($search)){

            $query_members = "SELECT * FROM `accounts` WHERE Concat(first_name, '',last_name, '', designation, '', email , '', address) like \"%$search%\"";
            $search_members = Members::findBySql($query_members)->all();

            $query_member_companies = "SELECT * FROM `account_company` WHERE Concat(name, '', name , '', about_company) like \"%$search%\"";
            $search_ccompanies_members = AccountCompany::findBySql($query_member_companies)->all();

            $query_categories = "SELECT * FROM `categories` WHERE Concat(title, '', short_description) like \"%$search%\"";
            $search_categories = Categories::findBySql($query_categories)->all();

            return $this->render('search_experties', [
                'members' => $search_members,
                'companies_members' =>  $search_ccompanies_members,
                'categories' =>  $search_categories,
            ]);

        }else{
            return $this->redirect(['/']);
        }
    }

    /**
     * Displays a single Accounts model.
     * @param Integer $id id of user
     * @return mixed
     */
    public function actionProfile()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $categories = Categories::find()->all();
        $categories_selected = ArrayHelper::map(
            InterestedCategories::find()
                ->where(['member_id' => Yii::$app->user->id])->all(), 'category_id', 'id');

        return $this->render('myprofile', [
            'model' => $this->findModel(Yii::$app->user->id),
            'interested_categories' => $categories,
            'categories_selected' => $categories_selected
        ]);
    }


    /**
     * Creates a new Accounts model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Members();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Accounts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $id = Yii::$app->user->id;
        $model = $this->findModel($id);
        $countries = ArrayHelper::map(Country::find()->all(), 'id', 'country_name');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['profile']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'country' => $countries
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionChangePassword()
    {
        $model = new ChangePassword();

        $id = Yii::$app->user->id;
        $profile = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->changeUserPassword()) {
                Yii::$app->getSession()->setFlash(
                    'error',
                    'Password not changed'
                );
            } else {
                Yii::$app->getSession()->setFlash(
                    'success',
                    'Password changed'
                );
            }
            $this->redirect(['/account/change-password/', 'id' => Yii::$app->user->id]);
        } else {
            return $this->render('change_password', [
                'model' => $model,
                'profile' => $profile,
            ]);
        }
    }

    /**
     * Deletes an existing Accounts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id , to delete the data against specific ID
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Accounts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id to find something against the primary key
     * @return Accounts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Members::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id id
     * @return static
     * @throws NotFoundHttpException
     */
    public function actionPaymentHistory()
    {
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);

        return $this->render('_payment_history', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),

        ]);

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberContacts(){
        return $this->render('member-contact', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * @param string $search
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionMemberDirectory($search = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }
        $member_directory_search = Yii::$app->request->get('md');
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);

        if ($search){
            $query = Members::find()->where(['status' => '1'])
                ->andWhere(['!=', 'id', Yii::$app->user->id])
                ->andWhere(['like', 'accounts.first_name', $search . '%', false])
                ->orWhere(['like', 'accounts.last_name', strtolower($search . '%'), false]);
        } else {
            $query = Members::find()->where(['status' => '1'])
                ->andWhere(['!=', 'id', Yii::$app->user->id]);
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $members = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('first_name', 'ASC')
            ->all();

        return $this->render('members-directory', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'members' => $members,
            'pages' => $pages,
        ]);

    }

    /**
     * @param string $company
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCompanyMembers($company = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);
        $companyInfo = AccountCompany::findOne($company);
        $showComapnyInfo = true;

        if ($company) {

            $query = Members::find()->where(['status' => '1', 'company' => $company]);

            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 20
            ]);

            $members = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy('first_name', 'ASC')
                ->all();

        }

        return $this->render('company-members', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'members' => $members,
            'pages' => $pages,
            'companyInfo' => $companyInfo,
            'showComapnyInfo' => $showComapnyInfo
        ]);
    }

    /**
     * @param string $search
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCompanyDirectory($search = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);

        if ($search) {
            $query = AccountCompany::find()->where(['like', 'account_company.name', $search . '%', false])
                ->orWhere(['like', 'account_company.name', strtolower($search . '%'), false]);
        } else {
            $query = AccountCompany::find();
        }

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $companies = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('name', 'ASC')
            ->all();

        return $this->render('company-directory', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'companies' => $companies,
            'pages' => $pages,
        ]);

    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberOffers(){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member_offers_data = MemberOffers::find()->all();
        return $this->render('members-offer', [
            'member_offers_data' => $member_offers_data,
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionMemberOffersDetail($id){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member_offer_detail = MemberOffers::findOne($id);
        return $this->render('member_offer_detail', [
            'member_offer_detail' => $member_offer_detail
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionPayments(){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = Payments::find()->where([
            'user_id' => Yii::$app->user->id,
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $payments = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('id', 'DESC')
            ->all();

        return $this->render('payment-history', [
            'model' => $this->findModel(Yii::$app->user->id),
            'payments' => $payments,
            'pages' => $pages
        ]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMembershipUpgrade(){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->user->identity->group_id <> 2) {
            $this->redirect('/account/profile');
        }

        $membership_types = Groups::findOne(1);

        return $this->render('upgrade', [
            'model' => $this->findModel(Yii::$app->user->id),
            'type' => $membership_types
        ]);

    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionUpgrade()
    {

        $model = new UpgradeForm();
        $member = Members::findOne(Yii::$app->user->id);

        $model->first_name = $member->first_name;
        $model->last_name = $member->last_name;
        $model->email = $member->email;
        $model->secondry_email = $member->secondry_email;
        $model->country_code = $member->country;
        $model->city = $member->city;
        $model->address = $member->address;
        $model->phone_number = $member->phone_number;
        $model->designation = $member->designation;
        $model->nationality = $member->nationality;
        $model->title = $member->title;
        $model->upgrade = true;

        $errors = [];

        if (Yii::$app->request->get()) {
            $model->group_id = Yii::$app->request->get('membership');
            $model->account_type = 'member';
        }

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->user->identity->group_id <> 2) {
            // $this->redirect('/account/profile');
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $licence = UploadedFile::getInstance($model, 'trade_licence');
            $visa = UploadedFile::getInstance($model, 'residence_visa');
            $passport = UploadedFile::getInstance($model, 'passport_copy');
            $pic = UploadedFile::getInstance($model, 'passport_size_pic');

            if ($licence <> null) {
                $trade_licence = $this->actionUpload($licence);
                $model->documents['trade_licence'] = $trade_licence['url'];
            }

            if ($visa <> null) {
                $residence_visa = $this->actionUpload($visa);
                $model->documents['residence_visa'] = $residence_visa['url'];
            }

            if ($passport <> null) {
                $passport_copy = $this->actionUpload($passport);
                $model->documents['passport_copy'] = $passport_copy['url'];
            }

            if ($pic <> null) {
                $passport_size_pic = $this->actionUpload($pic);
                $model->documents['passport_size_pic'] = $passport_size_pic['url'];
            }


            if ($user = $model->upgrade()) {
                \Yii::$app->session->setFlash('success', 'Your application for Business membership upgrade to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else {
            $errors[] = $model->getErrors();
        }

        return $this->render('/account/upgrade-form', [
            'model' => $model,
            'errors' => $errors
        ]);

    }
    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddMemberContact(){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->request->post()) {

            if (Yii::$app->request->post('type') == "add") {

                $model = new MemberContacts();
                $model->member_id = Yii::$app->user->id;
                $model->contact_id = Yii::$app->request->post('id');
                $model->date_added = date("Y-m-d");

                if ($model->save()) {
                    echo 1;
                } else {
                    print_r($model->getErrors());
                }
            } else {
                if (MemberContacts::deleteAll([
                    'contact_id' => Yii::$app->request->post('id'),
                    'member_id' => Yii::$app->user->id,
                ])) {
                    echo 1;
                }
            }
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionAddMemberCompany(){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if (Yii::$app->request->post()) {

            if (Yii::$app->request->post('type') == "add") {

                $model = new MemberCompanies();
                $model->member_id = Yii::$app->user->id;
                $model->member_company = Yii::$app->request->post('id');
                $model->date_added = date("Y-m-d");

                if ($model->save()) {
                    echo 1;
                } else {
                    print_r($model->getErrors());
                }
            } else {
                if (MemberCompanies::deleteAll([
                    'member_company' => Yii::$app->request->post('id'),
                    'member_id' => Yii::$app->user->id,
                ])) {
                    echo 1;
                }
            }
        }
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionMemberDetails($id, $type = "")
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if ($type) {

            $search = SearchExpertise::findOne([
                'search_keyword' => $type,
                'user_viewed' => $id,
                'search_by' => Yii::$app->user->id
            ]);


            if($search == null){
                $search = new SearchExpertise();
            }

            $search->search_keyword = $type;
            $search->user_viewed = $id;
            $search->search_by = Yii::$app->user->id;
            $search->save();
        }

        return $this->render('member-details', [
            'model' => $this->findModel(Yii::$app->user->id),
            'member' => $this->findModel($id)
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionEventsRegistered()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = EventSubscriptions::find()->where([
            'user_id' => Yii::$app->user->id,
        ]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $events = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('id', 'DESC')
            ->all();

        return $this->render('events_registered', [
            'model' => $this->findModel(Yii::$app->user->id),
            'events' => $events,
            'pages' => $pages
        ]);

    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionInvoices()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = Invoices::find()->where([
            'user_id' => Yii::$app->user->id,
            /*'payment_status' => 'unpaid'*/
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $invoices = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('invoice_id', 'DESC')
            ->all();

        return $this->render('invoices', [
            'model' => $this->findModel(Yii::$app->user->id),
            'invoices' => $invoices,
            'pages' => $pages
        ]);
    }

    /**
     * @param $invoice
     * @return \yii\web\Response
     */
    public function actionViewInvoice($invoice){

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $member = Members::findOne(Yii::$app->user->id);
        $invoices = Invoices::findOne($invoice);

        $pdf = \Yii::$app->pdf;
        $mpdf = $pdf->api;
        $mpdf->format = Pdf::FORMAT_A4;
        $mpdf->orientation = Pdf::ORIENT_PORTRAIT;

        if ($invoices->invoice_related_to == "event") {

            $content = $this->renderPartial('/account/send_event_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);

        } else {

            $content = $this->renderPartial('/account/send_invoice', [
                'member' => $member,
                'invoice' => $invoices
            ]);
        }

        $mpdf->WriteHtml($content);
        return $mpdf->Output('Invoice', 'I');
    }

    /**
     * @return \yii\web\Response
     */
    public function actionRenewMembership()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }


        $member = Members::findOne(Yii::$app->user->id);
        $invoice_data = new Members();
        $data = $invoice_data->createRenewalInvoice($member);

        if($data <> null && count($data) > 0){
            return $this->redirect('/site/invoice-payment?invoice_id=' . $data['invoice_num']);
        } else{
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/profile']);
        }

    }

    /**
     * @return bool|\yii\web\Response
     */
    public function actionSaveIntrestedCategories()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if(Yii::$app->request->post()){

            $categories = Yii::$app->request->post('interested_category');

            if(count($categories) > 0){

                InterestedCategories::deleteAll(['member_id' => Yii::$app->user->id]);

                foreach ($categories as $value ){

                    $cat = new InterestedCategories();
                    $cat->member_id = Yii::$app->user->id;
                    $cat->category_id = $value;
                    $cat->save();

                }
            }

            return false;
        }
    }

    /**
     *
     */
    public function actionExpertRequests()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = SearchExpertise::find()->where([
            'user_viewed' => Yii::$app->user->id
        ]);


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $requests = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('date', 'DESC')
            ->all();

        return $this->render('expert_requesst', [
            'model' => $this->findModel(Yii::$app->user->id),
            'requests' => $requests,
            'pages' => $pages
        ]);
    }

    public function actionMyCompanyMembers()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $company = Yii::$app->user->identity->company;
        $payments = Payments::find()->where(['user_id' => Yii::$app->user->id]);
        $companyInfo = AccountCompany::findOne($company);
        $showComapnyInfo = false;
        if ($company) {


            $query = Members::find()->where(['status' => '1', 'company' => $company])->andWhere(['!=', 'id', Yii::$app->user->id]);

            $countQuery = clone $query;
            $pages = new Pagination([
                'totalCount' => $countQuery->count(),
                'pageSize' => 20
            ]);

            $members = $query->offset($pages->offset)
                ->limit($pages->limit)->orderBy('first_name', 'ASC')
                ->all();
        }

        return $this->render('company-members', [
            'payments' => $payments,
            'model' => $this->findModel(Yii::$app->user->id),
            'members' => $members,
            'pages' => $pages,
            'companyInfo' => $companyInfo,
            'showComapnyInfo' => $showComapnyInfo
        ]);
    }

    /**
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionAddAdditional()
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $errors = [];

        $additional = new Additonal();
        $additional->group_id = 2;
        $additional->account_type = 'member';
        $additional->companyData = null;
        $additional->companyInfo = null;

        if ($additional->load(Yii::$app->request->post()) && $additional->validate()) {

            $licence = UploadedFile::getInstance($additional, 'trade_licence');
            $visa = UploadedFile::getInstance($additional, 'residence_visa');
            $passport = UploadedFile::getInstance($additional, 'passport_copy');
            $pic = UploadedFile::getInstance($additional, 'passport_size_pic');

            if ($licence <> null) {
                $trade_licence = $this->actionUpload($licence);
                $additional->documents['trade_licence'] = $trade_licence['url'];
            }

            if ($visa <> null) {
                $residence_visa = $this->actionUpload($visa);
                $additional->documents['residence_visa'] = $residence_visa['url'];
            }

            if ($passport <> null) {
                $passport_copy = $this->actionUpload($passport);
                $additional->documents['passport_copy'] = $passport_copy['url'];
            }

            if ($pic <> null) {
                $passport_size_pic = $this->actionUpload($pic);
                $additional->documents['passport_size_pic'] = $passport_size_pic['url'];
            }

            if ($user = $additional->signup()) {
                \Yii::$app->session->setFlash('success', 'Your application for membership to the British Business Group is received with thanks. Check your email for further instructions.');
                return $this->redirect(['/login']);
            }

        } else {
            $errors[] = $additional->getErrors();
        }

        return $this->render('additional_form', [
            'model' => $this->findModel(Yii::$app->user->id),
            'additional' => $additional,
            'errors' => $errors
        ]);

    }

    public function actionAssociates()
    {
        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $model = $this->findModel(Yii::$app->user->id);

        if ($model->group <> null && !$model->group->add_associate) {

            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $query = NamedAssosiate::find()->where(['parent_user_id' => Yii::$app->user->id]);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 20
        ]);

        $members = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('assosiate_id', 'DESC')
            ->all();


        return $this->render('associates_list', [
            'model' => $model,
            'associates' => $members,
            'pages' => $pages
        ]);

    }

    public function actionAssociateMember($id = "")
    {

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        $model = $this->findModel(Yii::$app->user->id);

        if ($model->group <> null && !$model->group->add_associate) {

            \Yii::$app->session->set("backToURL", Url::to(['/account/member-directory']));
            return $this->redirect(['/login']);
        }

        if ($id) {
            $member = NamedAssosiate::findOne($id);
        } else {
            $member = new NamedAssosiate();
            $member->parent_user_id = Yii::$app->user->id;
        }


        if ($member->load(Yii::$app->request->post()) && $member->save()) {
            $this->redirect('/account/associates');
        }

        return $this->render('associate_form', [
            'model' => $model,
            'associate' => $member,
        ]);

    }
}
