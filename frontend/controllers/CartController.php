<?php

namespace frontend\controllers;

use common\models\CartModel;
use Yii;

/**
 * Class CartController
 * @package frontend\controllers
 */
class CartController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     * @return mixed
     *
     */
    public function actionIndex()
    {
        $items = Yii::$app->session->get('cartItems');
        return $this->render('index', [
            'items' => $items,
        ]);
    }

    /**
     * @inheritdoc
     * @return mixed
     *
     */
    public function actionAddToCart()
    {
        $model = new CartModel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $model->addToCart();
            Yii::$app->session->setFlash('success', 'Item added into cart successfully.');

            if (!Yii::$app->user->isGuest) {
                return $this->redirect(['/booking/customer-checkout']);
            } else {
                return $this->redirect(['/booking/guest-checkout']);
            }
        } else {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $model;
        }
    }

    /**
     * @return CartModel|\yii\web\Response
     */
    public function actionDeleteCartItem()
    {

        $cart = new CartModel();

        $key = Yii::$app->request->post('key');
        $cart->deleteCartItem($key);

        if ($cart->getItems()) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['status' => 'OK', 'message' => 'Item deleted from cart.'];
        } else {
            return $this->redirect(['/tour']) ;
        }
    }
}
