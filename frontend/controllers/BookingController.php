<?php

namespace frontend\controllers;

use common\models\Address;
use common\models\Banners;
use common\models\CheckoutModel;
use common\models\PromoCode;
use common\models\SignupForm;
use common\models\TourBookings;
use Yii;
use yii\base\Model;

/**
 * Class BookingController
 * @package frontend\controllers
 */
class BookingController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     * @return mixed
     */
    public function actionIndex()
    {
        $items = Yii::$app->session->get('cartItems');
        return $this->render('index', [
            'items' => $items,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionGuestCheckout()
    {

        // Redirect user to tours page if he / she not logged in
        if (!\Yii::$app->session->has('cartItems')) {
            return $this->redirect(['/tours']);
        }

        // Redirect User to customer checkout if he / she try to get guest - checkout
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/customer-checkout']);
        }

        $model = new CheckoutModel([
            'scenario' => 'guest',
            'payment_type' => 'COD'
        ]);

        $signUpModel = new SignupForm();
        $address = new Address();

        if ($model->load(Yii::$app->request->post()) &&
            $signUpModel->load(Yii::$app->request->post()) &&
            $address->load(Yii::$app->request->post()) &&
            Model::validateMultiple([$model, $signUpModel, $address])
        ) {
            $signUpModel->group_id = 1;
            // Add customer and signup
            if ($user = $signUpModel->signup()) {
                Yii::$app->getUser()->login($user);
            }

            // Save Customer Address
            $address->account_id = $user->id;
            $address->save();

            $model->address_id = $address->id;
            $model->account_id = $user->id;
            $booking = $model->saveBooking();

            // Send email to customer
            $model->sendEmail(
                $user->email,
                $booking,
                $model->cart->totalCartAmount()
            );

            // Flush cart Session
            $model->cart->flushCart();
            return $this->redirect(['thank-you', 'booking_id' => $booking->id]);
        } else {
            return $this->render('guest-checkout', [
                'model' => $model,
                'address' => $address,
                'signUpModel' => $signUpModel
            ]);
        }
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCustomerCheckout()
    {

        if (!\Yii::$app->session->has('cartItems')) {
            return $this->redirect(['/tours']);
        }

        $model = new CheckoutModel([
            'scenario' => 'customer',
            'account_id' => Yii::$app->user->id,
            'payment_type' => 'COD'
        ]);

        // If request is post then try to create booking
        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $booking = $model->saveBooking();

                // Send email to customer
                $model->sendEmail(
                    \Yii::$app->user->getIdentity()->email,
                    $booking,
                    $model->cart->totalCartAmount()
                );

                // Flush cart Session
                $model->cart->flushCart();
                return $this->redirect(['thank-you', 'booking_id' => $booking->id]);
            }
        }

        return $this->render('customer-checkout', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $booking_id Tour Booking Id
     * @return string
     */
    public function actionThankYou($booking_id)
    {
        $banner = Banners::find()->where(['slug' => 'thank-you-banner'])->one();
        $booking = TourBookings::findOne($booking_id);

        return $this->render('thank-you', [
            'banner' => $banner,
            'model' => '',
            'booking' => $booking
        ]);
    }

    /**
     * Apply Promo discount
     * */
    public function actionApplyPromo()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $promo = new PromoCode();

        if ($promo->load(Yii::$app->request->post(), '') && $promo->validate()) {
            $promo->applyCode();

            return [
                'status' => 'OK',
                'message' => 'Promo code has been applied'
            ];

        } else {

            $errors = '';
            foreach ($promo->getErrors() as $error) {
                $errors .= '<br />' . $error[0];
            }

            return [
                'status' => 'ERROR',
                'message' => $errors
            ];
        }
    }


    /**
     * Apply Promo discount
     * */
    public function actionCancelPromo()
    {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        (new PromoCode())->cancelCode();

        return [
            'status' => 'OK',
            'message' => 'Promo code has been cancelled'
        ];
    }
}
