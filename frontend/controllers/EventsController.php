<?php

namespace frontend\controllers;

use common\helpers\EmailHelper;
use common\helpers\PaytabsHelper;
use common\models\EventImages;
use common\models\Events;
use common\models\EventSubscriptions;
use common\models\Invoices;
use common\models\Members;
use common\models\PaymentCards;
use common\models\Payments;
use common\models\SpecialEventsFee;
use common\models\TblEventfee;
use common\models\TblEventSubscription;
use common\models\TblImage;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;


/**
 * Class EventsController
 * @package frontend\controllers
 */
class EventsController extends \yii\web\Controller
{
    /**
     * @param $action
     * @return bool
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * @return string
     */
    public function actionIndex($type = "")
    {
        $isPast = false;

        if ($type == 'past') {
            $title = 'Past Events';
            $isPast = true;
            $query = Events::find()->where(['<=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1']);
        } else {
            $title = 'Upcoming Events';
            $query = Events::find()->where(['>=', 'event_startDate', date('Y-m-d')])->andWhere(['active' => '1']);
        }
        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $events = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('event_startDate', 'ASC')
            ->all();

        return $this->render('index', [
            'events' => $events,
            'pages' => $pages,
            'title' => $title,
            'isPast' => $isPast
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventDetails($id)
    {
        $event = Events::findOne($id);
        $reg_num = EventSubscriptions::find()->where(['event_id' => $id])->count();

        return $this->render('event_detail', [
            'event' => $event,
            'reg_num' => $reg_num
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventRegister($id, $s = "")
    {
        \Yii::$app->session->set("backToEventRegisterURL", null);

        $event = Events::findOne($id);


        if (Yii::$app->user->isGuest) {

            if (!$event->members_only) {
                return $this->redirect(['/events/event-guest-register', 'id' => $id]);
            }

            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id, 'members_only' => $event->members_only]));
            return $this->redirect(['/login']);
        }

        if ($event->registration_start >= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is not opend yet. Please wait for Registeration to open!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        if ($event->registration_end <= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is Closed!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        $event_subscriptions = EventSubscriptions::findAll([
            'registered_by' => Yii::$app->user->identity->id,
            'event_id' => $id
        ]);

        $member_contacts = Members::findAll(['parent_id' => Yii::$app->user->identity->id]);
        $cards = PaymentCards::find()->where(['member_id' => Yii::$app->user->identity->id])->all();
        $cardModel = new PaymentCards();

        if (\Yii::$app->request->post()) {
            $this->eventRegisterForm(Yii::$app->request->post());
        }

        return $this->render('register_event', [
            'event' => $event,
            'contacts' => $member_contacts,
            'event_subscriptions' => $event_subscriptions,
            'step' => $s,
            'cards' => $cards,
            'cardModel' => $cardModel
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventGuestRegister($id)
    {

        $event = Events::findOne($id);

        if (Yii::$app->request->post()) {

            $model = new EventSubscriptions();
            $invoice = $model->memberEventRegisteration(Yii::$app->request->post());

            if ($invoice) {
                $this->redirect('/site/invoice-payment?invoice_id=' . $invoice);
            }
        }

        return $this->render('register_guest', [
            'event' => $event,
        ]);

    }

    /**
     *
     */
    public function actionInformAboutEvent()
    {
        if (Yii::$app->request->post()) {
            $members = Yii::$app->request->post('members');
            $event = Events::findOne(Yii::$app->request->post('event'));

            if (count($members) > 0) {
                foreach ($members as $member) {

                    $user_email = Members::findOne($member)->email;

                    try {
                        // echo $user_email;
                        if((new EmailHelper())->sendEventEmail($user_email, 'faisal@800wisdom.ae', 'Event Information', 'events/event-registration', [
                            'event' => $event,
                            'user' => $user_email
                        ])){
                            echo 1;
                        } else{
                            echo 0;
                        }

                    } catch (Exception $e) {
                        echo false;
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @return array|\yii\web\Response
     */
    public function eventRegisterForm($data)
    {

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (count($data) > 0) {

            $subscribe = new EventSubscriptions();
            $resp = $subscribe->memberEventRegisteration($data);

            if (isset($resp['msg'])) {
                print_r(\GuzzleHttp\json_encode($resp));
                echo " | ";
                // $this->redirect('/events/event-register?id=' . $event . '&s=2');
            } else {
                return $subscribe->getErrors();
            }
        }
    }

    /**
     *
     */
    public function actionSubscriptionCancelRequest()
    {

        if (Yii::$app->request->post()) {
            $subscription = EventSubscriptions::findOne(Yii::$app->request->post('subscription'));
            $subscription->cancel_request = Yii::$app->request->post('subscription');
            if (!$subscription->save()) {
                print_r($subscription->getErrors());
                return false;
            }
            echo 1;
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionPayment($id)
    {
        $year = [];

        $y = date('y');
        $Y = date('Y');

        for ($i = 0; $i < 30; $i++) {

            if ($i > 0) {
                $y = $y + 1;
                $Y = $Y + 1;
            }

            $year[$y] = $Y;
        }
        $event = Events::findOne($id);

        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/payment', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $event_subscriptions = EventSubscriptions::findAll([
            'registered_by' => Yii::$app->user->identity->id,
            'event_id' => $id,
            'payment_status' => 'pending'
        ]);


        return $this->render('event-payment', [
            'event' => $event,
            'event_subscriptions' => $event_subscriptions,
            'years' => $year
        ]);
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     */
    public function actionThankYou($id = "")
    {
        $member_fee = 0;
        $userData = [];
        // echo '<pre>'; print_r(Yii::$app->request->post()); die;
        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $response = [];
        $event = Events::findOne($id);

        if(Yii::$app->request->post()){

            $response = \Yii::$app->request->post();
            $invoice_id = \Yii::$app->request->post('order_id');
            $invoice = Invoices::findOne(['invoice_id' => $invoice_id]);

            if($invoice <> null){
                if($invoice->invoice_related_to == 'event'){
                    $event = Events::findOne($invoice->invoice_rel_id);
                }
            }

            $payment = new Payments();
            $payment->paytabs_make_payment($response);

        }

        $member_contacts = Members::findAll(['parent_id' => Yii::$app->user->identity->id]);

        $event_subscriptions = EventSubscriptions::findAll([
            'registered_by' => Yii::$app->user->identity->id,
            'event_id' => $id
        ]);

        return $this->render('thankyou', [
            'event' => $event,
            'contacts' => $member_contacts,
            'response' => $response
            // 'event_subscriptions' => $event_subscriptions,
        ]);
    }

    /**
     * These functions are for the purpose of data import from old site to this one
     */

    public function actionImageImport()
    {

        $images = TblImage::find()->all();
        $path = '/uploads/events/';

        foreach ($images as $image) {

            if ($image->type == 'event') {

                $eventImg = EventImages::findOne(['event_id' => $image->product_id]);

                if ($eventImg == null) {
                    $eventImg = new EventImages();
                }

                $eventImg->event_id = $image->product_id;
                $eventImg->user_id = $image->user_id;
                $eventImg->image = $path . $image->filename;
                $eventImg->title = $image->title;

                if (!$eventImg->save()) {
                    echo '<pre>';
                    print_r($eventImg->getErrors());
                    echo '</pre>';
                } else {
                    echo '<span style="color: green;">' . $eventImg->id . '</span><br/>';
                }

            }
        }
    }

    public function actionSpecialEventFee()
    {

        $eventFee = TblEventfee::find()->all();

        foreach ($eventFee as $fee) {

            $specialFee = SpecialEventsFee::findOne(['id' => $fee->feeId]);

            if ($specialFee == null) {
                $specialFee = new SpecialEventsFee();
            }

            $specialFee->id = $fee->feeId;
            $specialFee->event_id = $fee->eventId;
            $specialFee->fee_type = $fee->feeName;
            $specialFee->amount = $fee->feeAmount;

            if (!$specialFee->save()) {
                echo '<pre>';
                print_r($specialFee->getErrors());
                echo '</pre>';
            } else {
                echo '<span style="color: green;">' . $specialFee->id . '</span><br/>';
            }

        }

    }

    public function actionImportSubscriptions()
    {
        // ?page=2&per-page=2
        $count = 0;
        $query = TblEventSubscription::find();

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 1000
        ]);

        $event_sub = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('create_date', 'DESC')->all();

        // $event_sub = TblEventSubscription::find()->all();
        if (count($event_sub) > 0) {
            foreach ($event_sub as $reg) {

                $new_subscriptions = EventSubscriptions::findOne(['id' => $reg->id]);

                if ($new_subscriptions == null) {
                    $new_subscriptions = new EventSubscriptions();
                }

                $new_subscriptions->id = $reg->id;
                $new_subscriptions->user_id = $reg->user_id;

                $new_subscriptions->user_type = "member";
                if ($reg->user_type == 'Guest') {
                    $new_subscriptions->user_type = "guest";
                }

                $new_subscriptions->registered_by = $reg->user_parent_id;
                $new_subscriptions->event_id = $reg->event_id;
                $new_subscriptions->firstname = $reg->firstname;
                $new_subscriptions->lastname = $reg->lastname;
                $new_subscriptions->email = $reg->email;
                $new_subscriptions->userNote = $reg->userNote;
                $new_subscriptions->mobile = $reg->mobile;
                $new_subscriptions->company = $reg->company;

                $reg->walkin = "0";
                if($reg->walkin == 'true'){
                    $reg->walkin = "1";
                }

                $new_subscriptions->walkin = $reg->walkin;

                $reg->attended = "0";
                if($reg->attended == 'true'){
                    $reg->attended = "1";
                }

                $new_subscriptions->attended = $reg->attended;
                $new_subscriptions->fee_paid = $reg->fee_paid;
                $new_subscriptions->subtotal = $reg->sub_total;
                $new_subscriptions->tax = $reg->vat;
                $new_subscriptions->tax_group_id = 1;
                $new_subscriptions->payment_status = $reg->payment_status;

                if ($reg->status == "true") {
                    $reg->status = "approved";
                }

                if ($reg->status == "false") {
                    $reg->status = "cancelled";
                }

                if ($reg->status == "pending") {
                    $reg->status = "pending";
                }



                $new_subscriptions->status = $reg->status;
                //$new_subscriptions->cancel_request = $reg->feeAmount;
                $new_subscriptions->created_at = strtotime($reg->create_date);
                if($reg->gen_invoice == "1"){
                    $new_subscriptions->gen_invoice = $str = ltrim($reg->payment_session_data, '0');
                }
                $new_subscriptions->focGuest = (string)$reg->focGuest;
                // $new_subscriptions->user_type_relation = $reg->feeAmount;
                $new_subscriptions->paymentMethod = $reg->paymentMethod;
                $new_subscriptions->session_key = $reg->session_data;


                if (!$new_subscriptions->save()) {
                    echo '<pre>';
                    print_r($new_subscriptions->getErrors());
                    echo '</pre>';
                } else {
                    echo '<span style="color: green;">' . $new_subscriptions->id . '</span><br/>';
                    $count++;
                }


            }
            echo '<br/> Registered: ' . $count;
        } else{
            echo "no record";
        }

    }
}
