<?php

namespace frontend\controllers;

use common\models\Cities;
use common\models\Hotels;
use Yii;
use common\models\Areas;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * Site controller
 */
class SettingController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * @inheritdoc
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * @param null $city_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionAreas($city_id = null) {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if( (int)$city_id > 0 ) {
            $areas = Areas::find()->where(['city_id' => $city_id])->all();
        } else {
            $areas = Areas::find()->all();
        }


        return $areas;
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionCities() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Cities::find()->all();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionHotels() {

        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return Hotels::find()->all();
    }
}
