<?php

namespace frontend\controllers;

use common\models\NewsletterSubscriptions;
use common\helpers\EmailHelper;
use common\models\Accounts;
use common\models\EventSubscriptions;
use common\models\News;
use Yii;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * Class EventsController
 * @package frontend\controllers
 */
class NewsController extends \yii\web\Controller
{
    /**
     * @return string
     */
    public function actionIndex($type = "")
    {

        if ($type != "") {

            if ($type == 'past') {

                $query = News::find()->where([
                    'isPublished' => '1'])->andWhere(['<=', 'FROM_UNIXTIME(created_at,"%Y-%m-%d")', date('Y-m-d')]);

            } else {
                $query = News::find()->where([
                    'isPublished' => '1',
                    'news_type' => $type
                ]);
            }

        } else {
            $query = News::find()->where(['isPublished' => '1']);
        }


        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $news = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('id', 'DESC')
            ->all();

        return $this->render('index', [
            'news' => $news,
            'pages' => $pages,
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionNewsDetails($id)
    {
        $news = News::findOne($id);

        return $this->render('news_detail', [
            'news' => $news
        ]);
    }

    /**
     * @param $id
     * @return string
     */
    public function actionEventRegister($id)
    {
        $member_fee = 0;
        $userData = [];

        \Yii::$app->session->set("backToEventRegisterURL", null);

        if (Yii::$app->user->isGuest) {
            \Yii::$app->session->set("backToEventRegisterURL", Url::to(['/events/event-register', 'id' => $id]));
            return $this->redirect(['/login']);
        }

        $event = Events::findOne($id);

        if ($event->registration_start >= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is not opend yet. Please wait for Registeration to open!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        if ($event->registration_end <= date('Y-m-d')) {
            \Yii::$app->session->setFlash('error', 'Event Registration is Closed!');
            $this->redirect(['/events/event-details', 'id' => $id]);
        }

        $member_contacts = Accounts::findAll(['parent_id' => Yii::$app->user->identity->id]);

        if (\Yii::$app->request->post()) {
            $userData = $this->eventRegisterForm(Yii::$app->request->post());
        }

        return $this->render('register_event', [
            'event' => $event,
            'contacts' => $member_contacts
        ]);
    }

    /**
     *
     */
    public function actionInformAboutEvent()
    {
        if (Yii::$app->request->post()) {
            $members = Yii::$app->request->post('members');
            $event = Events::findOne(Yii::$app->request->post('event'));

            if (count($members) > 0) {
                foreach ($members as $member) {

                    $user_email = Accounts::findOne($member)->email;
                     try {
                        echo $user_email;
                        (new EmailHelper())->sendEventEmail($user_email, 'faisal@800wisdom.ae', 'Event Information', 'events/event-registration.php', [
                            'event' => $event,
                            'user' => $user_email
                        ]);

                    } catch (Exception $e) {
                        echo '<pre>';
                        print_r($e->getMessage());
                        die();
                    }
                }
            }
        }
    }

    /**
     * @param $data
     * @return array|\yii\web\Response
     */
    public function eventRegisterForm($data)
    {

        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (count($data) > 0) {
            $subscribe = new EventSubscriptions();
            $resp = $subscribe->memberEventRegisteration($data);

            if ($resp['msg']) {
                print_r(\GuzzleHttp\json_encode($resp));
                echo " | ";
            } else {
                return $subscribe->getErrors();
            }
        }
    }

    /**
     * @return string
     */
    public function actionSubscribeNewsletter(){

        $model =   new NewsletterSubscriptions();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->setFlash('success', 'Your have subscribed to newsletter!');
            $this->redirect(['/']);
        } else {
            return $this->render('subscribe', [
                'model' => $model,
            ]);
        }
    }
}
