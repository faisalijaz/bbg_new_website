<?php

namespace frontend\controllers;

use Yii;
use common\models\Sponsors;
use frontend\models\SearchSponsors;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;
use yii\helpers\Url;

/**
 * SponsorsController implements the CRUD actions for Sponsors model.
 */
class SponsorsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Sponsors models.
     * @return mixed
     */
    public function actionIndex()
    {

        $query = Sponsors::find()->where(['status' => 'active']);

        $countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 10
        ]);

        $data = $query->offset($pages->offset)
            ->limit($pages->limit)->orderBy('id', 'DESC')
            ->all();

        return $this->render('index', [
            'data' => $data,
            'pages' => $pages,
        ]);
    }


    /**
     * @param $id
     * @return string
     */
    public function actionSponsorDetails($id)
    {
        $data = Sponsors::findOne($id);

        return $this->render('sponsor_detail', [
            'data' => $data
        ]);
    }

    /**
     * Finds the Sponsors model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sponsors the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sponsors::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
